<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FilterByUser;

/**
 * Class Order
 *
 * @package App
 * @property string $product
 * @property string $reference
 * @property enum $status
 * @property string $payment_completed_at
 * @property enum $active
 * @property string $created_by
*/
class Order extends Model
{
    use SoftDeletes, FilterByUser;

    
    protected $fillable = ['reference', 'status', 'payment_completed_at', 'active', 'product_id', 'created_by_id'];
    

    public static $enum_status = ["PENDING_PAYMENT" => "PENDING_PAYMENT", "COMPLETED" => "COMPLETED", "PROCESSING" => "PROCESSING", "ON_HOLD" => "ON_HOLD", "CANCELLED" => "CANCELLED", "REFUNDED" => "REFUNDED"];

    public static $enum_active = ["1" => "1", "0" => "0"];

    public static function boot()
    {
        parent::boot();

        Order::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'product_id' => 'integer|exists:products,id|max:4294967295|nullable',
            'reference' => 'max:191|required|unique:orders,reference',
            'status' => 'in:PENDING_PAYMENT,COMPLETED,PROCESSING,ON_HOLD,CANCELLED,REFUNDED|required',
            'payment_completed_at' => 'date_format:' . config('app.date_format') . '|max:191|nullable',
            'transactions' => 'array|nullable',
            'transactions.*' => 'integer|exists:transactions,id|max:4294967295|nullable',
            'active' => 'in:1,0|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'product_id' => 'integer|exists:products,id|max:4294967295|nullable',
            'reference' => 'max:191|required|unique:orders,reference,'.$request->route('order'),
            'status' => 'in:PENDING_PAYMENT,COMPLETED,PROCESSING,ON_HOLD,CANCELLED,REFUNDED|required',
            'payment_completed_at' => 'date_format:' . config('app.date_format') . '|max:191|nullable',
            'transactions' => 'array|nullable',
            'transactions.*' => 'integer|exists:transactions,id|max:4294967295|nullable',
            'active' => 'in:1,0|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setPaymentCompletedAtAttribute($input)
    {
        if ($input) {
            $this->attributes['payment_completed_at'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        }
    }

    /**
     * Get attribute from date format
     * @param $output
     *
     * @return string
     */
    public function getPaymentCompletedAtAttribute($output)
    {
        if ($output) {
            return Carbon::createFromFormat('Y-m-d', $output)->format(config('app.date_format'));
        }
    }
    
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id')->withTrashed();
    }
    
    public function transactions()
    {
        return $this->belongsToMany(Transaction::class, 'order_transaction')->withTrashed();
    }
    
    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }
    
    
}
