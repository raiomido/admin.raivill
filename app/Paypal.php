<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Paypal
 *
 * @package App
 * @property string $user
 * @property string $transaction
 * @property string $create_time
 * @property string $update_time
 * @property string $paypal_id
 * @property string $intent
 * @property string $transaction_status
 * @property string $email
 * @property string $payer_id
 * @property string $country_code
 * @property string $firstname
 * @property string $lastname
 * @property string $phone
 * @property decimal $amount
 * @property string $currency_code
 * @property string $payee_email_address
 * @property string $payee_merchant_id
 * @property text $shipping
 * @property string $payment_status
 * @property string $payment_id
 * @property text $seller_protection
 * @property text $links
*/
class Paypal extends Model
{
    use SoftDeletes;

    
    protected $fillable = ['create_time', 'update_time', 'paypal_id', 'intent', 'transaction_status', 'email', 'payer_id', 'country_code', 'firstname', 'lastname', 'phone', 'amount', 'currency_code', 'payee_email_address', 'payee_merchant_id', 'shipping', 'payment_status', 'payment_id', 'seller_protection', 'links', 'user_id', 'transaction_id'];
    

    public static function boot()
    {
        parent::boot();

        Paypal::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'user_id' => 'integer|exists:users,id|max:4294967295|nullable',
            'transaction_id' => 'integer|exists:transactions,id|max:4294967295|nullable',
            'create_time' => 'max:191|nullable',
            'update_time' => 'max:191|nullable',
            'paypal_id' => 'max:191|nullable',
            'intent' => 'max:191|nullable',
            'transaction_status' => 'max:191|nullable',
            'email' => 'max:191|nullable',
            'payer_id' => 'max:191|nullable',
            'country_code' => 'max:191|nullable',
            'firstname' => 'max:191|nullable',
            'lastname' => 'max:191|nullable',
            'phone' => 'max:191|nullable',
            'amount' => 'numeric|nullable',
            'currency_code' => 'max:191|nullable',
            'payee_email_address' => 'max:191|nullable',
            'payee_merchant_id' => 'max:191|nullable',
            'shipping' => 'max:65535|nullable',
            'payment_status' => 'max:191|nullable',
            'payment_id' => 'max:191|nullable',
            'seller_protection' => 'max:65535|nullable',
            'links' => 'max:65535|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'user_id' => 'integer|exists:users,id|max:4294967295|nullable',
            'transaction_id' => 'integer|exists:transactions,id|max:4294967295|nullable',
            'create_time' => 'max:191|nullable',
            'update_time' => 'max:191|nullable',
            'paypal_id' => 'max:191|nullable',
            'intent' => 'max:191|nullable',
            'transaction_status' => 'max:191|nullable',
            'email' => 'max:191|nullable',
            'payer_id' => 'max:191|nullable',
            'country_code' => 'max:191|nullable',
            'firstname' => 'max:191|nullable',
            'lastname' => 'max:191|nullable',
            'phone' => 'max:191|nullable',
            'amount' => 'numeric|nullable',
            'currency_code' => 'max:191|nullable',
            'payee_email_address' => 'max:191|nullable',
            'payee_merchant_id' => 'max:191|nullable',
            'shipping' => 'max:65535|nullable',
            'payment_status' => 'max:191|nullable',
            'payment_id' => 'max:191|nullable',
            'seller_protection' => 'max:65535|nullable',
            'links' => 'max:65535|nullable'
        ];
    }

    

    
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id')->withTrashed();
    }
    
    
}
