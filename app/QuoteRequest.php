<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FilterByUser;

/**
 * Class QuoteRequest
 *
 * @package App
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $phone
 * @property string $website
 * @property string $message
 * @property string $created_by
*/
class QuoteRequest extends Model
{
    use SoftDeletes, FilterByUser;

    
    protected $fillable = ['firstname', 'lastname', 'email', 'phone', 'website', 'message', 'created_by_id'];
    

    public static function boot()
    {
        parent::boot();

        QuoteRequest::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'firstname' => 'max:191|required',
            'lastname' => 'max:191|required',
            'email' => 'email|max:191|required',
            'phone' => 'max:191|nullable',
            'website' => 'max:191|nullable',
            'message' => 'max:191|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'firstname' => 'max:191|required',
            'lastname' => 'max:191|required',
            'email' => 'email|max:191|required',
            'phone' => 'max:191|nullable',
            'website' => 'max:191|nullable',
            'message' => 'max:191|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    

    
    
    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }
    
    
}
