<?php
namespace App;

use App\Http\Traits\Util;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FilterByUser;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Product
 *
 * @package App
 * @property string $name
 * @property string $slug
 * @property decimal $price
 * @property text $summary
 * @property text $description
 * @property integer $views
 * @property integer $likes_count
 * @property integer $comments_count
 * @property string $category
 * @property string $created_by
 * @property string $supplier
 * @property string $reference
 * @property string $file
 * @property string $link
*/
class Product extends Model implements HasMedia
{
    use SoftDeletes, FilterByUser, HasMediaTrait, Util;

    
    protected $fillable = ['name', 'slug', 'price', 'summary', 'description', 'views', 'likes_count', 'comments_count', 'reference', 'link', 'category_id', 'created_by_id', 'supplier_id'];
    protected $appends = ['images', 'images_link', 'uploaded_images', 'file', 'file_link'];
    protected $with = ['media'];
    protected $guarded = ['file'];


    public static function boot()
    {
        parent::boot();

        Product::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'name' => 'max:191|required',
            'images' => 'nullable',
            'images.*' => 'file|image|nullable',
            'price' => 'numeric|required',
            'summary' => 'max:65535|required',
            'description' => 'max:65535|required',
            'views' => 'integer|max:4294967295|nullable',
            'likes_count' => 'integer|max:4294967295|nullable',
            'comments_count' => 'integer|max:4294967295|nullable',
            'category_id' => 'integer|exists:product_categories,id|max:4294967295|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable',
            'supplier_id' => 'integer|exists:product_suppliers,id|max:4294967295|required',
            'file' => 'file|nullable',
            'link' => 'max:191|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'name' => 'max:191|required',
            'images' => 'sometimes',
            'images.*' => 'file|image|nullable',
            'price' => 'numeric|required',
            'summary' => 'max:65535|required',
            'description' => 'max:65535|required',
            'views' => 'integer|max:4294967295|nullable',
            'likes_count' => 'integer|max:4294967295|nullable',
            'comments_count' => 'integer|max:4294967295|nullable',
            'category_id' => 'integer|exists:product_categories,id|max:4294967295|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable',
            'supplier_id' => 'integer|exists:product_suppliers,id|max:4294967295|required',
            'file' => 'nullable',
            'link' => 'max:191|nullable'
        ];
    }

    

    public function getImagesAttribute()
    {
        return [];
    }

    public function getUploadedImagesAttribute()
    {
        return $this->getMedia('images')->keyBy('id');
    }

    /**
     * Hash password
     * @param $input
     */
    public function setSlugAttribute($input)
    {
        if ($input) {
            $this->attributes['slug'] = str_slug($input).'-'.time();
        }
    }

    /**
     * @return string
     */
    public function getImagesLinkAttribute()
    {
        $images = $this->getMedia('images');
        if (! count($images)) {
            return null;
        }
        $html = [];
        foreach ($images as $file) {
            $html[] = '<a href="' . $file->getUrl() . '" target="_blank">' . $file->file_name . '</a>';
        }

        return implode('<br/>', $html);
    }

    public function getFileAttribute()
    {
        return $this->getFirstMedia('file');
    }

    public function setReferenceAttribute()
    {
        $this->attributes['reference'] = $this->generateProductReference();
    }

    /**
     * @return string
     */
    public function getFileLinkAttribute()
    {
        $file = $this->getFirstMedia('file');
        if (! $file) {
            return null;
        }

        return '<a href="' . $file->getUrl() . '" target="_blank">' . $file->file_name . '</a>';
    }
    
    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'category_id')->withTrashed();
    }
    
    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }
    
    public function supplier()
    {
        return $this->belongsTo(ProductSupplier::class, 'supplier_id')->withTrashed();
    }
    
    
}
