<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FilterByUser;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Language
 *
 * @package App
 * @property string $title
 * @property string $code
 * @property string $locale
 * @property string $image
 * @property string $created_by
*/
class Language extends Model implements HasMedia
{
    use SoftDeletes, FilterByUser, HasMediaTrait;

    
    protected $fillable = ['title', 'code', 'locale', 'created_by_id'];
    protected $appends = ['image', 'image_link'];
    protected $with = ['media'];
    

    public static function boot()
    {
        parent::boot();

        Language::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'code' => 'max:191|required',
            'locale' => 'max:191|required',
            'image' => 'file|image|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'code' => 'max:191|required',
            'locale' => 'max:191|required',
            'image' => 'nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    

    public function getImageAttribute()
    {
        return $this->getFirstMedia('image');
    }

    /**
     * @return string
     */
    public function getImageLinkAttribute()
    {
        $file = $this->getFirstMedia('image');
        if (! $file) {
            return null;
        }

        return '<a href="' . $file->getUrl() . '" target="_blank">' . $file->file_name . '</a>';
    }
    
    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }
    
    
}
