<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FilterByUser;

/**
 * Class FragmentComment
 *
 * @package App
 * @property string $user
 * @property text $comment
 * @property string $active
 * @property string $fragment
 * @property string $created_by
*/
class FragmentComment extends Model
{
    use SoftDeletes, FilterByUser;

    
    protected $fillable = ['comment', 'active', 'user_id', 'fragment_id', 'created_by_id'];
    

    public static function boot()
    {
        parent::boot();

        FragmentComment::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'user_id' => 'integer|exists:users,id|max:4294967295|required',
            'comment' => 'max:65535|required',
            'active' => 'max:191|nullable',
            'fragment_id' => 'integer|exists:post_fragments,id|max:4294967295|required',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'user_id' => 'integer|exists:users,id|max:4294967295|required',
            'comment' => 'max:65535|required',
            'active' => 'max:191|nullable',
            'fragment_id' => 'integer|exists:post_fragments,id|max:4294967295|required',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    

    
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function fragment()
    {
        return $this->belongsTo(PostFragment::class, 'fragment_id')->withTrashed();
    }
    
    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }
    
    
}
