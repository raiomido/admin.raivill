<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FilterByUser;

/**
 * Class PostComment
 *
 * @package App
 * @property string $user
 * @property string $post
 * @property text $comment
 * @property string $active
 * @property string $created_by
*/
class PostComment extends Model
{
    use SoftDeletes, FilterByUser;

    
    protected $fillable = ['comment', 'active', 'user_id', 'post_id', 'created_by_id'];
    

    public static function boot()
    {
        parent::boot();

        PostComment::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'user_id' => 'integer|exists:users,id|max:4294967295|required',
            'post_id' => 'integer|exists:posts,id|max:4294967295|required',
            'comment' => 'max:65535|required',
            'active' => 'max:191|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'user_id' => 'integer|exists:users,id|max:4294967295|required',
            'post_id' => 'integer|exists:posts,id|max:4294967295|required',
            'comment' => 'max:65535|required',
            'active' => 'max:191|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    

    
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id')->withTrashed();
    }
    
    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }
    
    
}
