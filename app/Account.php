<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FilterByUser;

/**
 * Class Account
 *
 * @package App
 * @property string $number
 * @property decimal $balance
 * @property string $user
 * @property string $created_by
*/
class Account extends Model
{
    use SoftDeletes, FilterByUser;

    
    protected $fillable = ['number', 'balance', 'user_id', 'created_by_id'];
    

    public static function boot()
    {
        parent::boot();

        Account::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'number' => 'max:191|required|unique:accounts,number',
            'balance' => 'numeric|nullable',
            'user_id' => 'integer|exists:users,id|max:4294967295|required',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'number' => 'max:191|required|unique:accounts,number,'.$request->route('account'),
            'balance' => 'numeric|nullable',
            'user_id' => 'integer|exists:users,id|max:4294967295|required',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    

    
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }
    
    
}
