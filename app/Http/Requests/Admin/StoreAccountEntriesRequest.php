<?php
namespace App\Http\Requests\Admin;

use App\AccountEntry;
use Illuminate\Foundation\Http\FormRequest;

class StoreAccountEntriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return AccountEntry::storeValidation($this);
    }
}
