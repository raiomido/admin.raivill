<?php
namespace App\Http\Requests\Admin;

use App\RaivillAccount;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRaivillAccountsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return RaivillAccount::updateValidation($this);
    }
}
