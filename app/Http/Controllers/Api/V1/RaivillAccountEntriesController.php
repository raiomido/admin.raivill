<?php

namespace App\Http\Controllers\Api\V1;

use App\RaivillAccountEntry;
use App\Http\Controllers\Controller;
use App\Http\Resources\RaivillAccountEntry as RaivillAccountEntryResource;
use App\Http\Requests\Admin\StoreRaivillAccountEntriesRequest;
use App\Http\Requests\Admin\UpdateRaivillAccountEntriesRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;



class RaivillAccountEntriesController extends Controller
{
    public function index()
    {
        

        return new RaivillAccountEntryResource(RaivillAccountEntry::with(['raivill_account'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('raivill_account_entry_view')) {
            return abort(401);
        }

        $raivill_account_entry = RaivillAccountEntry::with(['raivill_account'])->findOrFail($id);

        return new RaivillAccountEntryResource($raivill_account_entry);
    }

    public function store(StoreRaivillAccountEntriesRequest $request)
    {
        if (Gate::denies('raivill_account_entry_create')) {
            return abort(401);
        }

        $raivill_account_entry = RaivillAccountEntry::create($request->all());
        
        

        return (new RaivillAccountEntryResource($raivill_account_entry))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateRaivillAccountEntriesRequest $request, $id)
    {
        if (Gate::denies('raivill_account_entry_edit')) {
            return abort(401);
        }

        $raivill_account_entry = RaivillAccountEntry::findOrFail($id);
        $raivill_account_entry->update($request->all());
        
        
        

        return (new RaivillAccountEntryResource($raivill_account_entry))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('raivill_account_entry_delete')) {
            return abort(401);
        }

        $raivill_account_entry = RaivillAccountEntry::findOrFail($id);
        $raivill_account_entry->delete();

        return response(null, 204);
    }
}
