<?php

namespace App\Http\Controllers\Api\V1;

use App\ProductComment;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductComment as ProductCommentResource;
use App\Http\Requests\Admin\StoreProductCommentsRequest;
use App\Http\Requests\Admin\UpdateProductCommentsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;


class ProductCommentsController extends Controller
{
    public function index()
    {
        

        return new ProductCommentResource(ProductComment::with(['user', 'product', 'created_by'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('product_comment_view')) {
            return abort(401);
        }

        $product_comment = ProductComment::with(['user', 'product', 'created_by'])->findOrFail($id);

        return new ProductCommentResource($product_comment);
    }

    public function store(StoreProductCommentsRequest $request)
    {
        if (Gate::denies('product_comment_create')) {
            return abort(401);
        }

        $product_comment = ProductComment::create($request->all());
        
        

        return (new ProductCommentResource($product_comment))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateProductCommentsRequest $request, $id)
    {
        if (Gate::denies('product_comment_edit')) {
            return abort(401);
        }

        $product_comment = ProductComment::findOrFail($id);
        $product_comment->update($request->all());
        
        
        

        return (new ProductCommentResource($product_comment))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('product_comment_delete')) {
            return abort(401);
        }

        $product_comment = ProductComment::findOrFail($id);
        $product_comment->delete();

        return response(null, 204);
    }
}
