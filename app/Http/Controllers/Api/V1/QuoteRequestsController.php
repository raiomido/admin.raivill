<?php

namespace App\Http\Controllers\Api\V1;

use App\QuoteRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\QuoteRequest as QuoteRequestResource;
use App\Http\Requests\Admin\StoreQuoteRequestsRequest;
use App\Http\Requests\Admin\UpdateQuoteRequestsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;


class QuoteRequestsController extends Controller
{
    public function index()
    {
        

        return new QuoteRequestResource(QuoteRequest::with(['created_by'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('quote_request_view')) {
            return abort(401);
        }

        $quote_request = QuoteRequest::with(['created_by'])->findOrFail($id);

        return new QuoteRequestResource($quote_request);
    }

    public function store(StoreQuoteRequestsRequest $request)
    {
        if (Gate::denies('quote_request_create')) {
            return abort(401);
        }

        $quote_request = QuoteRequest::create($request->all());
        
        

        return (new QuoteRequestResource($quote_request))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateQuoteRequestsRequest $request, $id)
    {
        if (Gate::denies('quote_request_edit')) {
            return abort(401);
        }

        $quote_request = QuoteRequest::findOrFail($id);
        $quote_request->update($request->all());
        
        
        

        return (new QuoteRequestResource($quote_request))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('quote_request_delete')) {
            return abort(401);
        }

        $quote_request = QuoteRequest::findOrFail($id);
        $quote_request->delete();

        return response(null, 204);
    }
}
