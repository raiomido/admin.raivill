<?php

namespace App\Http\Controllers\Api\V1;

use App\Mpesa;
use App\Http\Controllers\Controller;
use App\Http\Resources\Mpesa as MpesaResource;
use App\Http\Requests\Admin\StoreMpesasRequest;
use App\Http\Requests\Admin\UpdateMpesasRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;



class MpesasController extends Controller
{
    public function index()
    {
        

        return new MpesaResource(Mpesa::with(['user', 'transaction'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('mpesa_view')) {
            return abort(401);
        }

        $mpesa = Mpesa::with(['user', 'transaction'])->findOrFail($id);

        return new MpesaResource($mpesa);
    }

    public function store(StoreMpesasRequest $request)
    {
        if (Gate::denies('mpesa_create')) {
            return abort(401);
        }

        $mpesa = Mpesa::create($request->all());
        
        

        return (new MpesaResource($mpesa))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateMpesasRequest $request, $id)
    {
        if (Gate::denies('mpesa_edit')) {
            return abort(401);
        }

        $mpesa = Mpesa::findOrFail($id);
        $mpesa->update($request->all());
        
        
        

        return (new MpesaResource($mpesa))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('mpesa_delete')) {
            return abort(401);
        }

        $mpesa = Mpesa::findOrFail($id);
        $mpesa->delete();

        return response(null, 204);
    }
}
