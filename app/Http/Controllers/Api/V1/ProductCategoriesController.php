<?php

namespace App\Http\Controllers\Api\V1;

use App\ProductCategory;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductCategory as ProductCategoryResource;
use App\Http\Requests\Admin\StoreProductCategoriesRequest;
use App\Http\Requests\Admin\UpdateProductCategoriesRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;


class ProductCategoriesController extends Controller
{
    public function index()
    {
        

        return new ProductCategoryResource(ProductCategory::with(['created_by'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('product_category_view')) {
            return abort(401);
        }

        $product_category = ProductCategory::with(['created_by'])->findOrFail($id);

        return new ProductCategoryResource($product_category);
    }

    public function store(StoreProductCategoriesRequest $request)
    {
        if (Gate::denies('product_category_create')) {
            return abort(401);
        }

        $product_category = ProductCategory::create($request->all());
        
        

        return (new ProductCategoryResource($product_category))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateProductCategoriesRequest $request, $id)
    {
        if (Gate::denies('product_category_edit')) {
            return abort(401);
        }

        $product_category = ProductCategory::findOrFail($id);
        $product_category->update($request->all());
        
        
        

        return (new ProductCategoryResource($product_category))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('product_category_delete')) {
            return abort(401);
        }

        $product_category = ProductCategory::findOrFail($id);
        $product_category->delete();

        return response(null, 204);
    }
}
