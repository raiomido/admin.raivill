<?php

namespace App\Http\Controllers\Api\V1;

use App\Language;
use App\Http\Controllers\Controller;
use App\Http\Resources\Language as LanguageResource;
use App\Http\Requests\Admin\StoreLanguagesRequest;
use App\Http\Requests\Admin\UpdateLanguagesRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Http\Controllers\Traits\FileUploadTrait;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;


class LanguagesController extends Controller
{
    public function index()
    {
        

        return new LanguageResource(Language::with(['created_by'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('language_view')) {
            return abort(401);
        }

        $language = Language::with(['created_by'])->findOrFail($id);

        return new LanguageResource($language);
    }

    public function store(StoreLanguagesRequest $request)
    {
        if (Gate::denies('language_create')) {
            return abort(401);
        }

        $language = Language::create($request->all());
        
        if ($request->hasFile('image')) {
            $language->addMedia($request->file('image'))->toMediaCollection('image');
        }

        return (new LanguageResource($language))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateLanguagesRequest $request, $id)
    {
        if (Gate::denies('language_edit')) {
            return abort(401);
        }

        $language = Language::findOrFail($id);
        $language->update($request->all());
        
        if (! $request->input('image') && $language->getFirstMedia('image')) {
            $language->getFirstMedia('image')->delete();
        }
        if ($request->hasFile('image')) {
            $language->addMedia($request->file('image'))->toMediaCollection('image');
        }

        return (new LanguageResource($language))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('language_delete')) {
            return abort(401);
        }

        $language = Language::findOrFail($id);
        $language->delete();

        return response(null, 204);
    }
}
