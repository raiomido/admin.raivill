<?php

namespace App\Http\Controllers\Api\V1;

use App\Paypal;
use App\Http\Controllers\Controller;
use App\Http\Resources\Paypal as PaypalResource;
use App\Http\Requests\Admin\StorePaypalsRequest;
use App\Http\Requests\Admin\UpdatePaypalsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;



class PaypalsController extends Controller
{
    public function index()
    {
        

        return new PaypalResource(Paypal::with(['user', 'transaction'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('paypal_view')) {
            return abort(401);
        }

        $paypal = Paypal::with(['user', 'transaction'])->findOrFail($id);

        return new PaypalResource($paypal);
    }

    public function store(StorePaypalsRequest $request)
    {
        if (Gate::denies('paypal_create')) {
            return abort(401);
        }

        $paypal = Paypal::create($request->all());
        
        

        return (new PaypalResource($paypal))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdatePaypalsRequest $request, $id)
    {
        if (Gate::denies('paypal_edit')) {
            return abort(401);
        }

        $paypal = Paypal::findOrFail($id);
        $paypal->update($request->all());
        
        
        

        return (new PaypalResource($paypal))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('paypal_delete')) {
            return abort(401);
        }

        $paypal = Paypal::findOrFail($id);
        $paypal->delete();

        return response(null, 204);
    }
}
