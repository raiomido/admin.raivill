<?php

namespace App\Http\Controllers\Api\V1;

use App\RaivillAccount;
use App\Http\Controllers\Controller;
use App\Http\Resources\RaivillAccount as RaivillAccountResource;
use App\Http\Requests\Admin\StoreRaivillAccountsRequest;
use App\Http\Requests\Admin\UpdateRaivillAccountsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;


class RaivillAccountsController extends Controller
{
    public function index()
    {
        

        return new RaivillAccountResource(RaivillAccount::with(['created_by'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('raivill_account_view')) {
            return abort(401);
        }

        $raivill_account = RaivillAccount::with(['created_by'])->findOrFail($id);

        return new RaivillAccountResource($raivill_account);
    }

    public function store(StoreRaivillAccountsRequest $request)
    {
        if (Gate::denies('raivill_account_create')) {
            return abort(401);
        }

        $raivill_account = RaivillAccount::create($request->all());
        
        

        return (new RaivillAccountResource($raivill_account))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateRaivillAccountsRequest $request, $id)
    {
        if (Gate::denies('raivill_account_edit')) {
            return abort(401);
        }

        $raivill_account = RaivillAccount::findOrFail($id);
        $raivill_account->update($request->all());
        
        
        

        return (new RaivillAccountResource($raivill_account))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('raivill_account_delete')) {
            return abort(401);
        }

        $raivill_account = RaivillAccount::findOrFail($id);
        $raivill_account->delete();

        return response(null, 204);
    }
}
