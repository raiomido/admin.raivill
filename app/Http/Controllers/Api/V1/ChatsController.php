<?php

namespace App\Http\Controllers\Api\V1;

use App\Chat;
use App\Http\Controllers\Controller;
use App\Http\Resources\Chat as ChatResource;
use App\Http\Requests\Admin\StoreChatsRequest;
use App\Http\Requests\Admin\UpdateChatsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;


class ChatsController extends Controller
{
    public function index()
    {
        

        return new ChatResource(Chat::with(['user', 'created_by'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('chat_view')) {
            return abort(401);
        }

        $chat = Chat::with(['user', 'created_by'])->findOrFail($id);

        return new ChatResource($chat);
    }

    public function store(StoreChatsRequest $request)
    {
        if (Gate::denies('chat_create')) {
            return abort(401);
        }

        $chat = Chat::create($request->all());
        
        

        return (new ChatResource($chat))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateChatsRequest $request, $id)
    {
        if (Gate::denies('chat_edit')) {
            return abort(401);
        }

        $chat = Chat::findOrFail($id);
        $chat->update($request->all());
        
        
        

        return (new ChatResource($chat))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('chat_delete')) {
            return abort(401);
        }

        $chat = Chat::findOrFail($id);
        $chat->delete();

        return response(null, 204);
    }
}
