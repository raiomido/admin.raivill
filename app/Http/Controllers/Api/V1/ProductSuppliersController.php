<?php

namespace App\Http\Controllers\Api\V1;

use App\ProductSupplier;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductSupplier as ProductSupplierResource;
use App\Http\Requests\Admin\StoreProductSuppliersRequest;
use App\Http\Requests\Admin\UpdateProductSuppliersRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Http\Controllers\Traits\FileUploadTrait;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;


class ProductSuppliersController extends Controller
{
    public function index()
    {
        

        return new ProductSupplierResource(ProductSupplier::with(['country', 'created_by'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('product_supplier_view')) {
            return abort(401);
        }

        $product_supplier = ProductSupplier::with(['country', 'created_by'])->findOrFail($id);

        return new ProductSupplierResource($product_supplier);
    }

    public function store(StoreProductSuppliersRequest $request)
    {
        if (Gate::denies('product_supplier_create')) {
            return abort(401);
        }

        $product_supplier = ProductSupplier::create($request->all());
        
        if ($request->hasFile('image')) {
            $product_supplier->addMedia($request->file('image'))->toMediaCollection('image');
        }

        return (new ProductSupplierResource($product_supplier))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateProductSuppliersRequest $request, $id)
    {
        if (Gate::denies('product_supplier_edit')) {
            return abort(401);
        }

        $product_supplier = ProductSupplier::findOrFail($id);
        $product_supplier->update($request->all());
        
        if (! $request->input('image') && $product_supplier->getFirstMedia('image')) {
            $product_supplier->getFirstMedia('image')->delete();
        }
        if ($request->hasFile('image')) {
            $product_supplier->addMedia($request->file('image'))->toMediaCollection('image');
        }

        return (new ProductSupplierResource($product_supplier))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('product_supplier_delete')) {
            return abort(401);
        }

        $product_supplier = ProductSupplier::findOrFail($id);
        $product_supplier->delete();

        return response(null, 204);
    }
}
