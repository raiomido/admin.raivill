<?php

namespace App\Http\Controllers\Api\V1;

use App\PkAirtime;
use App\Http\Controllers\Controller;
use App\Http\Resources\PkAirtime as PkAirtimeResource;
use App\Http\Requests\Admin\StorePkAirtimesRequest;
use App\Http\Requests\Admin\UpdatePkAirtimesRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;



class PkAirtimesController extends Controller
{
    public function index()
    {
        

        return new PkAirtimeResource(PkAirtime::with(['transaction'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('pk_airtime_view')) {
            return abort(401);
        }

        $pk_airtime = PkAirtime::with(['transaction'])->findOrFail($id);

        return new PkAirtimeResource($pk_airtime);
    }

    public function store(StorePkAirtimesRequest $request)
    {
        if (Gate::denies('pk_airtime_create')) {
            return abort(401);
        }

        $pk_airtime = PkAirtime::create($request->all());
        
        

        return (new PkAirtimeResource($pk_airtime))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdatePkAirtimesRequest $request, $id)
    {
        if (Gate::denies('pk_airtime_edit')) {
            return abort(401);
        }

        $pk_airtime = PkAirtime::findOrFail($id);
        $pk_airtime->update($request->all());
        
        
        

        return (new PkAirtimeResource($pk_airtime))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('pk_airtime_delete')) {
            return abort(401);
        }

        $pk_airtime = PkAirtime::findOrFail($id);
        $pk_airtime->delete();

        return response(null, 204);
    }
}
