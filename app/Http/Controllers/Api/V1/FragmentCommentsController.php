<?php

namespace App\Http\Controllers\Api\V1;

use App\FragmentComment;
use App\Http\Controllers\Controller;
use App\Http\Resources\FragmentComment as FragmentCommentResource;
use App\Http\Requests\Admin\StoreFragmentCommentsRequest;
use App\Http\Requests\Admin\UpdateFragmentCommentsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;


class FragmentCommentsController extends Controller
{
    public function index()
    {
        

        return new FragmentCommentResource(FragmentComment::with(['user', 'fragment', 'created_by'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('fragment_comment_view')) {
            return abort(401);
        }

        $fragment_comment = FragmentComment::with(['user', 'fragment', 'created_by'])->findOrFail($id);

        return new FragmentCommentResource($fragment_comment);
    }

    public function store(StoreFragmentCommentsRequest $request)
    {
        if (Gate::denies('fragment_comment_create')) {
            return abort(401);
        }

        $fragment_comment = FragmentComment::create($request->all());
        
        

        return (new FragmentCommentResource($fragment_comment))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateFragmentCommentsRequest $request, $id)
    {
        if (Gate::denies('fragment_comment_edit')) {
            return abort(401);
        }

        $fragment_comment = FragmentComment::findOrFail($id);
        $fragment_comment->update($request->all());
        
        
        

        return (new FragmentCommentResource($fragment_comment))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('fragment_comment_delete')) {
            return abort(401);
        }

        $fragment_comment = FragmentComment::findOrFail($id);
        $fragment_comment->delete();

        return response(null, 204);
    }
}
