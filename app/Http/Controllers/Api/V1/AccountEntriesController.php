<?php

namespace App\Http\Controllers\Api\V1;

use App\AccountEntry;
use App\Http\Controllers\Controller;
use App\Http\Resources\AccountEntry as AccountEntryResource;
use App\Http\Requests\Admin\StoreAccountEntriesRequest;
use App\Http\Requests\Admin\UpdateAccountEntriesRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;



class AccountEntriesController extends Controller
{
    public function index()
    {
        

        return new AccountEntryResource(AccountEntry::with(['account'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('account_entry_view')) {
            return abort(401);
        }

        $account_entry = AccountEntry::with(['account'])->findOrFail($id);

        return new AccountEntryResource($account_entry);
    }

    public function store(StoreAccountEntriesRequest $request)
    {
        if (Gate::denies('account_entry_create')) {
            return abort(401);
        }

        $account_entry = AccountEntry::create($request->all());
        
        

        return (new AccountEntryResource($account_entry))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateAccountEntriesRequest $request, $id)
    {
        if (Gate::denies('account_entry_edit')) {
            return abort(401);
        }

        $account_entry = AccountEntry::findOrFail($id);
        $account_entry->update($request->all());
        
        
        

        return (new AccountEntryResource($account_entry))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('account_entry_delete')) {
            return abort(401);
        }

        $account_entry = AccountEntry::findOrFail($id);
        $account_entry->delete();

        return response(null, 204);
    }
}
