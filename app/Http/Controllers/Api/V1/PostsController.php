<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Traits\Util;
use App\Post;
use App\Http\Controllers\Controller;
use App\Http\Resources\Post as PostResource;
use App\Http\Requests\Admin\StorePostsRequest;
use App\Http\Requests\Admin\UpdatePostsRequest;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Http\Controllers\Traits\FileUploadTrait;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;


class PostsController extends Controller
{
    use Util;

    public function index()
    {


        return new PostResource(Post::with(['product', 'created_by', 'tags', 'categories'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('post_view')) {
            return abort(401);
        }

        $post = Post::with(['product', 'created_by', 'tags', 'categories'])->findOrFail($id);

        return new PostResource($post);
    }

    public function store(StorePostsRequest $request)
    {
        if (Gate::denies('post_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['slug'] = str_slug($request->title) . '-' . str_random(10);
        $post = Post::create($data);
        $post->tags()->sync($request->input('tags', []));
        $post->categories()->sync($request->input('categories', []));
        if ($request->hasFile('image')) {
            $post->addMedia($request->file('image'))->toMediaCollection('image');
        }

        return (new PostResource($post))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdatePostsRequest $request, $id)
    {
        if (Gate::denies('post_edit')) {
            return abort(401);
        }

        $post = Post::findOrFail($id);
        $data = $request->all();
        $post->fill($data)->save();
        $post->tags()->sync($request->input('tags', []));
        $post->categories()->sync($request->input('categories', []));
        if (!$request->input('image') && $post->getFirstMedia('image')) {
            $post->getFirstMedia('image')->delete();
        }
        if ($request->hasFile('image')) {
            $post->addMedia($request->file('image'))->toMediaCollection('image');
        }

        return (new PostResource($post))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('post_delete')) {
            return abort(401);
        }

        $post = Post::findOrFail($id);
        $post->delete();

        return response(null, 204);
    }
}
