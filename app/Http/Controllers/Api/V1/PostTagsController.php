<?php

namespace App\Http\Controllers\Api\V1;

use App\PostTag;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostTag as PostTagResource;
use App\Http\Requests\Admin\StorePostTagsRequest;
use App\Http\Requests\Admin\UpdatePostTagsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;


class PostTagsController extends Controller
{
    public function index()
    {
        

        return new PostTagResource(PostTag::with(['created_by'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('post_tag_view')) {
            return abort(401);
        }

        $post_tag = PostTag::with(['created_by'])->findOrFail($id);

        return new PostTagResource($post_tag);
    }

    public function store(StorePostTagsRequest $request)
    {
        if (Gate::denies('post_tag_create')) {
            return abort(401);
        }
        $data = $request->all();
        $data['slug'] = str_slug($request->slug);
        $post_tag = PostTag::create($data);
        
        

        return (new PostTagResource($post_tag))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdatePostTagsRequest $request, $id)
    {
        if (Gate::denies('post_tag_edit')) {
            return abort(401);
        }

        $post_tag = PostTag::findOrFail($id);
        $data = $request->all();
        $data['slug'] = str_slug($request->slug);
        $post_tag->update($data);
        
        
        

        return (new PostTagResource($post_tag))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('post_tag_delete')) {
            return abort(401);
        }

        $post_tag = PostTag::findOrFail($id);
        $post_tag->delete();

        return response(null, 204);
    }
}
