<?php

namespace App\Http\Controllers\Api\V1;

use App\Order;
use App\Http\Controllers\Controller;
use App\Http\Resources\Order as OrderResource;
use App\Http\Requests\Admin\StoreOrdersRequest;
use App\Http\Requests\Admin\UpdateOrdersRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;


class OrdersController extends Controller
{
    public function index()
    {
        

        return new OrderResource(Order::with(['product', 'transactions', 'created_by'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('order_view')) {
            return abort(401);
        }

        $order = Order::with(['product', 'transactions', 'created_by'])->findOrFail($id);

        return new OrderResource($order);
    }

    public function store(StoreOrdersRequest $request)
    {
        if (Gate::denies('order_create')) {
            return abort(401);
        }

        $order = Order::create($request->all());
        $order->transactions()->sync($request->input('transactions', []));
        

        return (new OrderResource($order))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateOrdersRequest $request, $id)
    {
        if (Gate::denies('order_edit')) {
            return abort(401);
        }

        $order = Order::findOrFail($id);
        $order->update($request->all());
        $order->transactions()->sync($request->input('transactions', []));
        
        

        return (new OrderResource($order))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('order_delete')) {
            return abort(401);
        }

        $order = Order::findOrFail($id);
        $order->delete();

        return response(null, 204);
    }
}
