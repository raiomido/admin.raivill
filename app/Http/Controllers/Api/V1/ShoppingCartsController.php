<?php

namespace App\Http\Controllers\Api\V1;

use App\ShoppingCart;
use App\Http\Controllers\Controller;
use App\Http\Resources\ShoppingCart as ShoppingCartResource;
use App\Http\Requests\Admin\StoreShoppingCartsRequest;
use App\Http\Requests\Admin\UpdateShoppingCartsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;


class ShoppingCartsController extends Controller
{
    public function index()
    {
        

        return new ShoppingCartResource(ShoppingCart::with(['created_by'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('shopping_cart_view')) {
            return abort(401);
        }

        $shopping_cart = ShoppingCart::with(['created_by'])->findOrFail($id);

        return new ShoppingCartResource($shopping_cart);
    }

    public function store(StoreShoppingCartsRequest $request)
    {
        if (Gate::denies('shopping_cart_create')) {
            return abort(401);
        }

        $shopping_cart = ShoppingCart::create($request->all());
        
        

        return (new ShoppingCartResource($shopping_cart))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateShoppingCartsRequest $request, $id)
    {
        if (Gate::denies('shopping_cart_edit')) {
            return abort(401);
        }

        $shopping_cart = ShoppingCart::findOrFail($id);
        $shopping_cart->update($request->all());
        
        
        

        return (new ShoppingCartResource($shopping_cart))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('shopping_cart_delete')) {
            return abort(401);
        }

        $shopping_cart = ShoppingCart::findOrFail($id);
        $shopping_cart->delete();

        return response(null, 204);
    }
}
