<?php

namespace App\Http\Controllers\Api\V1;

use App\PkToken;
use App\Http\Controllers\Controller;
use App\Http\Resources\PkToken as PkTokenResource;
use App\Http\Requests\Admin\StorePkTokensRequest;
use App\Http\Requests\Admin\UpdatePkTokensRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;



class PkTokensController extends Controller
{
    public function index()
    {
        

        return new PkTokenResource(PkToken::with(['transaction'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('pk_token_view')) {
            return abort(401);
        }

        $pk_token = PkToken::with(['transaction'])->findOrFail($id);

        return new PkTokenResource($pk_token);
    }

    public function store(StorePkTokensRequest $request)
    {
        if (Gate::denies('pk_token_create')) {
            return abort(401);
        }

        $pk_token = PkToken::create($request->all());
        
        

        return (new PkTokenResource($pk_token))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdatePkTokensRequest $request, $id)
    {
        if (Gate::denies('pk_token_edit')) {
            return abort(401);
        }

        $pk_token = PkToken::findOrFail($id);
        $pk_token->update($request->all());
        
        
        

        return (new PkTokenResource($pk_token))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('pk_token_delete')) {
            return abort(401);
        }

        $pk_token = PkToken::findOrFail($id);
        $pk_token->delete();

        return response(null, 204);
    }
}
