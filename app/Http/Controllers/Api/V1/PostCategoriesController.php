<?php

namespace App\Http\Controllers\Api\V1;

use App\PostCategory;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostCategory as PostCategoryResource;
use App\Http\Requests\Admin\StorePostCategoriesRequest;
use App\Http\Requests\Admin\UpdatePostCategoriesRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;


class PostCategoriesController extends Controller
{
    public function index()
    {
        

        return new PostCategoryResource(PostCategory::with(['created_by'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('post_category_view')) {
            return abort(401);
        }

        $post_category = PostCategory::with(['created_by'])->findOrFail($id);

        return new PostCategoryResource($post_category);
    }

    public function store(StorePostCategoriesRequest $request)
    {
        if (Gate::denies('post_category_create')) {
            return abort(401);
        }

        $post_category = PostCategory::create($request->all());
        
        

        return (new PostCategoryResource($post_category))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdatePostCategoriesRequest $request, $id)
    {
        if (Gate::denies('post_category_edit')) {
            return abort(401);
        }

        $post_category = PostCategory::findOrFail($id);
        $post_category->update($request->all());
        
        
        

        return (new PostCategoryResource($post_category))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('post_category_delete')) {
            return abort(401);
        }

        $post_category = PostCategory::findOrFail($id);
        $post_category->delete();

        return response(null, 204);
    }
}
