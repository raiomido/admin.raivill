<?php

namespace App\Http\Controllers\Api\V1;

use App\Account;
use App\Http\Controllers\Controller;
use App\Http\Resources\Account as AccountResource;
use App\Http\Requests\Admin\StoreAccountsRequest;
use App\Http\Requests\Admin\UpdateAccountsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;


class AccountsController extends Controller
{
    public function index()
    {
        

        return new AccountResource(Account::with(['user', 'created_by'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('account_view')) {
            return abort(401);
        }

        $account = Account::with(['user', 'created_by'])->findOrFail($id);

        return new AccountResource($account);
    }

    public function store(StoreAccountsRequest $request)
    {
        if (Gate::denies('account_create')) {
            return abort(401);
        }

        $account = Account::create($request->all());
        
        

        return (new AccountResource($account))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateAccountsRequest $request, $id)
    {
        if (Gate::denies('account_edit')) {
            return abort(401);
        }

        $account = Account::findOrFail($id);
        $account->update($request->all());
        
        
        

        return (new AccountResource($account))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('account_delete')) {
            return abort(401);
        }

        $account = Account::findOrFail($id);
        $account->delete();

        return response(null, 204);
    }
}
