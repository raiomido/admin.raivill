<?php

namespace App\Http\Controllers\Api\V1;

use App\PackageItem;
use App\Http\Controllers\Controller;
use App\Http\Resources\PackageItem as PackageItemResource;
use App\Http\Requests\Admin\StorePackageItemsRequest;
use App\Http\Requests\Admin\UpdatePackageItemsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;


class PackageItemsController extends Controller
{
    public function index()
    {
        

        return new PackageItemResource(PackageItem::with(['package', 'created_by'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('package_item_view')) {
            return abort(401);
        }

        $package_item = PackageItem::with(['package', 'created_by'])->findOrFail($id);

        return new PackageItemResource($package_item);
    }

    public function store(StorePackageItemsRequest $request)
    {
        if (Gate::denies('package_item_create')) {
            return abort(401);
        }

        $package_item = PackageItem::create($request->all());
        
        

        return (new PackageItemResource($package_item))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdatePackageItemsRequest $request, $id)
    {
        if (Gate::denies('package_item_edit')) {
            return abort(401);
        }

        $package_item = PackageItem::findOrFail($id);
        $package_item->update($request->all());
        
        
        

        return (new PackageItemResource($package_item))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('package_item_delete')) {
            return abort(401);
        }

        $package_item = PackageItem::findOrFail($id);
        $package_item->delete();

        return response(null, 204);
    }
}
