<?php

namespace App\Http\Controllers\Api\V1;

use App\PostFragment;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostFragment as PostFragmentResource;
use App\Http\Requests\Admin\StorePostFragmentsRequest;
use App\Http\Requests\Admin\UpdatePostFragmentsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;


class PostFragmentsController extends Controller
{
    public function index()
    {
        

        return new PostFragmentResource(PostFragment::with(['post', 'created_by'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('post_fragment_view')) {
            return abort(401);
        }

        $post_fragment = PostFragment::with(['post', 'created_by'])->findOrFail($id);

        return new PostFragmentResource($post_fragment);
    }

    public function store(StorePostFragmentsRequest $request)
    {
        if (Gate::denies('post_fragment_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['slug'] = str_slug($request->title).'-'.str_random(10);

        $post_fragment = PostFragment::create($data);
        
        

        return (new PostFragmentResource($post_fragment))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdatePostFragmentsRequest $request, $id)
    {
        if (Gate::denies('post_fragment_edit')) {
            return abort(401);
        }

        $post_fragment = PostFragment::findOrFail($id);
        $data = $request->all();
        $data['slug'] = '';
        $post_fragment->fill($request->all())->save();
        
        
        

        return (new PostFragmentResource($post_fragment))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('post_fragment_delete')) {
            return abort(401);
        }

        $post_fragment = PostFragment::findOrFail($id);
        $post_fragment->delete();

        return response(null, 204);
    }
}
