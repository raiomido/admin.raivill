<?php

namespace App\Http\Controllers\Api\V1;

use App\Product;
use App\Http\Controllers\Controller;
use App\Http\Resources\Product as ProductResource;
use App\Http\Requests\Admin\StoreProductsRequest;
use App\Http\Requests\Admin\UpdateProductsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Http\Controllers\Traits\FileUploadTrait;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;


class ProductsController extends Controller
{
    public function index()
    {
        

        return new ProductResource(Product::with(['category', 'created_by', 'supplier'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('product_view')) {
            return abort(401);
        }

        $product = Product::with(['category', 'created_by', 'supplier'])->findOrFail($id);

        return new ProductResource($product);
    }

    public function store(StoreProductsRequest $request)
    {
        if (Gate::denies('product_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['slug'] = $data['name'];
        $data['reference'] = '';
        $product = Product::create($data);
        
        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $key => $file) {
                $product->addMedia($file)->toMediaCollection('images');
            }
        }if ($request->hasFile('file')) {
            $product->addMedia($request->file('file'))->toMediaCollection('file');
        }

        return (new ProductResource($product))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateProductsRequest $request, $id)
    {
        if (Gate::denies('product_edit')) {
            return abort(401);
        }

        $product = Product::findOrFail($id);
        $product->update($request->except(['slug', 'reference']));
        
        $filesInfo = explode(',', $request->input('uploaded_images'));
        foreach ($product->getMedia('images') as $file) {
            if (! in_array($file->id, $filesInfo)) {
                $file->delete();
            }
        }if (! $request->input('file') && $product->getFirstMedia('file')) {
            $product->getFirstMedia('file')->delete();
        }
        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $key => $file) {
                $product->addMedia($file)->toMediaCollection('images');
            }
        }if ($request->hasFile('file')) {
            $product->addMedia($request->file('file'))->toMediaCollection('file');
        }

        return (new ProductResource($product))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('product_delete')) {
            return abort(401);
        }

        $product = Product::findOrFail($id);
        $product->delete();

        return response(null, 204);
    }
}
