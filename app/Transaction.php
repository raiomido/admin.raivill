<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Transaction
 *
 * @package App
 * @property string $user
 * @property string $reference
 * @property string $type
 * @property decimal $amount
 * @property decimal $cost
 * @property string $status
*/
class Transaction extends Model
{
    use SoftDeletes;

    
    protected $fillable = ['reference', 'type', 'amount', 'cost', 'status', 'user_id'];
    

    public static function boot()
    {
        parent::boot();

        Transaction::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'user_id' => 'integer|exists:users,id|max:4294967295|required',
            'reference' => 'max:191|required|unique:transactions,reference',
            'type' => 'max:191|required',
            'amount' => 'numeric|required',
            'cost' => 'numeric|required',
            'status' => 'max:191|required'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'user_id' => 'integer|exists:users,id|max:4294967295|required',
            'reference' => 'max:191|required|unique:transactions,reference,'.$request->route('transaction'),
            'type' => 'max:191|required',
            'amount' => 'numeric|required',
            'cost' => 'numeric|required',
            'status' => 'max:191|required'
        ];
    }

    

    
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    
}
