<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FilterByUser;

/**
 * Class Message
 *
 * @package App
 * @property string $chat
 * @property string $user
 * @property text $message
 * @property enum $active
 * @property string $created_by
 * @property string $email
 * @property string $phone
*/
class Message extends Model
{
    use SoftDeletes, FilterByUser;

    
    protected $fillable = ['message', 'active', 'email', 'phone', 'chat_id', 'user_id', 'created_by_id'];
    

    public static $enum_active = ["1" => "1", "0" => "0"];

    public static function boot()
    {
        parent::boot();

        Message::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'chat_id' => 'integer|exists:chats,id|max:4294967295|nullable',
            'user_id' => 'integer|exists:users,id|max:4294967295|required',
            'message' => 'max:65535|required',
            'active' => 'in:1,0|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable',
            'email' => 'email|max:191|nullable',
            'phone' => 'max:191|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'chat_id' => 'integer|exists:chats,id|max:4294967295|nullable',
            'user_id' => 'integer|exists:users,id|max:4294967295|required',
            'message' => 'max:65535|required',
            'active' => 'in:1,0|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable',
            'email' => 'email|max:191|nullable',
            'phone' => 'max:191|nullable'
        ];
    }

    

    
    
    public function chat()
    {
        return $this->belongsTo(Chat::class, 'chat_id')->withTrashed();
    }
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }
    
    
}
