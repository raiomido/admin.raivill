<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FilterByUser;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Country
 *
 * @package App
 * @property string $shortcode
 * @property string $title
 * @property string $created_by
 * @property string $flag
 * @property string $code
*/
class Country extends Model implements HasMedia
{
    use SoftDeletes, FilterByUser, HasMediaTrait;

    
    protected $fillable = ['shortcode', 'title', 'code', 'created_by_id'];
    protected $appends = ['flag', 'flag_link'];
    protected $with = ['media'];
    

    public static function boot()
    {
        parent::boot();

        Country::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'shortcode' => 'max:191|nullable',
            'title' => 'max:191|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable',
            'flag' => 'file|image|nullable',
            'code' => 'max:191|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'shortcode' => 'max:191|nullable',
            'title' => 'max:191|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable',
            'flag' => 'nullable',
            'code' => 'max:191|nullable'
        ];
    }

    

    public function getFlagAttribute()
    {
        return $this->getFirstMedia('flag');
    }

    /**
     * @return string
     */
    public function getFlagLinkAttribute()
    {
        $file = $this->getFirstMedia('flag');
        if (! $file) {
            return null;
        }

        return '<a href="' . $file->getUrl() . '" target="_blank">' . $file->file_name . '</a>';
    }
    
    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }
    
    
}
