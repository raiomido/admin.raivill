<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FilterByUser;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class ProductSupplier
 *
 * @package App
 * @property string $name
 * @property string $slug
 * @property string $website
 * @property string $image
 * @property string $email
 * @property string $phone
 * @property text $description
 * @property string $country
 * @property enum $active
 * @property string $created_by
*/
class ProductSupplier extends Model implements HasMedia
{
    use SoftDeletes, FilterByUser, HasMediaTrait;

    
    protected $fillable = ['name', 'slug', 'website', 'email', 'phone', 'description', 'active', 'country_id', 'created_by_id'];
    protected $appends = ['image', 'image_link'];
    protected $with = ['media'];
    

    public static $enum_active = ["1" => "1", "0" => "0"];

    public static function boot()
    {
        parent::boot();

        ProductSupplier::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'name' => 'max:191|required',
            'slug' => 'max:191|required|unique:product_suppliers,slug',
            'website' => 'max:191|required',
            'image' => 'file|image|nullable',
            'email' => 'email|max:191|nullable',
            'phone' => 'max:191|nullable',
            'description' => 'max:65535|required',
            'country_id' => 'integer|exists:countries,id|max:4294967295|required',
            'active' => 'in:1,0|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'name' => 'max:191|required',
            'slug' => 'max:191|required|unique:product_suppliers,slug,'.$request->route('product_supplier'),
            'website' => 'max:191|required',
            'image' => 'nullable',
            'email' => 'email|max:191|nullable',
            'phone' => 'max:191|nullable',
            'description' => 'max:65535|required',
            'country_id' => 'integer|exists:countries,id|max:4294967295|required',
            'active' => 'in:1,0|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    

    public function getImageAttribute()
    {
        return $this->getFirstMedia('image');
    }

    /**
     * @return string
     */
    public function getImageLinkAttribute()
    {
        $file = $this->getFirstMedia('image');
        if (! $file) {
            return null;
        }

        return '<a href="' . $file->getUrl() . '" target="_blank">' . $file->file_name . '</a>';
    }
    
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id')->withTrashed();
    }
    
    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }
    
    
}
