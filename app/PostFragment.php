<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FilterByUser;

/**
 * Class PostFragment
 *
 * @package App
 * @property string $title
 * @property text $body
 * @property string $post
 * @property enum $active
 * @property string $created_by
*/
class PostFragment extends Model
{
    use SoftDeletes, FilterByUser;

    
    protected $fillable = ['title', 'body', 'active', 'post_id', 'created_by_id'];
    

    public static $enum_active = ["1" => "1", "0" => "0"];

    public static function boot()
    {
        parent::boot();

        PostFragment::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'body' => 'max:65535|required',
            'post_id' => 'integer|exists:posts,id|max:4294967295|required',
            'active' => 'in:1,0|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'body' => 'max:65535|required',
            'post_id' => 'integer|exists:posts,id|max:4294967295|required',
            'active' => 'in:1,0|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    

    
    
    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id')->withTrashed();
    }
    
    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }
    
    
}
