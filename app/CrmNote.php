<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CrmNote
 *
 * @package App
 * @property string $customer
 * @property text $note
*/
class CrmNote extends Model
{
    
    protected $fillable = ['note', 'customer_id'];
    

    public static function boot()
    {
        parent::boot();

        CrmNote::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'customer_id' => 'integer|exists:crm_customers,id|max:4294967295|required',
            'note' => 'max:65535|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'customer_id' => 'integer|exists:crm_customers,id|max:4294967295|required',
            'note' => 'max:65535|nullable'
        ];
    }

    

    
    
    public function customer()
    {
        return $this->belongsTo(CrmCustomer::class, 'customer_id');
    }
    
    
}
