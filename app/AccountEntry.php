<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AccountEntry
 *
 * @package App
 * @property string $reference
 * @property enum $type
 * @property string $balance_before
 * @property decimal $balance_after
 * @property decimal $amount
 * @property string $account
*/
class AccountEntry extends Model
{
    use SoftDeletes;

    
    protected $fillable = ['reference', 'type', 'balance_before', 'balance_after', 'amount', 'account_id'];
    

    public static $enum_type = ["DEBIT" => "DEBIT", "CREDIT" => "CREDIT"];

    public static function boot()
    {
        parent::boot();

        AccountEntry::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'reference' => 'max:191|required|unique:account_entries,reference',
            'type' => 'in:DEBIT,CREDIT|required',
            'balance_before' => 'max:191|nullable',
            'balance_after' => 'numeric|nullable',
            'amount' => 'numeric|required',
            'account_id' => 'integer|exists:accounts,id|max:4294967295|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'reference' => 'max:191|required|unique:account_entries,reference,'.$request->route('account_entry'),
            'type' => 'in:DEBIT,CREDIT|required',
            'balance_before' => 'max:191|nullable',
            'balance_after' => 'numeric|nullable',
            'amount' => 'numeric|required',
            'account_id' => 'integer|exists:accounts,id|max:4294967295|nullable'
        ];
    }

    

    
    
    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id')->withTrashed();
    }
    
    
}
