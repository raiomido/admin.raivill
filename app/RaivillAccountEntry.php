<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RaivillAccountEntry
 *
 * @package App
 * @property string $reference
 * @property enum $type
 * @property string $balance_before
 * @property decimal $balance_after
 * @property decimal $amount
 * @property string $raivill_account
*/
class RaivillAccountEntry extends Model
{
    use SoftDeletes;

    
    protected $fillable = ['reference', 'type', 'balance_before', 'balance_after', 'amount', 'raivill_account_id'];
    

    public static $enum_type = ["DEBIT" => "DEBIT", "CREDIT" => "CREDIT"];

    public static function boot()
    {
        parent::boot();

        RaivillAccountEntry::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'reference' => 'max:191|required|unique:raivill_account_entries,reference',
            'type' => 'in:DEBIT,CREDIT|required',
            'balance_before' => 'max:191|nullable',
            'balance_after' => 'numeric|nullable',
            'amount' => 'numeric|required',
            'raivill_account_id' => 'integer|exists:raivill_accounts,id|max:4294967295|required'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'reference' => 'max:191|required|unique:raivill_account_entries,reference,'.$request->route('raivill_account_entry'),
            'type' => 'in:DEBIT,CREDIT|required',
            'balance_before' => 'max:191|nullable',
            'balance_after' => 'numeric|nullable',
            'amount' => 'numeric|required',
            'raivill_account_id' => 'integer|exists:raivill_accounts,id|max:4294967295|required'
        ];
    }

    

    
    
    public function raivill_account()
    {
        return $this->belongsTo(RaivillAccount::class, 'raivill_account_id')->withTrashed();
    }
    
    
}
