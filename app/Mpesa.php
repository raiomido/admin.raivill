<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Mpesa
 *
 * @package App
 * @property string $user
 * @property string $transaction
 * @property string $type
 * @property string $time
 * @property string $mpesa_transaction_id
 * @property string $amount
 * @property string $short_code
 * @property string $bill_ref_number
 * @property string $invoice_number
 * @property string $msisdn
 * @property string $firstname
 * @property string $middlename
 * @property string $lastname
 * @property string $org_account_balance
*/
class Mpesa extends Model
{
    use SoftDeletes;

    
    protected $fillable = ['type', 'time', 'mpesa_transaction_id', 'amount', 'short_code', 'bill_ref_number', 'invoice_number', 'msisdn', 'firstname', 'middlename', 'lastname', 'org_account_balance', 'user_id', 'transaction_id'];
    

    public static function boot()
    {
        parent::boot();

        Mpesa::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'user_id' => 'integer|exists:users,id|max:4294967295|nullable',
            'transaction_id' => 'integer|exists:transactions,id|max:4294967295|nullable',
            'type' => 'max:191|nullable',
            'time' => 'max:191|nullable',
            'mpesa_transaction_id' => 'max:191|nullable',
            'amount' => 'max:191|nullable',
            'short_code' => 'max:191|nullable',
            'bill_ref_number' => 'max:191|nullable',
            'invoice_number' => 'max:191|nullable',
            'msisdn' => 'max:191|nullable',
            'firstname' => 'max:191|nullable',
            'middlename' => 'max:191|nullable',
            'lastname' => 'max:191|nullable',
            'org_account_balance' => 'max:191|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'user_id' => 'integer|exists:users,id|max:4294967295|nullable',
            'transaction_id' => 'integer|exists:transactions,id|max:4294967295|nullable',
            'type' => 'max:191|nullable',
            'time' => 'max:191|nullable',
            'mpesa_transaction_id' => 'max:191|nullable',
            'amount' => 'max:191|nullable',
            'short_code' => 'max:191|nullable',
            'bill_ref_number' => 'max:191|nullable',
            'invoice_number' => 'max:191|nullable',
            'msisdn' => 'max:191|nullable',
            'firstname' => 'max:191|nullable',
            'middlename' => 'max:191|nullable',
            'lastname' => 'max:191|nullable',
            'org_account_balance' => 'max:191|nullable'
        ];
    }

    

    
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id')->withTrashed();
    }
    
    
}
