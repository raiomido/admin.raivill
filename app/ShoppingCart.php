<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FilterByUser;

/**
 * Class ShoppingCart
 *
 * @package App
 * @property string $identifier
 * @property string $instance
 * @property text $content
 * @property string $created_by
*/
class ShoppingCart extends Model
{
    use SoftDeletes, FilterByUser;

    
    protected $fillable = ['identifier', 'instance', 'content', 'created_by_id'];
    

    public static function boot()
    {
        parent::boot();

        ShoppingCart::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'identifier' => 'max:191|required|unique:shopping_carts,identifier',
            'instance' => 'max:191|required',
            'content' => 'max:65535|required',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'identifier' => 'max:191|required|unique:shopping_carts,identifier,'.$request->route('shopping_cart'),
            'instance' => 'max:191|required',
            'content' => 'max:65535|required',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    

    
    
    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }
    
    
}
