<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FilterByUser;

/**
 * Class RaivillAccount
 *
 * @package App
 * @property string $number
 * @property decimal $balance
 * @property string $name
 * @property string $created_by
*/
class RaivillAccount extends Model
{
    use SoftDeletes, FilterByUser;

    
    protected $fillable = ['number', 'balance', 'name', 'created_by_id'];
    

    public static function boot()
    {
        parent::boot();

        RaivillAccount::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'number' => 'max:191|required|unique:raivill_accounts,number',
            'balance' => 'numeric|nullable',
            'name' => 'max:191|required',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'number' => 'max:191|required|unique:raivill_accounts,number,'.$request->route('raivill_account'),
            'balance' => 'numeric|nullable',
            'name' => 'max:191|required',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    

    
    
    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }
    
    
}
