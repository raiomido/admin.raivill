<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class LinkFilesDirectory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'raivill:link-files';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Link files directory';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $target = base_path('../files.raivill.com');
            $link = public_path('files');
            symlink ($target ,$link );
            $this->info("Success. Files symbolic link created successfully");
        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }
    }
}
