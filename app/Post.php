<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FilterByUser;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Post
 *
 * @package App
 * @property string $title
 * @property string $slug
 * @property string $product
 * @property string $image
 * @property enum $active
 * @property string $created_by
 * @property string $video_link
*/
class Post extends Model implements HasMedia
{
    use SoftDeletes, FilterByUser, HasMediaTrait;

    
    protected $fillable = ['title', 'slug', 'active', 'product_id', 'created_by_id', 'video_link'];
    protected $appends = ['image', 'image_link'];
    protected $with = ['media'];
    

    public static $enum_active = ["1" => "1", "0" => "0"];

    public static function boot()
    {
        parent::boot();

        Post::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'product_id' => 'integer|exists:products,id|max:4294967295',
            'image' => 'file|image|nullable',
            'active' => 'in:1,0|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable',
            'tags' => 'array|nullable',
            'tags.*' => 'integer|exists:post_tags,id|max:4294967295|nullable',
            'categories' => 'array|nullable',
            'categories.*' => 'integer|exists:post_categories,id|max:4294967295|nullable',
            'video_link' => 'max:191|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'product_id' => 'integer|exists:products,id|max:4294967295',
            'image' => 'nullable',
            'active' => 'in:1,0|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable',
            'tags' => 'array|nullable',
            'tags.*' => 'integer|exists:post_tags,id|max:4294967295|nullable',
            'categories' => 'array|nullable',
            'categories.*' => 'integer|exists:post_categories,id|max:4294967295|nullable',
            'video_link' => 'max:191|nullable'
        ];
    }

    

    public function getImageAttribute()
    {
        return $this->getFirstMedia('image');
    }

    /**
     * @return string
     */
    public function getImageLinkAttribute()
    {
        $file = $this->getFirstMedia('image');
        if (! $file) {
            return null;
        }

        return '<a href="' . $file->getUrl() . '" target="_blank">' . $file->file_name . '</a>';
    }
    
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id')->withTrashed();
    }
    
    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }
    
    public function tags()
    {
        return $this->belongsToMany(PostTag::class, 'post_post_tag')->withTrashed();
    }
    
    public function categories()
    {
        return $this->belongsToMany(PostCategory::class, 'post_post_category')->withTrashed();
    }
    
    
}
