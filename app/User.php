<?php
namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Notifications\ResetPassword;
use Carbon\Carbon;
use Hash;
use App\Traits\FilterByUser;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Laravel\Passport\HasApiTokens;

/**
 * Class User
 *
 * @package App
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property string $firstname
 * @property string $lastname
 * @property string $avatar
 * @property enum $newsletter
 * @property text $about
 * @property string $created_by
 * @property string $bill_paid_at
 * @property string $email_verified_at
 * @property string $address_one
 * @property string $address_two
 * @property string $postal_code
 * @property string $city
 * @property enum $status
 * @property string $country
 * @property string $language
 * @property string $currency
*/
class User extends Authenticatable implements HasMedia
{
    use Notifiable;
    use FilterByUser, HasMediaTrait, HasApiTokens;

    
    protected $fillable = ['name', 'email', 'password', 'remember_token', 'firstname', 'lastname', 'newsletter', 'about', 'bill_paid_at', 'email_verified_at', 'address_one', 'address_two', 'postal_code', 'city', 'status', 'created_by_id', 'country_id', 'language_id', 'currency_id'];
    protected $hidden = ['password', 'remember_token'];
    protected $appends = ['avatar', 'avatar_link'];
    protected $with = ['media'];

    public static function storeValidation($request)
    {
        return [
            'name' => 'max:191|required',
            'email' => 'email|max:191|required|unique:users,email',
            'password' => 'required',
            'role' => 'array|required',
            'role.*' => 'integer|exists:roles,id|max:4294967295|required',
            'remember_token' => 'max:191|nullable',
            'firstname' => 'max:191|nullable',
            'lastname' => 'max:191|nullable',
            'avatar' => 'file|image|nullable',
            'newsletter' => 'in:1,0|nullable',
            'about' => 'max:65535|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable',
            'bill_paid_at' => 'date_format:' . config('app.date_format') . '|max:191|nullable',
            'email_verified_at' => 'date_format:' . config('app.date_format') . '|max:191|nullable',
            'address_one' => 'max:191|nullable',
            'address_two' => 'max:191|nullable',
            'postal_code' => 'max:191|nullable',
            'city' => 'max:191|nullable',
            'status' => 'in:1,0|required',
            'country_id' => 'integer|exists:countries,id|max:4294967295|nullable',
            'language_id' => 'integer|exists:languages,id|max:4294967295|nullable',
            'currency_id' => 'integer|exists:currencies,id|max:4294967295|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'name' => 'max:191|required',
            'email' => 'email|max:191|required|unique:users,email,'.$request->route('user'),
            'password' => '',
            'role' => 'array|required',
            'role.*' => 'integer|exists:roles,id|max:4294967295|required',
            'remember_token' => 'max:191|nullable',
            'firstname' => 'max:191|nullable',
            'lastname' => 'max:191|nullable',
            'avatar' => 'nullable',
            'newsletter' => 'in:1,0|nullable',
            'about' => 'max:65535|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable',
            'bill_paid_at' => 'date_format:' . config('app.date_format') . '|max:191|nullable',
            'email_verified_at' => 'date_format:' . config('app.date_format') . '|max:191|nullable',
            'address_one' => 'max:191|nullable',
            'address_two' => 'max:191|nullable',
            'postal_code' => 'max:191|nullable',
            'city' => 'max:191|nullable',
            'status' => 'in:1,0|required',
            'country_id' => 'integer|exists:countries,id|max:4294967295|nullable',
            'language_id' => 'integer|exists:languages,id|max:4294967295|nullable',
            'currency_id' => 'integer|exists:currencies,id|max:4294967295|nullable'
        ];
    }

    
    

    public static function boot()
    {
        parent::boot();

        User::observe(new \App\Observers\UserActionsObserver);
    }
    

    public static $enum_newsletter = ["1" => "1", "0" => "0"];

    public static $enum_status = ["1" => "1", "0" => "0"];
    /**
     * Hash password
     * @param $input
     */
    public function setPasswordAttribute($input)
    {
        if ($input) {
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
        }
    }

    public function getAvatarAttribute()
    {
        return $this->getFirstMedia('avatar');
    }

    /**
     * @return string
     */
    public function getAvatarLinkAttribute()
    {
        $file = $this->getFirstMedia('avatar');
        if (! $file) {
            return null;
        }

        return '<a href="' . $file->getUrl() . '" target="_blank">' . $file->file_name . '</a>';
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setBillPaidAtAttribute($input)
    {
        if ($input) {
            $this->attributes['bill_paid_at'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        }
    }

    /**
     * Get attribute from date format
     * @param $output
     *
     * @return string
     */
    public function getBillPaidAtAttribute($output)
    {
        if ($output) {
            return Carbon::createFromFormat('Y-m-d', $output)->format(config('app.date_format'));
        }
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setEmailVerifiedAtAttribute($input)
    {
        if ($input) {
            $this->attributes['email_verified_at'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        }
    }

    /**
     * Get attribute from date format
     * @param $output
     *
     * @return string
     */
    public function getEmailVerifiedAtAttribute($output)
    {
        if ($output) {
            return Carbon::createFromFormat('Y-m-d', $output)->format(config('app.date_format'));
        }
    }
    
    public function role()
    {
        return $this->belongsToMany(Role::class, 'role_user');
    }
    
    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }
    
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id')->withTrashed();
    }
    
    public function language()
    {
        return $this->belongsTo(Language::class, 'language_id')->withTrashed();
    }

    public function account()
    {
        return $this->hasOne(Account::class);
    }
    
    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id')->withTrashed();
    }
    
    
    

    public function sendPasswordResetNotification($token)
    {
       $this->notify(new ResetPassword($token));
    }
}
