<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FilterByUser;

/**
 * Class PackageItem
 *
 * @package App
 * @property string $package
 * @property string $name
 * @property text $description
 * @property decimal $price
 * @property enum $active
 * @property string $created_by
*/
class PackageItem extends Model
{
    use SoftDeletes, FilterByUser;

    
    protected $fillable = ['name', 'description', 'price', 'active', 'package_id', 'created_by_id'];
    

    public static $enum_active = ["1" => "1", "0" => "0"];

    public static function boot()
    {
        parent::boot();

        PackageItem::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'package_id' => 'integer|exists:packages,id|max:4294967295|required',
            'name' => 'max:191|nullable',
            'description' => 'max:65535|required',
            'price' => 'numeric|required',
            'active' => 'in:1,0|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'package_id' => 'integer|exists:packages,id|max:4294967295|required',
            'name' => 'max:191|nullable',
            'description' => 'max:65535|required',
            'price' => 'numeric|required',
            'active' => 'in:1,0|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    

    
    
    public function package()
    {
        return $this->belongsTo(Package::class, 'package_id')->withTrashed();
    }
    
    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }
    
    
}
