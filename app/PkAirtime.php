<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PkAirtime
 *
 * @package App
 * @property string $pay_konnect_transaction_id
 * @property string $pds_transaction_id
 * @property string $time
 * @property string $response_code
 * @property string $response_message
 * @property string $transaction
*/
class PkAirtime extends Model
{
    use SoftDeletes;

    
    protected $fillable = ['pay_konnect_transaction_id', 'pds_transaction_id', 'time', 'response_code', 'response_message', 'transaction_id'];
    

    public static function boot()
    {
        parent::boot();

        PkAirtime::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'pay_konnect_transaction_id' => 'max:191|nullable',
            'pds_transaction_id' => 'max:191|nullable',
            'time' => 'max:191|nullable',
            'response_code' => 'max:191|nullable',
            'response_message' => 'max:191|nullable',
            'transaction_id' => 'integer|exists:transactions,id|max:4294967295|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'pay_konnect_transaction_id' => 'max:191|nullable',
            'pds_transaction_id' => 'max:191|nullable',
            'time' => 'max:191|nullable',
            'response_code' => 'max:191|nullable',
            'response_message' => 'max:191|nullable',
            'transaction_id' => 'integer|exists:transactions,id|max:4294967295|nullable'
        ];
    }

    

    
    
    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id')->withTrashed();
    }
    
    
}
