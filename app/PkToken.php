<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PkToken
 *
 * @package App
 * @property string $pay_konnect_transaction_id
 * @property string $pds_transaction_id
 * @property string $time
 * @property string $response_code
 * @property string $response_message
 * @property string $transaction
 * @property string $token
 * @property string $units
 * @property string $bssttoken
 * @property string $bsst_token_units
*/
class PkToken extends Model
{
    use SoftDeletes;

    
    protected $fillable = ['pay_konnect_transaction_id', 'pds_transaction_id', 'time', 'response_code', 'response_message', 'token', 'units', 'bssttoken', 'bsst_token_units', 'transaction_id'];
    

    public static function boot()
    {
        parent::boot();

        PkToken::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'pay_konnect_transaction_id' => 'max:191|nullable',
            'pds_transaction_id' => 'max:191|nullable',
            'time' => 'max:191|nullable',
            'response_code' => 'max:191|nullable',
            'response_message' => 'max:191|nullable',
            'transaction_id' => 'integer|exists:transactions,id|max:4294967295|nullable',
            'token' => 'max:191|nullable',
            'units' => 'max:191|nullable',
            'bssttoken' => 'max:191|nullable',
            'bsst_token_units' => 'max:191|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'pay_konnect_transaction_id' => 'max:191|nullable',
            'pds_transaction_id' => 'max:191|nullable',
            'time' => 'max:191|nullable',
            'response_code' => 'max:191|nullable',
            'response_message' => 'max:191|nullable',
            'transaction_id' => 'integer|exists:transactions,id|max:4294967295|nullable',
            'token' => 'max:191|nullable',
            'units' => 'max:191|nullable',
            'bssttoken' => 'max:191|nullable',
            'bsst_token_units' => 'max:191|nullable'
        ];
    }

    

    
    
    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id')->withTrashed();
    }
    
    
}
