<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class CrmDocument
 *
 * @package App
 * @property string $customer
 * @property string $name
 * @property text $description
 * @property string $file
*/
class CrmDocument extends Model implements HasMedia
{
    use HasMediaTrait;

    
    protected $fillable = ['name', 'description', 'customer_id'];
    protected $appends = ['file', 'file_link'];
    protected $with = ['media'];
    

    public static function boot()
    {
        parent::boot();

        CrmDocument::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'customer_id' => 'integer|exists:crm_customers,id|max:4294967295|required',
            'name' => 'max:191|nullable',
            'description' => 'max:65535|nullable',
            'file' => 'file|required'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'customer_id' => 'integer|exists:crm_customers,id|max:4294967295|required',
            'name' => 'max:191|nullable',
            'description' => 'max:65535|nullable',
            'file' => 'nullable'
        ];
    }

    

    public function getFileAttribute()
    {
        return $this->getFirstMedia('file');
    }

    /**
     * @return string
     */
    public function getFileLinkAttribute()
    {
        $file = $this->getFirstMedia('file');
        if (! $file) {
            return null;
        }

        return '<a href="' . $file->getUrl() . '" target="_blank">' . $file->file_name . '</a>';
    }
    
    public function customer()
    {
        return $this->belongsTo(CrmCustomer::class, 'customer_id');
    }
    
    
}
