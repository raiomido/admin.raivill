<?php
/**
 * Created by PhpStorm.
 * User: raiomido
 * Date: 2/11/19
 * Time: 3:19 PM
 */

namespace App\Http\Traits;


use App\Order;
use App\Post;
use App\Product;
use App\Transaction;

trait Util
{
    public function getDefaultImage() {
        return 'data:image/svg+xml;charset=utf8,<svg height="512pt" viewBox="0 -80 512 512" width="512pt" xmlns="http://www.w3.org/2000/svg"><path d="m512 15.113281v322.378907c0 8.347656-6.761719 15.109374-15.113281 15.109374h-481.773438c-8.351562 0-15.113281-6.761718-15.113281-15.109374v-322.378907c0-8.351562 6.761719-15.113281 15.113281-15.113281h481.773438c8.351562 0 15.113281 6.761719 15.113281 15.113281zm0 0" fill="#edf0f5"/><path d="m512 15.113281v322.378907c0 8.347656-6.761719 15.109374-15.113281 15.109374h-240.886719v-352.601562h240.886719c8.351562 0 15.113281 6.761719 15.113281 15.113281zm0 0" fill="#d8dceb"/><path d="m481.785156 40.296875v272.007813c0 5.570312-4.511718 10.082031-10.082031 10.082031h-431.40625c-5.570313 0-10.082031-4.511719-10.082031-10.082031v-272.007813c0-5.570313 4.511718-10.082031 10.082031-10.082031h431.40625c5.570313 0 10.082031 4.511718 10.082031 10.082031zm0 0" fill="#2682ff"/><path d="m481.785156 40.296875v272.007813c0 5.570312-4.511718 10.082031-10.082031 10.082031h-215.703125v-292.171875h215.703125c5.570313 0 10.082031 4.511718 10.082031 10.082031zm0 0" fill="#046eff"/><path d="m481.773438 248.597656v63.707032c0 5.484374-4.429688 10.082031-10.070313 10.082031h-365.558594l149.855469-148.894531 61.804688-61.410157c5.816406-5.785156 15.175781-5.863281 21.101562-.203125zm0 0" fill="#00cc76"/><path d="m481.773438 248.597656v63.707032c0 5.484374-4.429688 10.082031-10.070313 10.082031h-215.703125v-148.894531l61.804688-61.410157c5.816406-5.785156 15.175781-5.863281 21.101562-.203125zm0 0" fill="#00aa63"/><path d="m275.105469 322.386719h-234.808594c-5.558594 0-10.070313-4.511719-10.070313-10.082031v-48.1875l82.621094-82.621094c5.953125-5.957032 15.519532-5.859375 21.375 0l121.777344 121.789062zm0 0" fill="#00ff94"/><path d="m275.105469 322.386719h-19.105469v-19.101563zm0 0" fill="#00dd80"/><path d="m229.09375 110.535156c0 24.859375-20.152344 45-45 45-24.859375 0-45.003906-20.140625-45.003906-45s20.144531-45.003906 45.003906-45.003906c24.847656 0 45 20.144531 45 45.003906zm0 0" fill="#ffe477"/><path d="m229.09375 110.535156c0 24.859375-20.152344 45-45 45v-90.003906c24.847656 0 45 20.144531 45 45.003906zm0 0" fill="#ffcd00"/></svg>';
    }

    protected function generateTransactionReference(Order $order) {
        $randomStr = str_random(1);
        $randomInt = random_int(0,10000);
        $randomInt = preg_replace("/[01]/", "",$randomInt);
        $padLen = (8- (strlen($order->id.$randomInt)));
        $orderCode = str_pad($randomInt, $padLen, $randomStr,STR_PAD_LEFT);
        return strtoupper('RC'.now()->month.substr(now()->year, -2).$order->id.$orderCode);
    }

    protected function generateAccountNumber($business = false) {
        $prefix = 'RAC';
        if($business) {
            $prefix = 'RBN';
        }
        $prefix .= now()->day;
        $randomStr = str_random(1);
        $randomInt = random_int(0,10000);
        $padLen = (8- (strlen($randomInt)));
        $accCode = str_pad($randomInt, $padLen, $randomStr,STR_PAD_LEFT);
        return strtoupper($prefix.now()->month.substr(now()->year, -2).$accCode);
    }

    protected function generateOrderReference(Product $product) {
        $randomStr = str_random(1);
        $randomInt = random_int(0,10000);
        $randomInt = preg_replace("/[01]/", "",$randomInt);
        $padLen = (8- (strlen($product->id.$randomInt)));
        $orderCode = str_pad($randomInt, $padLen, $randomStr,STR_PAD_LEFT);
        return strtoupper('RC'.now()->month.substr(now()->year, -2).$product->id.$orderCode);
    }

    protected function generateProductReference() {
        $randomStr = str_random(1);
        $randomInt = random_int(0,10000);
        $randomInt = preg_replace("/[01]/", "",$randomInt);
        $padLen = (8- (strlen(now()->day.$randomInt)));
        $orderCode = str_pad($randomInt, $padLen, $randomStr,STR_PAD_LEFT);
        return strtoupper('RP'.now()->month.substr(now()->year, -2).now()->day.$orderCode);
    }

    protected function createPostProduct(Post $post) {
        return Product::create([
            'name' => ($name = $post->title.' PDF'),
            'slug' => str_slug($name),
            'price' => 700,
            'link' => url('/'),
            'summary' => $name,
            'description' => $name,
            'reference' => $this->generateProductReference($post),
            'category_id' => 1,
            'created_by_id' => 1,
            'supplier_id' => 1
        ]);
    }
}