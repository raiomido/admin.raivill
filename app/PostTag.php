<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FilterByUser;

/**
 * Class PostTag
 *
 * @package App
 * @property string $name
 * @property string $slug
 * @property enum $active
 * @property string $created_by
*/
class PostTag extends Model
{
    use SoftDeletes, FilterByUser;

    
    protected $fillable = ['name', 'slug', 'active', 'created_by_id'];
    

    public static $enum_active = ["1" => "1", "0" => "0"];

    public static function boot()
    {
        parent::boot();

        PostTag::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'name' => 'max:191|required',
            'slug' => 'max:191|required|unique:post_tags,slug',
            'active' => 'in:1,0|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'name' => 'max:191|required',
            'slug' => 'max:191|required|unique:post_tags,slug,'.$request->route('post_tag'),
            'active' => 'in:1,0|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    

    
    
    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }
    
    
}
