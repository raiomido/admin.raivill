<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FilterByUser;

/**
 * Class Currency
 *
 * @package App
 * @property string $title
 * @property string $code
 * @property string $symbol_left
 * @property string $symbol_right
 * @property integer $decimal_place
 * @property integer $value
 * @property enum $active
 * @property string $created_by
*/
class Currency extends Model
{
    use SoftDeletes, FilterByUser;

    
    protected $fillable = ['title', 'code', 'symbol_left', 'symbol_right', 'decimal_place', 'value', 'active', 'created_by_id'];
    

    public static $enum_active = ["1" => "1", "0" => "0"];

    public static function boot()
    {
        parent::boot();

        Currency::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'code' => 'max:191|required',
            'symbol_left' => 'max:191|nullable',
            'symbol_right' => 'max:191|nullable',
            'decimal_place' => 'integer|max:9|nullable',
            'value' => 'integer|max:4294967295|nullable',
            'active' => 'in:1,0|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'code' => 'max:191|required',
            'symbol_left' => 'max:191|nullable',
            'symbol_right' => 'max:191|nullable',
            'decimal_place' => 'integer|max:9|nullable',
            'value' => 'integer|max:4294967295|nullable',
            'active' => 'in:1,0|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable'
        ];
    }

    

    
    
    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }
    
    
}
