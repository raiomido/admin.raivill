function initialState() {
    return {
        item: {
            id: null,
            chat: null,
            user: null,
            message: null,
            active: null,
            created_by: null,
            email: null,
            phone: null,
        },
        chatsAll: [],
        usersAll: [],
        usersAll: [],
        activeEnum: [ { value: '1', label: '1' }, { value: '0', label: '0' }, ],
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    chatsAll: state => state.chatsAll,
    usersAll: state => state.usersAll,
    usersAll: state => state.usersAll,
    activeEnum: state => state.activeEnum,
}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.chat)) {
                params.set('chat_id', '')
            } else {
                params.set('chat_id', state.item.chat.id)
            }
            if (_.isEmpty(state.item.user)) {
                params.set('user_id', '')
            } else {
                params.set('user_id', state.item.user.id)
            }
            if (! _.isEmpty(state.item.active) && typeof state.item.active === 'object') {
                params.set('active', state.item.active.value)
            }
            if (_.isEmpty(state.item.created_by)) {
                params.set('created_by_id', '')
            } else {
                params.set('created_by_id', state.item.created_by.id)
            }

            axios.post('/api/v1/messages', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.chat)) {
                params.set('chat_id', '')
            } else {
                params.set('chat_id', state.item.chat.id)
            }
            if (_.isEmpty(state.item.user)) {
                params.set('user_id', '')
            } else {
                params.set('user_id', state.item.user.id)
            }
            if (! _.isEmpty(state.item.active) && typeof state.item.active === 'object') {
                params.set('active', state.item.active.value)
            }
            if (_.isEmpty(state.item.created_by)) {
                params.set('created_by_id', '')
            } else {
                params.set('created_by_id', state.item.created_by.id)
            }

            axios.post('/api/v1/messages/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/messages/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        dispatch('fetchChatsAll')
    dispatch('fetchUsersAll')
    dispatch('fetchUsersAll')
    },
    fetchChatsAll({ commit }) {
        axios.get('/api/v1/chats')
            .then(response => {
                commit('setChatsAll', response.data.data)
            })
    },
    fetchUsersAll({ commit }) {
        axios.get('/api/v1/users')
            .then(response => {
                commit('setUsersAll', response.data.data)
            })
    },
    fetchUsersAll({ commit }) {
        axios.get('/api/v1/users')
            .then(response => {
                commit('setUsersAll', response.data.data)
            })
    },
    setChat({ commit }, value) {
        commit('setChat', value)
    },
    setUser({ commit }, value) {
        commit('setUser', value)
    },
    setMessage({ commit }, value) {
        commit('setMessage', value)
    },
    setActive({ commit }, value) {
        commit('setActive', value)
    },
    setCreated_by({ commit }, value) {
        commit('setCreated_by', value)
    },
    setEmail({ commit }, value) {
        commit('setEmail', value)
    },
    setPhone({ commit }, value) {
        commit('setPhone', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setChat(state, value) {
        state.item.chat = value
    },
    setUser(state, value) {
        state.item.user = value
    },
    setMessage(state, value) {
        state.item.message = value
    },
    setActive(state, value) {
        state.item.active = value
    },
    setCreated_by(state, value) {
        state.item.created_by = value
    },
    setEmail(state, value) {
        state.item.email = value
    },
    setPhone(state, value) {
        state.item.phone = value
    },
    setChatsAll(state, value) {
        state.chatsAll = value
    },
    setUsersAll(state, value) {
        state.usersAll = value
    },
    setUsersAll(state, value) {
        state.usersAll = value
    },
    setActiveEnum(state, value) {
        state.activeEnum = value
    },
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
