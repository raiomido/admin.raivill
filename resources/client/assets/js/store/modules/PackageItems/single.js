function initialState() {
    return {
        item: {
            id: null,
            package: null,
            name: null,
            description: null,
            price: null,
            active: null,
            created_by: null,
        },
        packagesAll: [],
        usersAll: [],
        activeEnum: [ { value: '1', label: '1' }, { value: '0', label: '0' }, ],
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    packagesAll: state => state.packagesAll,
    usersAll: state => state.usersAll,
    activeEnum: state => state.activeEnum,
}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.package)) {
                params.set('package_id', '')
            } else {
                params.set('package_id', state.item.package.id)
            }
            if (! _.isEmpty(state.item.active) && typeof state.item.active === 'object') {
                params.set('active', state.item.active.value)
            }
            if (_.isEmpty(state.item.created_by)) {
                params.set('created_by_id', '')
            } else {
                params.set('created_by_id', state.item.created_by.id)
            }

            axios.post('/api/v1/package-items', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.package)) {
                params.set('package_id', '')
            } else {
                params.set('package_id', state.item.package.id)
            }
            if (! _.isEmpty(state.item.active) && typeof state.item.active === 'object') {
                params.set('active', state.item.active.value)
            }
            if (_.isEmpty(state.item.created_by)) {
                params.set('created_by_id', '')
            } else {
                params.set('created_by_id', state.item.created_by.id)
            }

            axios.post('/api/v1/package-items/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/package-items/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        dispatch('fetchPackagesAll')
    dispatch('fetchUsersAll')
    },
    fetchPackagesAll({ commit }) {
        axios.get('/api/v1/packages')
            .then(response => {
                commit('setPackagesAll', response.data.data)
            })
    },
    fetchUsersAll({ commit }) {
        axios.get('/api/v1/users')
            .then(response => {
                commit('setUsersAll', response.data.data)
            })
    },
    setPackage({ commit }, value) {
        commit('setPackage', value)
    },
    setName({ commit }, value) {
        commit('setName', value)
    },
    setDescription({ commit }, value) {
        commit('setDescription', value)
    },
    setPrice({ commit }, value) {
        commit('setPrice', value)
    },
    setActive({ commit }, value) {
        commit('setActive', value)
    },
    setCreated_by({ commit }, value) {
        commit('setCreated_by', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setPackage(state, value) {
        state.item.package = value
    },
    setName(state, value) {
        state.item.name = value
    },
    setDescription(state, value) {
        state.item.description = value
    },
    setPrice(state, value) {
        state.item.price = value
    },
    setActive(state, value) {
        state.item.active = value
    },
    setCreated_by(state, value) {
        state.item.created_by = value
    },
    setPackagesAll(state, value) {
        state.packagesAll = value
    },
    setUsersAll(state, value) {
        state.usersAll = value
    },
    setActiveEnum(state, value) {
        state.activeEnum = value
    },
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
