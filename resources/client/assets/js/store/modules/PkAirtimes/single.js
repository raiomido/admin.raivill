function initialState() {
    return {
        item: {
            id: null,
            pay_konnect_transaction_id: null,
            pds_transaction_id: null,
            time: null,
            response_code: null,
            response_message: null,
            transaction: null,
        },
        transactionsAll: [],
        
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    transactionsAll: state => state.transactionsAll,
    
}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.transaction)) {
                params.set('transaction_id', '')
            } else {
                params.set('transaction_id', state.item.transaction.id)
            }

            axios.post('/api/v1/pk-airtimes', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.transaction)) {
                params.set('transaction_id', '')
            } else {
                params.set('transaction_id', state.item.transaction.id)
            }

            axios.post('/api/v1/pk-airtimes/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/pk-airtimes/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        dispatch('fetchTransactionsAll')
    },
    fetchTransactionsAll({ commit }) {
        axios.get('/api/v1/transactions')
            .then(response => {
                commit('setTransactionsAll', response.data.data)
            })
    },
    setPay_konnect_transaction_id({ commit }, value) {
        commit('setPay_konnect_transaction_id', value)
    },
    setPds_transaction_id({ commit }, value) {
        commit('setPds_transaction_id', value)
    },
    setTime({ commit }, value) {
        commit('setTime', value)
    },
    setResponse_code({ commit }, value) {
        commit('setResponse_code', value)
    },
    setResponse_message({ commit }, value) {
        commit('setResponse_message', value)
    },
    setTransaction({ commit }, value) {
        commit('setTransaction', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setPay_konnect_transaction_id(state, value) {
        state.item.pay_konnect_transaction_id = value
    },
    setPds_transaction_id(state, value) {
        state.item.pds_transaction_id = value
    },
    setTime(state, value) {
        state.item.time = value
    },
    setResponse_code(state, value) {
        state.item.response_code = value
    },
    setResponse_message(state, value) {
        state.item.response_message = value
    },
    setTransaction(state, value) {
        state.item.transaction = value
    },
    setTransactionsAll(state, value) {
        state.transactionsAll = value
    },
    
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
