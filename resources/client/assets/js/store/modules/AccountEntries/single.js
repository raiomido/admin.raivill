function initialState() {
    return {
        item: {
            id: null,
            reference: null,
            type: null,
            balance_before: null,
            balance_after: null,
            amount: null,
            account: null,
        },
        accountsAll: [],
        typeEnum: [ { value: 'DEBIT', label: 'DEBIT' }, { value: 'CREDIT', label: 'CREDIT' }, ],
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    accountsAll: state => state.accountsAll,
    typeEnum: state => state.typeEnum,
}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (! _.isEmpty(state.item.type) && typeof state.item.type === 'object') {
                params.set('type', state.item.type.value)
            }
            if (_.isEmpty(state.item.account)) {
                params.set('account_id', '')
            } else {
                params.set('account_id', state.item.account.id)
            }

            axios.post('/api/v1/account-entries', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (! _.isEmpty(state.item.type) && typeof state.item.type === 'object') {
                params.set('type', state.item.type.value)
            }
            if (_.isEmpty(state.item.account)) {
                params.set('account_id', '')
            } else {
                params.set('account_id', state.item.account.id)
            }

            axios.post('/api/v1/account-entries/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/account-entries/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        dispatch('fetchAccountsAll')
    },
    fetchAccountsAll({ commit }) {
        axios.get('/api/v1/accounts')
            .then(response => {
                commit('setAccountsAll', response.data.data)
            })
    },
    setReference({ commit }, value) {
        commit('setReference', value)
    },
    setType({ commit }, value) {
        commit('setType', value)
    },
    setBalance_before({ commit }, value) {
        commit('setBalance_before', value)
    },
    setBalance_after({ commit }, value) {
        commit('setBalance_after', value)
    },
    setAmount({ commit }, value) {
        commit('setAmount', value)
    },
    setAccount({ commit }, value) {
        commit('setAccount', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setReference(state, value) {
        state.item.reference = value
    },
    setType(state, value) {
        state.item.type = value
    },
    setBalance_before(state, value) {
        state.item.balance_before = value
    },
    setBalance_after(state, value) {
        state.item.balance_after = value
    },
    setAmount(state, value) {
        state.item.amount = value
    },
    setAccount(state, value) {
        state.item.account = value
    },
    setAccountsAll(state, value) {
        state.accountsAll = value
    },
    setTypeEnum(state, value) {
        state.typeEnum = value
    },
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
