function initialState() {
    return {
        item: {
            id: null,
            name: null,
            slug: null,
            images: [],
            uploaded_images: [],
            price: null,
            summary: null,
            description: null,
            views: null,
            likes_count: null,
            comments_count: null,
            category: null,
            created_by: null,
            supplier: null,
            reference: null,
            file: null,
            link: null,
        },
        productcategoriesAll: [],
        usersAll: [],
        productsuppliersAll: [],
        
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    productcategoriesAll: state => state.productcategoriesAll,
    usersAll: state => state.usersAll,
    productsuppliersAll: state => state.productsuppliersAll,
    
}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            params.set('uploaded_images', state.item.uploaded_images.map(o => o['id']))
            if (_.isEmpty(state.item.category)) {
                params.set('category_id', '')
            } else {
                params.set('category_id', state.item.category.id)
            }
            if (_.isEmpty(state.item.created_by)) {
                params.set('created_by_id', '')
            } else {
                params.set('created_by_id', state.item.created_by.id)
            }
            if (_.isEmpty(state.item.supplier)) {
                params.set('supplier_id', '')
            } else {
                params.set('supplier_id', state.item.supplier.id)
            }
            if (state.item.file === null) {
                params.delete('file');
            }

            axios.post('/api/v1/products', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            params.set('uploaded_images', state.item.uploaded_images.map(o => o['id']))
            if (_.isEmpty(state.item.category)) {
                params.set('category_id', '')
            } else {
                params.set('category_id', state.item.category.id)
            }
            if (_.isEmpty(state.item.created_by)) {
                params.set('created_by_id', '')
            } else {
                params.set('created_by_id', state.item.created_by.id)
            }
            if (_.isEmpty(state.item.supplier)) {
                params.set('supplier_id', '')
            } else {
                params.set('supplier_id', state.item.supplier.id)
            }
            if (state.item.file === null) {
                params.delete('file');
            }

            axios.post('/api/v1/products/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/products/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        dispatch('fetchProductcategoriesAll')
    dispatch('fetchUsersAll')
    dispatch('fetchProductsuppliersAll')
    },
    fetchProductcategoriesAll({ commit }) {
        axios.get('/api/v1/product-categories')
            .then(response => {
                commit('setProductcategoriesAll', response.data.data)
            })
    },
    fetchUsersAll({ commit }) {
        axios.get('/api/v1/users')
            .then(response => {
                commit('setUsersAll', response.data.data)
            })
    },
    fetchProductsuppliersAll({ commit }) {
        axios.get('/api/v1/product-suppliers')
            .then(response => {
                commit('setProductsuppliersAll', response.data.data)
            })
    },
    setName({ commit }, value) {
        commit('setName', value)
    },
    setSlug({ commit }, value) {
        commit('setSlug', value)
    },
    setImages({ commit }, value) {
        commit('setImages', value)
    },
    destroyImages({ commit }, value) {
        commit('destroyImages', value)
    },
    destroyUploadedImages({ commit }, value) {
        commit('destroyUploadedImages', value)
    },
    setPrice({ commit }, value) {
        commit('setPrice', value)
    },
    setSummary({ commit }, value) {
        commit('setSummary', value)
    },
    setDescription({ commit }, value) {
        commit('setDescription', value)
    },
    setViews({ commit }, value) {
        commit('setViews', value)
    },
    setLikes_count({ commit }, value) {
        commit('setLikes_count', value)
    },
    setComments_count({ commit }, value) {
        commit('setComments_count', value)
    },
    setCategory({ commit }, value) {
        commit('setCategory', value)
    },
    setCreated_by({ commit }, value) {
        commit('setCreated_by', value)
    },
    setSupplier({ commit }, value) {
        commit('setSupplier', value)
    },
    setReference({ commit }, value) {
        commit('setReference', value)
    },
    setFile({ commit }, value) {
        commit('setFile', value)
    },
    
    setLink({ commit }, value) {
        commit('setLink', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setName(state, value) {
        state.item.name = value
    },
    setSlug(state, value) {
        state.item.slug = value
    },
    setImages(state, value) {
        for (let i in value) {
            let images = value[i];
            if (typeof images === "object") {
                state.item.images.push(images);
            }
        }
    },
    destroyImages(state, value) {
        for (let i in state.item.images) {
            if (i == value) {
                state.item.images.splice(i, 1);
            }
        }
    },
    destroyUploadedImages(state, value) {
        for (let i in state.item.uploaded_images) {
            let data = state.item.uploaded_images[i];
            if (data.id === value) {
                state.item.uploaded_images.splice(i, 1);
            }
        }
    },
    setPrice(state, value) {
        state.item.price = value
    },
    setSummary(state, value) {
        state.item.summary = value
    },
    setDescription(state, value) {
        state.item.description = value
    },
    setViews(state, value) {
        state.item.views = value
    },
    setLikes_count(state, value) {
        state.item.likes_count = value
    },
    setComments_count(state, value) {
        state.item.comments_count = value
    },
    setCategory(state, value) {
        state.item.category = value
    },
    setCreated_by(state, value) {
        state.item.created_by = value
    },
    setSupplier(state, value) {
        state.item.supplier = value
    },
    setReference(state, value) {
        state.item.reference = value
    },
    setFile(state, value) {
        state.item.file = value
    },
    setLink(state, value) {
        state.item.link = value
    },
    setProductcategoriesAll(state, value) {
        state.productcategoriesAll = value
    },
    setUsersAll(state, value) {
        state.usersAll = value
    },
    setProductsuppliersAll(state, value) {
        state.productsuppliersAll = value
    },
    
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
