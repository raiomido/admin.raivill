function initialState() {
    return {
        item: {
            id: null,
            reference: null,
            type: null,
            balance_before: null,
            balance_after: null,
            amount: null,
            raivill_account: null,
        },
        raivillaccountsAll: [],
        typeEnum: [ { value: 'DEBIT', label: 'DEBIT' }, { value: 'CREDIT', label: 'CREDIT' }, ],
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    raivillaccountsAll: state => state.raivillaccountsAll,
    typeEnum: state => state.typeEnum,
}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (! _.isEmpty(state.item.type) && typeof state.item.type === 'object') {
                params.set('type', state.item.type.value)
            }
            if (_.isEmpty(state.item.raivill_account)) {
                params.set('raivill_account_id', '')
            } else {
                params.set('raivill_account_id', state.item.raivill_account.id)
            }

            axios.post('/api/v1/raivill-account-entries', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (! _.isEmpty(state.item.type) && typeof state.item.type === 'object') {
                params.set('type', state.item.type.value)
            }
            if (_.isEmpty(state.item.raivill_account)) {
                params.set('raivill_account_id', '')
            } else {
                params.set('raivill_account_id', state.item.raivill_account.id)
            }

            axios.post('/api/v1/raivill-account-entries/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/raivill-account-entries/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        dispatch('fetchRaivillaccountsAll')
    },
    fetchRaivillaccountsAll({ commit }) {
        axios.get('/api/v1/raivill-accounts')
            .then(response => {
                commit('setRaivillaccountsAll', response.data.data)
            })
    },
    setReference({ commit }, value) {
        commit('setReference', value)
    },
    setType({ commit }, value) {
        commit('setType', value)
    },
    setBalance_before({ commit }, value) {
        commit('setBalance_before', value)
    },
    setBalance_after({ commit }, value) {
        commit('setBalance_after', value)
    },
    setAmount({ commit }, value) {
        commit('setAmount', value)
    },
    setRaivill_account({ commit }, value) {
        commit('setRaivill_account', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setReference(state, value) {
        state.item.reference = value
    },
    setType(state, value) {
        state.item.type = value
    },
    setBalance_before(state, value) {
        state.item.balance_before = value
    },
    setBalance_after(state, value) {
        state.item.balance_after = value
    },
    setAmount(state, value) {
        state.item.amount = value
    },
    setRaivill_account(state, value) {
        state.item.raivill_account = value
    },
    setRaivillaccountsAll(state, value) {
        state.raivillaccountsAll = value
    },
    setTypeEnum(state, value) {
        state.typeEnum = value
    },
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
