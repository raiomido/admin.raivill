function initialState() {
    return {
        item: {
            id: null,
            user: null,
            transaction: null,
            type: null,
            time: null,
            mpesa_transaction_id: null,
            amount: null,
            short_code: null,
            bill_ref_number: null,
            invoice_number: null,
            msisdn: null,
            firstname: null,
            middlename: null,
            lastname: null,
            org_account_balance: null,
        },
        usersAll: [],
        transactionsAll: [],
        
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    usersAll: state => state.usersAll,
    transactionsAll: state => state.transactionsAll,
    
}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.user)) {
                params.set('user_id', '')
            } else {
                params.set('user_id', state.item.user.id)
            }
            if (_.isEmpty(state.item.transaction)) {
                params.set('transaction_id', '')
            } else {
                params.set('transaction_id', state.item.transaction.id)
            }

            axios.post('/api/v1/mpesas', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.user)) {
                params.set('user_id', '')
            } else {
                params.set('user_id', state.item.user.id)
            }
            if (_.isEmpty(state.item.transaction)) {
                params.set('transaction_id', '')
            } else {
                params.set('transaction_id', state.item.transaction.id)
            }

            axios.post('/api/v1/mpesas/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/mpesas/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        dispatch('fetchUsersAll')
    dispatch('fetchTransactionsAll')
    },
    fetchUsersAll({ commit }) {
        axios.get('/api/v1/users')
            .then(response => {
                commit('setUsersAll', response.data.data)
            })
    },
    fetchTransactionsAll({ commit }) {
        axios.get('/api/v1/transactions')
            .then(response => {
                commit('setTransactionsAll', response.data.data)
            })
    },
    setUser({ commit }, value) {
        commit('setUser', value)
    },
    setTransaction({ commit }, value) {
        commit('setTransaction', value)
    },
    setType({ commit }, value) {
        commit('setType', value)
    },
    setTime({ commit }, value) {
        commit('setTime', value)
    },
    setMpesa_transaction_id({ commit }, value) {
        commit('setMpesa_transaction_id', value)
    },
    setAmount({ commit }, value) {
        commit('setAmount', value)
    },
    setShort_code({ commit }, value) {
        commit('setShort_code', value)
    },
    setBill_ref_number({ commit }, value) {
        commit('setBill_ref_number', value)
    },
    setInvoice_number({ commit }, value) {
        commit('setInvoice_number', value)
    },
    setMsisdn({ commit }, value) {
        commit('setMsisdn', value)
    },
    setFirstname({ commit }, value) {
        commit('setFirstname', value)
    },
    setMiddlename({ commit }, value) {
        commit('setMiddlename', value)
    },
    setLastname({ commit }, value) {
        commit('setLastname', value)
    },
    setOrg_account_balance({ commit }, value) {
        commit('setOrg_account_balance', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setUser(state, value) {
        state.item.user = value
    },
    setTransaction(state, value) {
        state.item.transaction = value
    },
    setType(state, value) {
        state.item.type = value
    },
    setTime(state, value) {
        state.item.time = value
    },
    setMpesa_transaction_id(state, value) {
        state.item.mpesa_transaction_id = value
    },
    setAmount(state, value) {
        state.item.amount = value
    },
    setShort_code(state, value) {
        state.item.short_code = value
    },
    setBill_ref_number(state, value) {
        state.item.bill_ref_number = value
    },
    setInvoice_number(state, value) {
        state.item.invoice_number = value
    },
    setMsisdn(state, value) {
        state.item.msisdn = value
    },
    setFirstname(state, value) {
        state.item.firstname = value
    },
    setMiddlename(state, value) {
        state.item.middlename = value
    },
    setLastname(state, value) {
        state.item.lastname = value
    },
    setOrg_account_balance(state, value) {
        state.item.org_account_balance = value
    },
    setUsersAll(state, value) {
        state.usersAll = value
    },
    setTransactionsAll(state, value) {
        state.transactionsAll = value
    },
    
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
