function initialState() {
    return {
        item: {
            id: null,
            product: null,
            reference: null,
            status: null,
            payment_completed_at: null,
            transactions: [],
            active: null,
            created_by: null,
        },
        productsAll: [],
        transactionsAll: [],
        usersAll: [],
        statusEnum: [ { value: 'PENDING_PAYMENT', label: 'PENDING_PAYMENT' }, { value: 'COMPLETED', label: 'COMPLETED' }, { value: 'PROCESSING', label: 'PROCESSING' }, { value: 'ON_HOLD', label: 'ON_HOLD' }, { value: 'CANCELLED', label: 'CANCELLED' }, { value: 'REFUNDED', label: 'REFUNDED' }, ],
        activeEnum: [ { value: '1', label: '1' }, { value: '0', label: '0' }, ],
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    productsAll: state => state.productsAll,
    transactionsAll: state => state.transactionsAll,
    usersAll: state => state.usersAll,
    statusEnum: state => state.statusEnum,
    activeEnum: state => state.activeEnum,
}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.product)) {
                params.set('product_id', '')
            } else {
                params.set('product_id', state.item.product.id)
            }
            if (! _.isEmpty(state.item.status) && typeof state.item.status === 'object') {
                params.set('status', state.item.status.value)
            }
            if (_.isEmpty(state.item.transactions)) {
                params.delete('transactions')
            } else {
                for (let index in state.item.transactions) {
                    params.set('transactions['+index+']', state.item.transactions[index].id)
                }
            }
            if (! _.isEmpty(state.item.active) && typeof state.item.active === 'object') {
                params.set('active', state.item.active.value)
            }
            if (_.isEmpty(state.item.created_by)) {
                params.set('created_by_id', '')
            } else {
                params.set('created_by_id', state.item.created_by.id)
            }

            axios.post('/api/v1/orders', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.product)) {
                params.set('product_id', '')
            } else {
                params.set('product_id', state.item.product.id)
            }
            if (! _.isEmpty(state.item.status) && typeof state.item.status === 'object') {
                params.set('status', state.item.status.value)
            }
            if (_.isEmpty(state.item.transactions)) {
                params.delete('transactions')
            } else {
                for (let index in state.item.transactions) {
                    params.set('transactions['+index+']', state.item.transactions[index].id)
                }
            }
            if (! _.isEmpty(state.item.active) && typeof state.item.active === 'object') {
                params.set('active', state.item.active.value)
            }
            if (_.isEmpty(state.item.created_by)) {
                params.set('created_by_id', '')
            } else {
                params.set('created_by_id', state.item.created_by.id)
            }

            axios.post('/api/v1/orders/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/orders/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        dispatch('fetchProductsAll')
    dispatch('fetchTransactionsAll')
    dispatch('fetchUsersAll')
    },
    fetchProductsAll({ commit }) {
        axios.get('/api/v1/products')
            .then(response => {
                commit('setProductsAll', response.data.data)
            })
    },
    fetchTransactionsAll({ commit }) {
        axios.get('/api/v1/transactions')
            .then(response => {
                commit('setTransactionsAll', response.data.data)
            })
    },
    fetchUsersAll({ commit }) {
        axios.get('/api/v1/users')
            .then(response => {
                commit('setUsersAll', response.data.data)
            })
    },
    setProduct({ commit }, value) {
        commit('setProduct', value)
    },
    setReference({ commit }, value) {
        commit('setReference', value)
    },
    setStatus({ commit }, value) {
        commit('setStatus', value)
    },
    setPayment_completed_at({ commit }, value) {
        commit('setPayment_completed_at', value)
    },
    setTransactions({ commit }, value) {
        commit('setTransactions', value)
    },
    setActive({ commit }, value) {
        commit('setActive', value)
    },
    setCreated_by({ commit }, value) {
        commit('setCreated_by', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setProduct(state, value) {
        state.item.product = value
    },
    setReference(state, value) {
        state.item.reference = value
    },
    setStatus(state, value) {
        state.item.status = value
    },
    setPayment_completed_at(state, value) {
        state.item.payment_completed_at = value
    },
    setTransactions(state, value) {
        state.item.transactions = value
    },
    setActive(state, value) {
        state.item.active = value
    },
    setCreated_by(state, value) {
        state.item.created_by = value
    },
    setProductsAll(state, value) {
        state.productsAll = value
    },
    setTransactionsAll(state, value) {
        state.transactionsAll = value
    },
    setUsersAll(state, value) {
        state.usersAll = value
    },
    setStatusEnum(state, value) {
        state.statusEnum = value
    },
    setActiveEnum(state, value) {
        state.activeEnum = value
    },
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
