function initialState() {
    return {
        item: {
            id: null,
            name: null,
            email: null,
            password: null,
            role: [],
            firstname: null,
            lastname: null,
            avatar: null,
            newsletter: null,
            about: null,
            created_by: null,
            bill_paid_at: null,
            email_verified_at: null,
            address_one: null,
            address_two: null,
            postal_code: null,
            city: null,
            status: null,
            country: null,
            language: null,
            currency: null,
        },
        rolesAll: [],
        usersAll: [],
        countriesAll: [],
        languagesAll: [],
        currenciesAll: [],
        newsletterEnum: [ { value: '1', label: '1' }, { value: '0', label: '0' }, ],
        statusEnum: [ { value: '1', label: '1' }, { value: '0', label: '0' }, ],
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    rolesAll: state => state.rolesAll,
    usersAll: state => state.usersAll,
    countriesAll: state => state.countriesAll,
    languagesAll: state => state.languagesAll,
    currenciesAll: state => state.currenciesAll,
    newsletterEnum: state => state.newsletterEnum,
    statusEnum: state => state.statusEnum,
}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.role)) {
                params.delete('role')
            } else {
                for (let index in state.item.role) {
                    params.set('role['+index+']', state.item.role[index].id)
                }
            }
            if (state.item.avatar === null) {
                params.delete('avatar');
            }
            if (! _.isEmpty(state.item.newsletter) && typeof state.item.newsletter === 'object') {
                params.set('newsletter', state.item.newsletter.value)
            }
            if (_.isEmpty(state.item.created_by)) {
                params.set('created_by_id', '')
            } else {
                params.set('created_by_id', state.item.created_by.id)
            }
            if (! _.isEmpty(state.item.status) && typeof state.item.status === 'object') {
                params.set('status', state.item.status.value)
            }
            if (_.isEmpty(state.item.country)) {
                params.set('country_id', '')
            } else {
                params.set('country_id', state.item.country.id)
            }
            if (_.isEmpty(state.item.language)) {
                params.set('language_id', '')
            } else {
                params.set('language_id', state.item.language.id)
            }
            if (_.isEmpty(state.item.currency)) {
                params.set('currency_id', '')
            } else {
                params.set('currency_id', state.item.currency.id)
            }

            axios.post('/api/v1/users', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.role)) {
                params.delete('role')
            } else {
                for (let index in state.item.role) {
                    params.set('role['+index+']', state.item.role[index].id)
                }
            }
            if (state.item.avatar === null) {
                params.delete('avatar');
            }
            if (! _.isEmpty(state.item.newsletter) && typeof state.item.newsletter === 'object') {
                params.set('newsletter', state.item.newsletter.value)
            }
            if (_.isEmpty(state.item.created_by)) {
                params.set('created_by_id', '')
            } else {
                params.set('created_by_id', state.item.created_by.id)
            }
            if (! _.isEmpty(state.item.status) && typeof state.item.status === 'object') {
                params.set('status', state.item.status.value)
            }
            if (_.isEmpty(state.item.country)) {
                params.set('country_id', '')
            } else {
                params.set('country_id', state.item.country.id)
            }
            if (_.isEmpty(state.item.language)) {
                params.set('language_id', '')
            } else {
                params.set('language_id', state.item.language.id)
            }
            if (_.isEmpty(state.item.currency)) {
                params.set('currency_id', '')
            } else {
                params.set('currency_id', state.item.currency.id)
            }

            axios.post('/api/v1/users/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/users/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        dispatch('fetchRolesAll')
    dispatch('fetchUsersAll')
    dispatch('fetchCountriesAll')
    dispatch('fetchLanguagesAll')
    dispatch('fetchCurrenciesAll')
    },
    fetchRolesAll({ commit }) {
        axios.get('/api/v1/roles')
            .then(response => {
                commit('setRolesAll', response.data.data)
            })
    },
    fetchUsersAll({ commit }) {
        axios.get('/api/v1/users')
            .then(response => {
                commit('setUsersAll', response.data.data)
            })
    },
    fetchCountriesAll({ commit }) {
        axios.get('/api/v1/countries')
            .then(response => {
                commit('setCountriesAll', response.data.data)
            })
    },
    fetchLanguagesAll({ commit }) {
        axios.get('/api/v1/languages')
            .then(response => {
                commit('setLanguagesAll', response.data.data)
            })
    },
    fetchCurrenciesAll({ commit }) {
        axios.get('/api/v1/currencies')
            .then(response => {
                commit('setCurrenciesAll', response.data.data)
            })
    },
    setName({ commit }, value) {
        commit('setName', value)
    },
    setEmail({ commit }, value) {
        commit('setEmail', value)
    },
    setPassword({ commit }, value) {
        commit('setPassword', value)
    },
    setRole({ commit }, value) {
        commit('setRole', value)
    },
    setFirstname({ commit }, value) {
        commit('setFirstname', value)
    },
    setLastname({ commit }, value) {
        commit('setLastname', value)
    },
    setAvatar({ commit }, value) {
        commit('setAvatar', value)
    },
    
    setNewsletter({ commit }, value) {
        commit('setNewsletter', value)
    },
    setAbout({ commit }, value) {
        commit('setAbout', value)
    },
    setCreated_by({ commit }, value) {
        commit('setCreated_by', value)
    },
    setBill_paid_at({ commit }, value) {
        commit('setBill_paid_at', value)
    },
    setEmail_verified_at({ commit }, value) {
        commit('setEmail_verified_at', value)
    },
    setAddress_one({ commit }, value) {
        commit('setAddress_one', value)
    },
    setAddress_two({ commit }, value) {
        commit('setAddress_two', value)
    },
    setPostal_code({ commit }, value) {
        commit('setPostal_code', value)
    },
    setCity({ commit }, value) {
        commit('setCity', value)
    },
    setStatus({ commit }, value) {
        commit('setStatus', value)
    },
    setCountry({ commit }, value) {
        commit('setCountry', value)
    },
    setLanguage({ commit }, value) {
        commit('setLanguage', value)
    },
    setCurrency({ commit }, value) {
        commit('setCurrency', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setName(state, value) {
        state.item.name = value
    },
    setEmail(state, value) {
        state.item.email = value
    },
    setPassword(state, value) {
        state.item.password = value
    },
    setRole(state, value) {
        state.item.role = value
    },
    setFirstname(state, value) {
        state.item.firstname = value
    },
    setLastname(state, value) {
        state.item.lastname = value
    },
    setAvatar(state, value) {
        state.item.avatar = value
    },
    setNewsletter(state, value) {
        state.item.newsletter = value
    },
    setAbout(state, value) {
        state.item.about = value
    },
    setCreated_by(state, value) {
        state.item.created_by = value
    },
    setBill_paid_at(state, value) {
        state.item.bill_paid_at = value
    },
    setEmail_verified_at(state, value) {
        state.item.email_verified_at = value
    },
    setAddress_one(state, value) {
        state.item.address_one = value
    },
    setAddress_two(state, value) {
        state.item.address_two = value
    },
    setPostal_code(state, value) {
        state.item.postal_code = value
    },
    setCity(state, value) {
        state.item.city = value
    },
    setStatus(state, value) {
        state.item.status = value
    },
    setCountry(state, value) {
        state.item.country = value
    },
    setLanguage(state, value) {
        state.item.language = value
    },
    setCurrency(state, value) {
        state.item.currency = value
    },
    setRolesAll(state, value) {
        state.rolesAll = value
    },
    setUsersAll(state, value) {
        state.usersAll = value
    },
    setCountriesAll(state, value) {
        state.countriesAll = value
    },
    setLanguagesAll(state, value) {
        state.languagesAll = value
    },
    setCurrenciesAll(state, value) {
        state.currenciesAll = value
    },
    setNewsletterEnum(state, value) {
        state.newsletterEnum = value
    },
    setStatusEnum(state, value) {
        state.statusEnum = value
    },
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
