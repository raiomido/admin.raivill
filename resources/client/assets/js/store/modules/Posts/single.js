function initialState() {
    return {
        item: {
            id: null,
            title: null,
            slug: null,
            product: null,
            image: null,
            active: null,
            created_by: null,
            tags: [],
            categories: [],
            video_link: null,
        },
        productsAll: [],
        usersAll: [],
        posttagsAll: [],
        postcategoriesAll: [],
        activeEnum: [ { value: '1', label: '1' }, { value: '0', label: '0' }, ],
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    productsAll: state => state.productsAll,
    usersAll: state => state.usersAll,
    posttagsAll: state => state.posttagsAll,
    postcategoriesAll: state => state.postcategoriesAll,
    activeEnum: state => state.activeEnum,
}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.product)) {
                params.set('product_id', '')
            } else {
                params.set('product_id', state.item.product.id)
            }
            if (state.item.image === null) {
                params.delete('image');
            }
            if (! _.isEmpty(state.item.active) && typeof state.item.active === 'object') {
                params.set('active', state.item.active.value)
            }
            if (_.isEmpty(state.item.created_by)) {
                params.set('created_by_id', '')
            } else {
                params.set('created_by_id', state.item.created_by.id)
            }
            if (_.isEmpty(state.item.tags)) {
                params.delete('tags')
            } else {
                for (let index in state.item.tags) {
                    params.set('tags['+index+']', state.item.tags[index].id)
                }
            }
            if (_.isEmpty(state.item.categories)) {
                params.delete('categories')
            } else {
                for (let index in state.item.categories) {
                    params.set('categories['+index+']', state.item.categories[index].id)
                }
            }

            axios.post('/api/v1/posts', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.product)) {
                params.set('product_id', '')
            } else {
                params.set('product_id', state.item.product.id)
            }
            if (state.item.image === null) {
                params.delete('image');
            }
            if (! _.isEmpty(state.item.active) && typeof state.item.active === 'object') {
                params.set('active', state.item.active.value)
            }
            if (_.isEmpty(state.item.created_by)) {
                params.set('created_by_id', '')
            } else {
                params.set('created_by_id', state.item.created_by.id)
            }
            if (_.isEmpty(state.item.tags)) {
                params.delete('tags')
            } else {
                for (let index in state.item.tags) {
                    params.set('tags['+index+']', state.item.tags[index].id)
                }
            }
            if (_.isEmpty(state.item.categories)) {
                params.delete('categories')
            } else {
                for (let index in state.item.categories) {
                    params.set('categories['+index+']', state.item.categories[index].id)
                }
            }

            axios.post('/api/v1/posts/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/posts/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        dispatch('fetchProductsAll')
    dispatch('fetchUsersAll')
    dispatch('fetchPosttagsAll')
    dispatch('fetchPostcategoriesAll')
    },
    fetchProductsAll({ commit }) {
        axios.get('/api/v1/products')
            .then(response => {
                commit('setProductsAll', response.data.data)
            })
    },
    fetchUsersAll({ commit }) {
        axios.get('/api/v1/users')
            .then(response => {
                commit('setUsersAll', response.data.data)
            })
    },
    fetchPosttagsAll({ commit }) {
        axios.get('/api/v1/post-tags')
            .then(response => {
                commit('setPosttagsAll', response.data.data)
            })
    },
    fetchPostcategoriesAll({ commit }) {
        axios.get('/api/v1/post-categories')
            .then(response => {
                commit('setPostcategoriesAll', response.data.data)
            })
    },
    setTitle({ commit }, value) {
        commit('setTitle', value)
    },
    setSlug({ commit }, value) {
        commit('setSlug', value)
    },
    setProduct({ commit }, value) {
        commit('setProduct', value)
    },
    setImage({ commit }, value) {
        commit('setImage', value)
    },
    
    setActive({ commit }, value) {
        commit('setActive', value)
    },
    setCreated_by({ commit }, value) {
        commit('setCreated_by', value)
    },
    setTags({ commit }, value) {
        commit('setTags', value)
    },
    setCategories({ commit }, value) {
        commit('setCategories', value)
    },
    setVideo_link({ commit }, value) {
        commit('setVideo_link', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setTitle(state, value) {
        state.item.title = value
    },
    setSlug(state, value) {
        state.item.slug = value
    },
    setProduct(state, value) {
        state.item.product = value
    },
    setImage(state, value) {
        state.item.image = value
    },
    setActive(state, value) {
        state.item.active = value
    },
    setCreated_by(state, value) {
        state.item.created_by = value
    },
    setTags(state, value) {
        state.item.tags = value
    },
    setCategories(state, value) {
        state.item.categories = value
    },
    setVideo_link(state, value) {
        state.item.video_link = value
    },
    setProductsAll(state, value) {
        state.productsAll = value
    },
    setUsersAll(state, value) {
        state.usersAll = value
    },
    setPosttagsAll(state, value) {
        state.posttagsAll = value
    },
    setPostcategoriesAll(state, value) {
        state.postcategoriesAll = value
    },
    setActiveEnum(state, value) {
        state.activeEnum = value
    },
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
