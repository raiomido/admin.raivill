function initialState() {
    return {
        item: {
            id: null,
            user: null,
            transaction: null,
            create_time: null,
            update_time: null,
            paypal_id: null,
            intent: null,
            transaction_status: null,
            email: null,
            payer_id: null,
            country_code: null,
            firstname: null,
            lastname: null,
            phone: null,
            amount: null,
            currency_code: null,
            payee_email_address: null,
            payee_merchant_id: null,
            shipping: null,
            payment_status: null,
            payment_id: null,
            seller_protection: null,
            links: null,
        },
        usersAll: [],
        transactionsAll: [],
        
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    usersAll: state => state.usersAll,
    transactionsAll: state => state.transactionsAll,
    
}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.user)) {
                params.set('user_id', '')
            } else {
                params.set('user_id', state.item.user.id)
            }
            if (_.isEmpty(state.item.transaction)) {
                params.set('transaction_id', '')
            } else {
                params.set('transaction_id', state.item.transaction.id)
            }

            axios.post('/api/v1/paypals', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.user)) {
                params.set('user_id', '')
            } else {
                params.set('user_id', state.item.user.id)
            }
            if (_.isEmpty(state.item.transaction)) {
                params.set('transaction_id', '')
            } else {
                params.set('transaction_id', state.item.transaction.id)
            }

            axios.post('/api/v1/paypals/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/paypals/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        dispatch('fetchUsersAll')
    dispatch('fetchTransactionsAll')
    },
    fetchUsersAll({ commit }) {
        axios.get('/api/v1/users')
            .then(response => {
                commit('setUsersAll', response.data.data)
            })
    },
    fetchTransactionsAll({ commit }) {
        axios.get('/api/v1/transactions')
            .then(response => {
                commit('setTransactionsAll', response.data.data)
            })
    },
    setUser({ commit }, value) {
        commit('setUser', value)
    },
    setTransaction({ commit }, value) {
        commit('setTransaction', value)
    },
    setCreate_time({ commit }, value) {
        commit('setCreate_time', value)
    },
    setUpdate_time({ commit }, value) {
        commit('setUpdate_time', value)
    },
    setPaypal_id({ commit }, value) {
        commit('setPaypal_id', value)
    },
    setIntent({ commit }, value) {
        commit('setIntent', value)
    },
    setTransaction_status({ commit }, value) {
        commit('setTransaction_status', value)
    },
    setEmail({ commit }, value) {
        commit('setEmail', value)
    },
    setPayer_id({ commit }, value) {
        commit('setPayer_id', value)
    },
    setCountry_code({ commit }, value) {
        commit('setCountry_code', value)
    },
    setFirstname({ commit }, value) {
        commit('setFirstname', value)
    },
    setLastname({ commit }, value) {
        commit('setLastname', value)
    },
    setPhone({ commit }, value) {
        commit('setPhone', value)
    },
    setAmount({ commit }, value) {
        commit('setAmount', value)
    },
    setCurrency_code({ commit }, value) {
        commit('setCurrency_code', value)
    },
    setPayee_email_address({ commit }, value) {
        commit('setPayee_email_address', value)
    },
    setPayee_merchant_id({ commit }, value) {
        commit('setPayee_merchant_id', value)
    },
    setShipping({ commit }, value) {
        commit('setShipping', value)
    },
    setPayment_status({ commit }, value) {
        commit('setPayment_status', value)
    },
    setPayment_id({ commit }, value) {
        commit('setPayment_id', value)
    },
    setSeller_protection({ commit }, value) {
        commit('setSeller_protection', value)
    },
    setLinks({ commit }, value) {
        commit('setLinks', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setUser(state, value) {
        state.item.user = value
    },
    setTransaction(state, value) {
        state.item.transaction = value
    },
    setCreate_time(state, value) {
        state.item.create_time = value
    },
    setUpdate_time(state, value) {
        state.item.update_time = value
    },
    setPaypal_id(state, value) {
        state.item.paypal_id = value
    },
    setIntent(state, value) {
        state.item.intent = value
    },
    setTransaction_status(state, value) {
        state.item.transaction_status = value
    },
    setEmail(state, value) {
        state.item.email = value
    },
    setPayer_id(state, value) {
        state.item.payer_id = value
    },
    setCountry_code(state, value) {
        state.item.country_code = value
    },
    setFirstname(state, value) {
        state.item.firstname = value
    },
    setLastname(state, value) {
        state.item.lastname = value
    },
    setPhone(state, value) {
        state.item.phone = value
    },
    setAmount(state, value) {
        state.item.amount = value
    },
    setCurrency_code(state, value) {
        state.item.currency_code = value
    },
    setPayee_email_address(state, value) {
        state.item.payee_email_address = value
    },
    setPayee_merchant_id(state, value) {
        state.item.payee_merchant_id = value
    },
    setShipping(state, value) {
        state.item.shipping = value
    },
    setPayment_status(state, value) {
        state.item.payment_status = value
    },
    setPayment_id(state, value) {
        state.item.payment_id = value
    },
    setSeller_protection(state, value) {
        state.item.seller_protection = value
    },
    setLinks(state, value) {
        state.item.links = value
    },
    setUsersAll(state, value) {
        state.usersAll = value
    },
    setTransactionsAll(state, value) {
        state.transactionsAll = value
    },
    
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
