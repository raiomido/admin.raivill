import Vue from 'vue'
import Vuex from 'vuex'
import Alert from './modules/alert'
import ChangePassword from './modules/change_password'
import Rules from './modules/rules'
import PermissionsIndex from './modules/Permissions'
import PermissionsSingle from './modules/Permissions/single'
import RolesIndex from './modules/Roles'
import RolesSingle from './modules/Roles/single'
import UsersIndex from './modules/Users'
import UsersSingle from './modules/Users/single'
import UserActionsIndex from './modules/UserActions'
import CrmStatusesIndex from './modules/CrmStatuses'
import CrmStatusesSingle from './modules/CrmStatuses/single'
import CrmCustomersIndex from './modules/CrmCustomers'
import CrmCustomersSingle from './modules/CrmCustomers/single'
import CrmNotesIndex from './modules/CrmNotes'
import CrmNotesSingle from './modules/CrmNotes/single'
import CrmDocumentsIndex from './modules/CrmDocuments'
import CrmDocumentsSingle from './modules/CrmDocuments/single'
import ContentCategoriesIndex from './modules/ContentCategories'
import ContentCategoriesSingle from './modules/ContentCategories/single'
import ContentTagsIndex from './modules/ContentTags'
import ContentTagsSingle from './modules/ContentTags/single'
import ContentPagesIndex from './modules/ContentPages'
import ContentPagesSingle from './modules/ContentPages/single'
import PackagesIndex from './modules/Packages'
import PackagesSingle from './modules/Packages/single'
import PackageItemsIndex from './modules/PackageItems'
import PackageItemsSingle from './modules/PackageItems/single'
import ProductCategoriesIndex from './modules/ProductCategories'
import ProductCategoriesSingle from './modules/ProductCategories/single'
import ProductSuppliersIndex from './modules/ProductSuppliers'
import ProductSuppliersSingle from './modules/ProductSuppliers/single'
import ProductsIndex from './modules/Products'
import ProductsSingle from './modules/Products/single'
import QuoteRequestsIndex from './modules/QuoteRequests'
import QuoteRequestsSingle from './modules/QuoteRequests/single'
import TransactionsIndex from './modules/Transactions'
import TransactionsSingle from './modules/Transactions/single'
import CurrenciesIndex from './modules/Currencies'
import CurrenciesSingle from './modules/Currencies/single'
import CountriesIndex from './modules/Countries'
import CountriesSingle from './modules/Countries/single'
import LanguagesIndex from './modules/Languages'
import LanguagesSingle from './modules/Languages/single'
import MpesasIndex from './modules/Mpesas'
import MpesasSingle from './modules/Mpesas/single'
import PkAirtimesIndex from './modules/PkAirtimes'
import PkAirtimesSingle from './modules/PkAirtimes/single'
import PkTokensIndex from './modules/PkTokens'
import PkTokensSingle from './modules/PkTokens/single'
import AccountsIndex from './modules/Accounts'
import AccountsSingle from './modules/Accounts/single'
import RaivillAccountsIndex from './modules/RaivillAccounts'
import RaivillAccountsSingle from './modules/RaivillAccounts/single'
import AccountEntriesIndex from './modules/AccountEntries'
import AccountEntriesSingle from './modules/AccountEntries/single'
import RaivillAccountEntriesIndex from './modules/RaivillAccountEntries'
import RaivillAccountEntriesSingle from './modules/RaivillAccountEntries/single'
import ContactCompaniesIndex from './modules/ContactCompanies'
import ContactCompaniesSingle from './modules/ContactCompanies/single'
import ContactsIndex from './modules/Contacts'
import ContactsSingle from './modules/Contacts/single'
import FaqCategoriesIndex from './modules/FaqCategories'
import FaqCategoriesSingle from './modules/FaqCategories/single'
import FaqQuestionsIndex from './modules/FaqQuestions'
import FaqQuestionsSingle from './modules/FaqQuestions/single'
import PostCategoriesIndex from './modules/PostCategories'
import PostCategoriesSingle from './modules/PostCategories/single'
import ProductCommentsIndex from './modules/ProductComments'
import ProductCommentsSingle from './modules/ProductComments/single'
import PostsIndex from './modules/Posts'
import PostsSingle from './modules/Posts/single'
import PostTagsIndex from './modules/PostTags'
import PostTagsSingle from './modules/PostTags/single'
import PostFragmentsIndex from './modules/PostFragments'
import PostFragmentsSingle from './modules/PostFragments/single'
import ShoppingCartsIndex from './modules/ShoppingCarts'
import ShoppingCartsSingle from './modules/ShoppingCarts/single'
import OrdersIndex from './modules/Orders'
import OrdersSingle from './modules/Orders/single'
import PostCommentsIndex from './modules/PostComments'
import PostCommentsSingle from './modules/PostComments/single'
import FragmentCommentsIndex from './modules/FragmentComments'
import FragmentCommentsSingle from './modules/FragmentComments/single'
import PaypalsIndex from './modules/Paypals'
import PaypalsSingle from './modules/Paypals/single'
import ChatsIndex from './modules/Chats'
import ChatsSingle from './modules/Chats/single'
import MessagesIndex from './modules/Messages'
import MessagesSingle from './modules/Messages/single'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    modules: {
        Alert,
        ChangePassword,
        Rules,
        PermissionsIndex,
        PermissionsSingle,
        RolesIndex,
        RolesSingle,
        UsersIndex,
        UsersSingle,
        UserActionsIndex,
        CrmStatusesIndex,
        CrmStatusesSingle,
        CrmCustomersIndex,
        CrmCustomersSingle,
        CrmNotesIndex,
        CrmNotesSingle,
        CrmDocumentsIndex,
        CrmDocumentsSingle,
        ContentCategoriesIndex,
        ContentCategoriesSingle,
        ContentTagsIndex,
        ContentTagsSingle,
        ContentPagesIndex,
        ContentPagesSingle,
        PackagesIndex,
        PackagesSingle,
        PackageItemsIndex,
        PackageItemsSingle,
        ProductCategoriesIndex,
        ProductCategoriesSingle,
        ProductSuppliersIndex,
        ProductSuppliersSingle,
        ProductsIndex,
        ProductsSingle,
        QuoteRequestsIndex,
        QuoteRequestsSingle,
        TransactionsIndex,
        TransactionsSingle,
        CurrenciesIndex,
        CurrenciesSingle,
        CountriesIndex,
        CountriesSingle,
        LanguagesIndex,
        LanguagesSingle,
        MpesasIndex,
        MpesasSingle,
        PkAirtimesIndex,
        PkAirtimesSingle,
        PkTokensIndex,
        PkTokensSingle,
        AccountsIndex,
        AccountsSingle,
        RaivillAccountsIndex,
        RaivillAccountsSingle,
        AccountEntriesIndex,
        AccountEntriesSingle,
        RaivillAccountEntriesIndex,
        RaivillAccountEntriesSingle,
        ContactCompaniesIndex,
        ContactCompaniesSingle,
        ContactsIndex,
        ContactsSingle,
        FaqCategoriesIndex,
        FaqCategoriesSingle,
        FaqQuestionsIndex,
        FaqQuestionsSingle,
        PostCategoriesIndex,
        PostCategoriesSingle,
        ProductCommentsIndex,
        ProductCommentsSingle,
        PostsIndex,
        PostsSingle,
        PostTagsIndex,
        PostTagsSingle,
        PostFragmentsIndex,
        PostFragmentsSingle,
        ShoppingCartsIndex,
        ShoppingCartsSingle,
        OrdersIndex,
        OrdersSingle,
        PostCommentsIndex,
        PostCommentsSingle,
        FragmentCommentsIndex,
        FragmentCommentsSingle,
        PaypalsIndex,
        PaypalsSingle,
        ChatsIndex,
        ChatsSingle,
        MessagesIndex,
        MessagesSingle,
    },
    strict: debug,
})
