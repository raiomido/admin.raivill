import Vue from 'vue'
import VueRouter from 'vue-router'

import ChangePassword from '../components/ChangePassword.vue'
import PermissionsIndex from '../components/cruds/Permissions/Index.vue'
import PermissionsCreate from '../components/cruds/Permissions/Create.vue'
import PermissionsShow from '../components/cruds/Permissions/Show.vue'
import PermissionsEdit from '../components/cruds/Permissions/Edit.vue'
import RolesIndex from '../components/cruds/Roles/Index.vue'
import RolesCreate from '../components/cruds/Roles/Create.vue'
import RolesShow from '../components/cruds/Roles/Show.vue'
import RolesEdit from '../components/cruds/Roles/Edit.vue'
import UsersIndex from '../components/cruds/Users/Index.vue'
import UsersCreate from '../components/cruds/Users/Create.vue'
import UsersShow from '../components/cruds/Users/Show.vue'
import UsersEdit from '../components/cruds/Users/Edit.vue'
import UserActionsIndex from '../components/cruds/UserActions/Index.vue'
import CrmStatusesIndex from '../components/cruds/CrmStatuses/Index.vue'
import CrmStatusesCreate from '../components/cruds/CrmStatuses/Create.vue'
import CrmStatusesShow from '../components/cruds/CrmStatuses/Show.vue'
import CrmStatusesEdit from '../components/cruds/CrmStatuses/Edit.vue'
import CrmCustomersIndex from '../components/cruds/CrmCustomers/Index.vue'
import CrmCustomersCreate from '../components/cruds/CrmCustomers/Create.vue'
import CrmCustomersShow from '../components/cruds/CrmCustomers/Show.vue'
import CrmCustomersEdit from '../components/cruds/CrmCustomers/Edit.vue'
import CrmNotesIndex from '../components/cruds/CrmNotes/Index.vue'
import CrmNotesCreate from '../components/cruds/CrmNotes/Create.vue'
import CrmNotesShow from '../components/cruds/CrmNotes/Show.vue'
import CrmNotesEdit from '../components/cruds/CrmNotes/Edit.vue'
import CrmDocumentsIndex from '../components/cruds/CrmDocuments/Index.vue'
import CrmDocumentsCreate from '../components/cruds/CrmDocuments/Create.vue'
import CrmDocumentsShow from '../components/cruds/CrmDocuments/Show.vue'
import CrmDocumentsEdit from '../components/cruds/CrmDocuments/Edit.vue'
import ContentCategoriesIndex from '../components/cruds/ContentCategories/Index.vue'
import ContentCategoriesCreate from '../components/cruds/ContentCategories/Create.vue'
import ContentCategoriesShow from '../components/cruds/ContentCategories/Show.vue'
import ContentCategoriesEdit from '../components/cruds/ContentCategories/Edit.vue'
import ContentTagsIndex from '../components/cruds/ContentTags/Index.vue'
import ContentTagsCreate from '../components/cruds/ContentTags/Create.vue'
import ContentTagsShow from '../components/cruds/ContentTags/Show.vue'
import ContentTagsEdit from '../components/cruds/ContentTags/Edit.vue'
import ContentPagesIndex from '../components/cruds/ContentPages/Index.vue'
import ContentPagesCreate from '../components/cruds/ContentPages/Create.vue'
import ContentPagesShow from '../components/cruds/ContentPages/Show.vue'
import ContentPagesEdit from '../components/cruds/ContentPages/Edit.vue'
import PackagesIndex from '../components/cruds/Packages/Index.vue'
import PackagesCreate from '../components/cruds/Packages/Create.vue'
import PackagesShow from '../components/cruds/Packages/Show.vue'
import PackagesEdit from '../components/cruds/Packages/Edit.vue'
import PackageItemsIndex from '../components/cruds/PackageItems/Index.vue'
import PackageItemsCreate from '../components/cruds/PackageItems/Create.vue'
import PackageItemsShow from '../components/cruds/PackageItems/Show.vue'
import PackageItemsEdit from '../components/cruds/PackageItems/Edit.vue'
import ProductCategoriesIndex from '../components/cruds/ProductCategories/Index.vue'
import ProductCategoriesCreate from '../components/cruds/ProductCategories/Create.vue'
import ProductCategoriesShow from '../components/cruds/ProductCategories/Show.vue'
import ProductCategoriesEdit from '../components/cruds/ProductCategories/Edit.vue'
import ProductSuppliersIndex from '../components/cruds/ProductSuppliers/Index.vue'
import ProductSuppliersCreate from '../components/cruds/ProductSuppliers/Create.vue'
import ProductSuppliersShow from '../components/cruds/ProductSuppliers/Show.vue'
import ProductSuppliersEdit from '../components/cruds/ProductSuppliers/Edit.vue'
import ProductsIndex from '../components/cruds/Products/Index.vue'
import ProductsCreate from '../components/cruds/Products/Create.vue'
import ProductsShow from '../components/cruds/Products/Show.vue'
import ProductsEdit from '../components/cruds/Products/Edit.vue'
import QuoteRequestsIndex from '../components/cruds/QuoteRequests/Index.vue'
import QuoteRequestsShow from '../components/cruds/QuoteRequests/Show.vue'
import TransactionsIndex from '../components/cruds/Transactions/Index.vue'
import TransactionsShow from '../components/cruds/Transactions/Show.vue'
import CurrenciesIndex from '../components/cruds/Currencies/Index.vue'
import CurrenciesCreate from '../components/cruds/Currencies/Create.vue'
import CurrenciesShow from '../components/cruds/Currencies/Show.vue'
import CurrenciesEdit from '../components/cruds/Currencies/Edit.vue'
import CountriesIndex from '../components/cruds/Countries/Index.vue'
import CountriesCreate from '../components/cruds/Countries/Create.vue'
import CountriesShow from '../components/cruds/Countries/Show.vue'
import CountriesEdit from '../components/cruds/Countries/Edit.vue'
import LanguagesIndex from '../components/cruds/Languages/Index.vue'
import LanguagesCreate from '../components/cruds/Languages/Create.vue'
import LanguagesShow from '../components/cruds/Languages/Show.vue'
import LanguagesEdit from '../components/cruds/Languages/Edit.vue'
import MpesasIndex from '../components/cruds/Mpesas/Index.vue'
import MpesasShow from '../components/cruds/Mpesas/Show.vue'
import PkAirtimesIndex from '../components/cruds/PkAirtimes/Index.vue'
import PkAirtimesShow from '../components/cruds/PkAirtimes/Show.vue'
import PkTokensIndex from '../components/cruds/PkTokens/Index.vue'
import PkTokensShow from '../components/cruds/PkTokens/Show.vue'
import AccountsIndex from '../components/cruds/Accounts/Index.vue'
import AccountsCreate from '../components/cruds/Accounts/Create.vue'
import AccountsShow from '../components/cruds/Accounts/Show.vue'
import RaivillAccountsIndex from '../components/cruds/RaivillAccounts/Index.vue'
import RaivillAccountsCreate from '../components/cruds/RaivillAccounts/Create.vue'
import RaivillAccountsShow from '../components/cruds/RaivillAccounts/Show.vue'
import AccountEntriesIndex from '../components/cruds/AccountEntries/Index.vue'
import AccountEntriesShow from '../components/cruds/AccountEntries/Show.vue'
import RaivillAccountEntriesIndex from '../components/cruds/RaivillAccountEntries/Index.vue'
import RaivillAccountEntriesShow from '../components/cruds/RaivillAccountEntries/Show.vue'
import ContactCompaniesIndex from '../components/cruds/ContactCompanies/Index.vue'
import ContactCompaniesCreate from '../components/cruds/ContactCompanies/Create.vue'
import ContactCompaniesShow from '../components/cruds/ContactCompanies/Show.vue'
import ContactCompaniesEdit from '../components/cruds/ContactCompanies/Edit.vue'
import ContactsIndex from '../components/cruds/Contacts/Index.vue'
import ContactsCreate from '../components/cruds/Contacts/Create.vue'
import ContactsShow from '../components/cruds/Contacts/Show.vue'
import ContactsEdit from '../components/cruds/Contacts/Edit.vue'
import FaqCategoriesIndex from '../components/cruds/FaqCategories/Index.vue'
import FaqCategoriesCreate from '../components/cruds/FaqCategories/Create.vue'
import FaqCategoriesShow from '../components/cruds/FaqCategories/Show.vue'
import FaqCategoriesEdit from '../components/cruds/FaqCategories/Edit.vue'
import FaqQuestionsIndex from '../components/cruds/FaqQuestions/Index.vue'
import FaqQuestionsCreate from '../components/cruds/FaqQuestions/Create.vue'
import FaqQuestionsShow from '../components/cruds/FaqQuestions/Show.vue'
import FaqQuestionsEdit from '../components/cruds/FaqQuestions/Edit.vue'
import PostCategoriesIndex from '../components/cruds/PostCategories/Index.vue'
import PostCategoriesCreate from '../components/cruds/PostCategories/Create.vue'
import PostCategoriesShow from '../components/cruds/PostCategories/Show.vue'
import PostCategoriesEdit from '../components/cruds/PostCategories/Edit.vue'
import ProductCommentsIndex from '../components/cruds/ProductComments/Index.vue'
import ProductCommentsCreate from '../components/cruds/ProductComments/Create.vue'
import ProductCommentsShow from '../components/cruds/ProductComments/Show.vue'
import ProductCommentsEdit from '../components/cruds/ProductComments/Edit.vue'
import PostsIndex from '../components/cruds/Posts/Index.vue'
import PostsCreate from '../components/cruds/Posts/Create.vue'
import PostsShow from '../components/cruds/Posts/Show.vue'
import PostsEdit from '../components/cruds/Posts/Edit.vue'
import PostTagsIndex from '../components/cruds/PostTags/Index.vue'
import PostTagsCreate from '../components/cruds/PostTags/Create.vue'
import PostTagsShow from '../components/cruds/PostTags/Show.vue'
import PostTagsEdit from '../components/cruds/PostTags/Edit.vue'
import PostFragmentsIndex from '../components/cruds/PostFragments/Index.vue'
import PostFragmentsCreate from '../components/cruds/PostFragments/Create.vue'
import PostFragmentsShow from '../components/cruds/PostFragments/Show.vue'
import PostFragmentsEdit from '../components/cruds/PostFragments/Edit.vue'
import ShoppingCartsIndex from '../components/cruds/ShoppingCarts/Index.vue'
import ShoppingCartsShow from '../components/cruds/ShoppingCarts/Show.vue'
import OrdersIndex from '../components/cruds/Orders/Index.vue'
import OrdersCreate from '../components/cruds/Orders/Create.vue'
import OrdersShow from '../components/cruds/Orders/Show.vue'
import OrdersEdit from '../components/cruds/Orders/Edit.vue'
import PostCommentsIndex from '../components/cruds/PostComments/Index.vue'
import PostCommentsShow from '../components/cruds/PostComments/Show.vue'
import FragmentCommentsIndex from '../components/cruds/FragmentComments/Index.vue'
import FragmentCommentsCreate from '../components/cruds/FragmentComments/Create.vue'
import FragmentCommentsShow from '../components/cruds/FragmentComments/Show.vue'
import FragmentCommentsEdit from '../components/cruds/FragmentComments/Edit.vue'
import PaypalsIndex from '../components/cruds/Paypals/Index.vue'
import PaypalsShow from '../components/cruds/Paypals/Show.vue'
import ChatsIndex from '../components/cruds/Chats/Index.vue'
import ChatsCreate from '../components/cruds/Chats/Create.vue'
import ChatsShow from '../components/cruds/Chats/Show.vue'
import ChatsEdit from '../components/cruds/Chats/Edit.vue'
import MessagesIndex from '../components/cruds/Messages/Index.vue'
import MessagesCreate from '../components/cruds/Messages/Create.vue'
import MessagesShow from '../components/cruds/Messages/Show.vue'
import MessagesEdit from '../components/cruds/Messages/Edit.vue'

Vue.use(VueRouter)

const routes = [
    { path: '/change-password', component: ChangePassword, name: 'auth.change_password' },
    { path: '/permissions', component: PermissionsIndex, name: 'permissions.index' },
    { path: '/permissions/create', component: PermissionsCreate, name: 'permissions.create' },
    { path: '/permissions/:id', component: PermissionsShow, name: 'permissions.show' },
    { path: '/permissions/:id/edit', component: PermissionsEdit, name: 'permissions.edit' },
    { path: '/roles', component: RolesIndex, name: 'roles.index' },
    { path: '/roles/create', component: RolesCreate, name: 'roles.create' },
    { path: '/roles/:id', component: RolesShow, name: 'roles.show' },
    { path: '/roles/:id/edit', component: RolesEdit, name: 'roles.edit' },
    { path: '/users', component: UsersIndex, name: 'users.index' },
    { path: '/users/create', component: UsersCreate, name: 'users.create' },
    { path: '/users/:id', component: UsersShow, name: 'users.show' },
    { path: '/users/:id/edit', component: UsersEdit, name: 'users.edit' },
    { path: '/user-actions', component: UserActionsIndex, name: 'user_actions.index' },
    { path: '/crm-statuses', component: CrmStatusesIndex, name: 'crm_statuses.index' },
    { path: '/crm-statuses/create', component: CrmStatusesCreate, name: 'crm_statuses.create' },
    { path: '/crm-statuses/:id', component: CrmStatusesShow, name: 'crm_statuses.show' },
    { path: '/crm-statuses/:id/edit', component: CrmStatusesEdit, name: 'crm_statuses.edit' },
    { path: '/crm-customers', component: CrmCustomersIndex, name: 'crm_customers.index' },
    { path: '/crm-customers/create', component: CrmCustomersCreate, name: 'crm_customers.create' },
    { path: '/crm-customers/:id', component: CrmCustomersShow, name: 'crm_customers.show' },
    { path: '/crm-customers/:id/edit', component: CrmCustomersEdit, name: 'crm_customers.edit' },
    { path: '/crm-notes', component: CrmNotesIndex, name: 'crm_notes.index' },
    { path: '/crm-notes/create', component: CrmNotesCreate, name: 'crm_notes.create' },
    { path: '/crm-notes/:id', component: CrmNotesShow, name: 'crm_notes.show' },
    { path: '/crm-notes/:id/edit', component: CrmNotesEdit, name: 'crm_notes.edit' },
    { path: '/crm-documents', component: CrmDocumentsIndex, name: 'crm_documents.index' },
    { path: '/crm-documents/create', component: CrmDocumentsCreate, name: 'crm_documents.create' },
    { path: '/crm-documents/:id', component: CrmDocumentsShow, name: 'crm_documents.show' },
    { path: '/crm-documents/:id/edit', component: CrmDocumentsEdit, name: 'crm_documents.edit' },
    { path: '/content-categories', component: ContentCategoriesIndex, name: 'content_categories.index' },
    { path: '/content-categories/create', component: ContentCategoriesCreate, name: 'content_categories.create' },
    { path: '/content-categories/:id', component: ContentCategoriesShow, name: 'content_categories.show' },
    { path: '/content-categories/:id/edit', component: ContentCategoriesEdit, name: 'content_categories.edit' },
    { path: '/content-tags', component: ContentTagsIndex, name: 'content_tags.index' },
    { path: '/content-tags/create', component: ContentTagsCreate, name: 'content_tags.create' },
    { path: '/content-tags/:id', component: ContentTagsShow, name: 'content_tags.show' },
    { path: '/content-tags/:id/edit', component: ContentTagsEdit, name: 'content_tags.edit' },
    { path: '/content-pages', component: ContentPagesIndex, name: 'content_pages.index' },
    { path: '/content-pages/create', component: ContentPagesCreate, name: 'content_pages.create' },
    { path: '/content-pages/:id', component: ContentPagesShow, name: 'content_pages.show' },
    { path: '/content-pages/:id/edit', component: ContentPagesEdit, name: 'content_pages.edit' },
    { path: '/packages', component: PackagesIndex, name: 'packages.index' },
    { path: '/packages/create', component: PackagesCreate, name: 'packages.create' },
    { path: '/packages/:id', component: PackagesShow, name: 'packages.show' },
    { path: '/packages/:id/edit', component: PackagesEdit, name: 'packages.edit' },
    { path: '/package-items', component: PackageItemsIndex, name: 'package_items.index' },
    { path: '/package-items/create', component: PackageItemsCreate, name: 'package_items.create' },
    { path: '/package-items/:id', component: PackageItemsShow, name: 'package_items.show' },
    { path: '/package-items/:id/edit', component: PackageItemsEdit, name: 'package_items.edit' },
    { path: '/product-categories', component: ProductCategoriesIndex, name: 'product_categories.index' },
    { path: '/product-categories/create', component: ProductCategoriesCreate, name: 'product_categories.create' },
    { path: '/product-categories/:id', component: ProductCategoriesShow, name: 'product_categories.show' },
    { path: '/product-categories/:id/edit', component: ProductCategoriesEdit, name: 'product_categories.edit' },
    { path: '/product-suppliers', component: ProductSuppliersIndex, name: 'product_suppliers.index' },
    { path: '/product-suppliers/create', component: ProductSuppliersCreate, name: 'product_suppliers.create' },
    { path: '/product-suppliers/:id', component: ProductSuppliersShow, name: 'product_suppliers.show' },
    { path: '/product-suppliers/:id/edit', component: ProductSuppliersEdit, name: 'product_suppliers.edit' },
    { path: '/products', component: ProductsIndex, name: 'products.index' },
    { path: '/products/create', component: ProductsCreate, name: 'products.create' },
    { path: '/products/:id', component: ProductsShow, name: 'products.show' },
    { path: '/products/:id/edit', component: ProductsEdit, name: 'products.edit' },
    { path: '/quote-requests', component: QuoteRequestsIndex, name: 'quote_requests.index' },
    { path: '/quote-requests/:id', component: QuoteRequestsShow, name: 'quote_requests.show' },
    { path: '/transactions', component: TransactionsIndex, name: 'transactions.index' },
    { path: '/transactions/:id', component: TransactionsShow, name: 'transactions.show' },
    { path: '/currencies', component: CurrenciesIndex, name: 'currencies.index' },
    { path: '/currencies/create', component: CurrenciesCreate, name: 'currencies.create' },
    { path: '/currencies/:id', component: CurrenciesShow, name: 'currencies.show' },
    { path: '/currencies/:id/edit', component: CurrenciesEdit, name: 'currencies.edit' },
    { path: '/countries', component: CountriesIndex, name: 'countries.index' },
    { path: '/countries/create', component: CountriesCreate, name: 'countries.create' },
    { path: '/countries/:id', component: CountriesShow, name: 'countries.show' },
    { path: '/countries/:id/edit', component: CountriesEdit, name: 'countries.edit' },
    { path: '/languages', component: LanguagesIndex, name: 'languages.index' },
    { path: '/languages/create', component: LanguagesCreate, name: 'languages.create' },
    { path: '/languages/:id', component: LanguagesShow, name: 'languages.show' },
    { path: '/languages/:id/edit', component: LanguagesEdit, name: 'languages.edit' },
    { path: '/mpesas', component: MpesasIndex, name: 'mpesas.index' },
    { path: '/mpesas/:id', component: MpesasShow, name: 'mpesas.show' },
    { path: '/pk-airtimes', component: PkAirtimesIndex, name: 'pk_airtimes.index' },
    { path: '/pk-airtimes/:id', component: PkAirtimesShow, name: 'pk_airtimes.show' },
    { path: '/pk-tokens', component: PkTokensIndex, name: 'pk_tokens.index' },
    { path: '/pk-tokens/:id', component: PkTokensShow, name: 'pk_tokens.show' },
    { path: '/accounts', component: AccountsIndex, name: 'accounts.index' },
    { path: '/accounts/create', component: AccountsCreate, name: 'accounts.create' },
    { path: '/accounts/:id', component: AccountsShow, name: 'accounts.show' },
    { path: '/raivill-accounts', component: RaivillAccountsIndex, name: 'raivill_accounts.index' },
    { path: '/raivill-accounts/create', component: RaivillAccountsCreate, name: 'raivill_accounts.create' },
    { path: '/raivill-accounts/:id', component: RaivillAccountsShow, name: 'raivill_accounts.show' },
    { path: '/account-entries', component: AccountEntriesIndex, name: 'account_entries.index' },
    { path: '/account-entries/:id', component: AccountEntriesShow, name: 'account_entries.show' },
    { path: '/raivill-account-entries', component: RaivillAccountEntriesIndex, name: 'raivill_account_entries.index' },
    { path: '/raivill-account-entries/:id', component: RaivillAccountEntriesShow, name: 'raivill_account_entries.show' },
    { path: '/contact-companies', component: ContactCompaniesIndex, name: 'contact_companies.index' },
    { path: '/contact-companies/create', component: ContactCompaniesCreate, name: 'contact_companies.create' },
    { path: '/contact-companies/:id', component: ContactCompaniesShow, name: 'contact_companies.show' },
    { path: '/contact-companies/:id/edit', component: ContactCompaniesEdit, name: 'contact_companies.edit' },
    { path: '/contacts', component: ContactsIndex, name: 'contacts.index' },
    { path: '/contacts/create', component: ContactsCreate, name: 'contacts.create' },
    { path: '/contacts/:id', component: ContactsShow, name: 'contacts.show' },
    { path: '/contacts/:id/edit', component: ContactsEdit, name: 'contacts.edit' },
    { path: '/faq-categories', component: FaqCategoriesIndex, name: 'faq_categories.index' },
    { path: '/faq-categories/create', component: FaqCategoriesCreate, name: 'faq_categories.create' },
    { path: '/faq-categories/:id', component: FaqCategoriesShow, name: 'faq_categories.show' },
    { path: '/faq-categories/:id/edit', component: FaqCategoriesEdit, name: 'faq_categories.edit' },
    { path: '/faq-questions', component: FaqQuestionsIndex, name: 'faq_questions.index' },
    { path: '/faq-questions/create', component: FaqQuestionsCreate, name: 'faq_questions.create' },
    { path: '/faq-questions/:id', component: FaqQuestionsShow, name: 'faq_questions.show' },
    { path: '/faq-questions/:id/edit', component: FaqQuestionsEdit, name: 'faq_questions.edit' },
    { path: '/post-categories', component: PostCategoriesIndex, name: 'post_categories.index' },
    { path: '/post-categories/create', component: PostCategoriesCreate, name: 'post_categories.create' },
    { path: '/post-categories/:id', component: PostCategoriesShow, name: 'post_categories.show' },
    { path: '/post-categories/:id/edit', component: PostCategoriesEdit, name: 'post_categories.edit' },
    { path: '/product-comments', component: ProductCommentsIndex, name: 'product_comments.index' },
    { path: '/product-comments/create', component: ProductCommentsCreate, name: 'product_comments.create' },
    { path: '/product-comments/:id', component: ProductCommentsShow, name: 'product_comments.show' },
    { path: '/product-comments/:id/edit', component: ProductCommentsEdit, name: 'product_comments.edit' },
    { path: '/posts', component: PostsIndex, name: 'posts.index' },
    { path: '/posts/create', component: PostsCreate, name: 'posts.create' },
    { path: '/posts/:id', component: PostsShow, name: 'posts.show' },
    { path: '/posts/:id/edit', component: PostsEdit, name: 'posts.edit' },
    { path: '/post-tags', component: PostTagsIndex, name: 'post_tags.index' },
    { path: '/post-tags/create', component: PostTagsCreate, name: 'post_tags.create' },
    { path: '/post-tags/:id', component: PostTagsShow, name: 'post_tags.show' },
    { path: '/post-tags/:id/edit', component: PostTagsEdit, name: 'post_tags.edit' },
    { path: '/post-fragments', component: PostFragmentsIndex, name: 'post_fragments.index' },
    { path: '/post-fragments/create', component: PostFragmentsCreate, name: 'post_fragments.create' },
    { path: '/post-fragments/:id', component: PostFragmentsShow, name: 'post_fragments.show' },
    { path: '/post-fragments/:id/edit', component: PostFragmentsEdit, name: 'post_fragments.edit' },
    { path: '/shopping-carts', component: ShoppingCartsIndex, name: 'shopping_carts.index' },
    { path: '/shopping-carts/:id', component: ShoppingCartsShow, name: 'shopping_carts.show' },
    { path: '/orders', component: OrdersIndex, name: 'orders.index' },
    { path: '/orders/create', component: OrdersCreate, name: 'orders.create' },
    { path: '/orders/:id', component: OrdersShow, name: 'orders.show' },
    { path: '/orders/:id/edit', component: OrdersEdit, name: 'orders.edit' },
    { path: '/post-comments', component: PostCommentsIndex, name: 'post_comments.index' },
    { path: '/post-comments/:id', component: PostCommentsShow, name: 'post_comments.show' },
    { path: '/fragment-comments', component: FragmentCommentsIndex, name: 'fragment_comments.index' },
    { path: '/fragment-comments/create', component: FragmentCommentsCreate, name: 'fragment_comments.create' },
    { path: '/fragment-comments/:id', component: FragmentCommentsShow, name: 'fragment_comments.show' },
    { path: '/fragment-comments/:id/edit', component: FragmentCommentsEdit, name: 'fragment_comments.edit' },
    { path: '/paypals', component: PaypalsIndex, name: 'paypals.index' },
    { path: '/paypals/:id', component: PaypalsShow, name: 'paypals.show' },
    { path: '/chats', component: ChatsIndex, name: 'chats.index' },
    { path: '/chats/create', component: ChatsCreate, name: 'chats.create' },
    { path: '/chats/:id', component: ChatsShow, name: 'chats.show' },
    { path: '/chats/:id/edit', component: ChatsEdit, name: 'chats.edit' },
    { path: '/messages', component: MessagesIndex, name: 'messages.index' },
    { path: '/messages/create', component: MessagesCreate, name: 'messages.create' },
    { path: '/messages/:id', component: MessagesShow, name: 'messages.show' },
    { path: '/messages/:id/edit', component: MessagesEdit, name: 'messages.edit' },
]

export default new VueRouter({
    mode: 'history',
    base: '/admin',
    routes
})
