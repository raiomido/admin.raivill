@inject('request', 'Illuminate\Http\Request')
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">

            <li>
                <a href="{{ url('/') }}">
                    <i class="fa fa-wrench"></i>
                    <span class="title">@lang('quickadmin.qa_dashboard')</span>
                </a>
            </li>

            <li class="treeview" v-if="$can('user_management_access')">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>@lang('quickadmin.user-management.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li v-if="$can('permission_access')">
                        <router-link :to="{ name: 'permissions.index' }">
                            <i class="fa fa-briefcase"></i>
                            <span>@lang('quickadmin.permissions.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('role_access')">
                        <router-link :to="{ name: 'roles.index' }">
                            <i class="fa fa-briefcase"></i>
                            <span>@lang('quickadmin.roles.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('user_access')">
                        <router-link :to="{ name: 'users.index' }">
                            <i class="fa fa-user"></i>
                            <span>@lang('quickadmin.users.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('user_action_access')">
                        <router-link :to="{ name: 'user_actions.index' }">
                            <i class="fa fa-th-list"></i>
                            <span>@lang('quickadmin.user-actions.title')</span>
                        </router-link>
                    </li>
                </ul>
            </li>
            <li class="treeview" v-if="$can('crm_access')">
                <a href="#">
                    <i class="fa fa-briefcase"></i>
                    <span>@lang('quickadmin.crm.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li v-if="$can('crm_status_access')">
                        <router-link :to="{ name: 'crm_statuses.index' }">
                            <i class="fa fa-folder"></i>
                            <span>@lang('quickadmin.crm-statuses.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('crm_customer_access')">
                        <router-link :to="{ name: 'crm_customers.index' }">
                            <i class="fa fa-user-plus"></i>
                            <span>@lang('quickadmin.crm-customers.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('crm_note_access')">
                        <router-link :to="{ name: 'crm_notes.index' }">
                            <i class="fa fa-building-o"></i>
                            <span>@lang('quickadmin.crm-notes.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('crm_document_access')">
                        <router-link :to="{ name: 'crm_documents.index' }">
                            <i class="fa fa-file"></i>
                            <span>@lang('quickadmin.crm-documents.title')</span>
                        </router-link>
                    </li>
                </ul>
            </li>
            <li class="treeview" v-if="$can('content_management_access')">
                <a href="#">
                    <i class="fa fa-book"></i>
                    <span>@lang('quickadmin.content-management.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li v-if="$can('content_category_access')">
                        <router-link :to="{ name: 'content_categories.index' }">
                            <i class="fa fa-folder"></i>
                            <span>@lang('quickadmin.content-categories.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('content_tag_access')">
                        <router-link :to="{ name: 'content_tags.index' }">
                            <i class="fa fa-tags"></i>
                            <span>@lang('quickadmin.content-tags.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('content_page_access')">
                        <router-link :to="{ name: 'content_pages.index' }">
                            <i class="fa fa-file-o"></i>
                            <span>@lang('quickadmin.content-pages.title')</span>
                        </router-link>
                    </li>
                </ul>
            </li>
            <li class="treeview" v-if="$can('catalog_access')">
                <a href="#">
                    <i class="fa fa-gears"></i>
                    <span>@lang('quickadmin.catalog.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li v-if="$can('package_access')">
                        <router-link :to="{ name: 'packages.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.package.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('package_item_access')">
                        <router-link :to="{ name: 'package_items.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.package-item.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('product_category_access')">
                        <router-link :to="{ name: 'product_categories.index' }">
                            <i class="fa fa-adjust"></i>
                            <span>@lang('quickadmin.product-categories.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('product_supplier_access')">
                        <router-link :to="{ name: 'product_suppliers.index' }">
                            <i class="fa fa-adjust"></i>
                            <span>@lang('quickadmin.product-suppliers.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('product_access')">
                        <router-link :to="{ name: 'products.index' }">
                            <i class="fa fa-adjust"></i>
                            <span>@lang('quickadmin.products.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('product_comment_access')">
                        <router-link :to="{ name: 'product_comments.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.product-comments.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('order_access')">
                        <router-link :to="{ name: 'orders.index' }">
                            <i class="fa fa-adjust"></i>
                            <span>@lang('quickadmin.orders.title')</span>
                        </router-link>
                    </li>
                </ul>
            </li>
            <li class="treeview" v-if="$can('report_access')">
                <a href="#">
                    <i class="fa fa-adjust"></i>
                    <span>@lang('quickadmin.reports.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li v-if="$can('quote_request_access')">
                        <router-link :to="{ name: 'quote_requests.index' }">
                            <i class="fa fa-adjust"></i>
                            <span>@lang('quickadmin.quote-requests.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('transaction_access')">
                        <router-link :to="{ name: 'transactions.index' }">
                            <i class="fa fa-shopping-cart"></i>
                            <span>@lang('quickadmin.transactions.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('shopping_cart_access')">
                        <router-link :to="{ name: 'shopping_carts.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.shopping-cart.title')</span>
                        </router-link>
                    </li>
                </ul>
            </li>
            <li class="treeview" v-if="$can('setting_access')">
                <a href="#">
                    <i class="fa fa-gears"></i>
                    <span>@lang('quickadmin.settings.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li v-if="$can('currency_access')">
                        <router-link :to="{ name: 'currencies.index' }">
                            <i class="fa fa-money"></i>
                            <span>@lang('quickadmin.currencies.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('country_access')">
                        <router-link :to="{ name: 'countries.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.countries.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('language_access')">
                        <router-link :to="{ name: 'languages.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.languages.title')</span>
                        </router-link>
                    </li>
                </ul>
            </li>
            <li class="treeview" v-if="$can('transaction_log_access')">
                <a href="#">
                    <i class="fa fa-gears"></i>
                    <span>@lang('quickadmin.transaction-logs.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li v-if="$can('mpesa_access')">
                        <router-link :to="{ name: 'mpesas.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.mpesa.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('pk_airtime_access')">
                        <router-link :to="{ name: 'pk_airtimes.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.pk-airtime.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('pk_token_access')">
                        <router-link :to="{ name: 'pk_tokens.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.pk-tokens.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('paypal_access')">
                        <router-link :to="{ name: 'paypals.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.paypal.title')</span>
                        </router-link>
                    </li>
                </ul>
            </li>
            <li class="treeview" v-if="$can('accounts_management_access')">
                <a href="#">
                    <i class="fa fa-gears"></i>
                    <span>@lang('quickadmin.accounts-management.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li v-if="$can('account_access')">
                        <router-link :to="{ name: 'accounts.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.accounts.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('raivill_account_access')">
                        <router-link :to="{ name: 'raivill_accounts.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.raivill-accounts.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('account_entry_access')">
                        <router-link :to="{ name: 'account_entries.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.account-entries.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('raivill_account_entry_access')">
                        <router-link :to="{ name: 'raivill_account_entries.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.raivill-account-entries.title')</span>
                        </router-link>
                    </li>
                </ul>
            </li>
            <li class="treeview" v-if="$can('contact_management_access')">
                <a href="#">
                    <i class="fa fa-phone-square"></i>
                    <span>@lang('quickadmin.contact-management.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li v-if="$can('contact_company_access')">
                        <router-link :to="{ name: 'contact_companies.index' }">
                            <i class="fa fa-building-o"></i>
                            <span>@lang('quickadmin.contact-companies.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('contact_access')">
                        <router-link :to="{ name: 'contacts.index' }">
                            <i class="fa fa-user-plus"></i>
                            <span>@lang('quickadmin.contacts.title')</span>
                        </router-link>
                    </li>
                </ul>
            </li>
            <li class="treeview" v-if="$can('faq_management_access')">
                <a href="#">
                    <i class="fa fa-question"></i>
                    <span>@lang('quickadmin.faq-management.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li v-if="$can('faq_category_access')">
                        <router-link :to="{ name: 'faq_categories.index' }">
                            <i class="fa fa-briefcase"></i>
                            <span>@lang('quickadmin.faq-categories.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('faq_question_access')">
                        <router-link :to="{ name: 'faq_questions.index' }">
                            <i class="fa fa-question"></i>
                            <span>@lang('quickadmin.faq-questions.title')</span>
                        </router-link>
                    </li>
                </ul>
            </li>
            <li class="treeview" v-if="$can('blog_management_access')">
                <a href="#">
                    <i class="fa fa-gears"></i>
                    <span>@lang('quickadmin.blog-management.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li v-if="$can('post_category_access')">
                        <router-link :to="{ name: 'post_categories.index' }">
                            <i class="fa fa-adjust"></i>
                            <span>@lang('quickadmin.post-categories.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('post_access')">
                        <router-link :to="{ name: 'posts.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.posts.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('post_tag_access')">
                        <router-link :to="{ name: 'post_tags.index' }">
                            <i class="fa fa-adjust"></i>
                            <span>@lang('quickadmin.post-tags.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('post_fragment_access')">
                        <router-link :to="{ name: 'post_fragments.index' }">
                            <i class="fa fa-adjust"></i>
                            <span>@lang('quickadmin.post-fragments.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('post_comment_access')">
                        <router-link :to="{ name: 'post_comments.index' }">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.post-comments.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('fragment_comment_access')">
                        <router-link :to="{ name: 'fragment_comments.index' }">
                            <i class="fa fa-adjust"></i>
                            <span>@lang('quickadmin.fragment-comments.title')</span>
                        </router-link>
                    </li>
                </ul>
            </li>
            <li class="treeview" v-if="$can('message_center_access')">
                <a href="#">
                    <i class="fa fa-comments"></i>
                    <span>@lang('quickadmin.message-center.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li v-if="$can('chat_access')">
                        <router-link :to="{ name: 'chats.index' }">
                            <i class="fa fa-comments"></i>
                            <span>@lang('quickadmin.chats.title')</span>
                        </router-link>
                    </li>
                    <li v-if="$can('message_access')">
                        <router-link :to="{ name: 'messages.index' }">
                            <i class="fa fa-adjust"></i>
                            <span>@lang('quickadmin.messages.title')</span>
                        </router-link>
                    </li>
                </ul>
            </li>

            <li>
                <router-link :to="{ name: 'auth.change_password' }">
                    <i class="fa fa-key"></i>
                    <span class="title">@lang('quickadmin.qa_change_password')</span>
                </router-link>
            </li>

            <li>
                <a href="#logout" onclick="$('#logout').submit();">
                    <i class="fa fa-arrow-left"></i>
                    <span class="title">@lang('quickadmin.qa_logout')</span>
                </a>
            </li>
        </ul>
    </section>
</aside>
