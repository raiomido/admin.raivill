@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.threads.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.threads.fields.title')</th>
                            <td field-key='title'>{{ $thread->title }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.threads.fields.active')</th>
                            <td field-key='active'>{{ $thread->active }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.threads.fields.slug')</th>
                            <td field-key='slug'>{{ $thread->slug }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.threads.fields.created-by')</th>
                            <td field-key='created_by'>{{ $thread->created_by->name or '' }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.threads.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
