@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.users.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.users.fields.name')</th>
                            <td field-key='name'>{{ $user->name }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.email')</th>
                            <td field-key='email'>{{ $user->email }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.role')</th>
                            <td field-key='role'>
                                @foreach ($user->role as $singleRole)
                                    <span class="label label-info label-many">{{ $singleRole->title }}</span>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.firstname')</th>
                            <td field-key='firstname'>{{ $user->firstname }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.lastname')</th>
                            <td field-key='lastname'>{{ $user->lastname }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.avatar')</th>
                            <td field-key='avatar'>@if($user->avatar)<a href="{{ asset(env('UPLOAD_PATH').'/' . $user->avatar) }}" target="_blank"><img src="{{ asset(env('UPLOAD_PATH').'/thumb/' . $user->avatar) }}"/></a>@endif</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.newsletter')</th>
                            <td field-key='newsletter'>{{ $user->newsletter }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.about')</th>
                            <td field-key='about'>{!! $user->about !!}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.created-by')</th>
                            <td field-key='created_by'>{{ $user->created_by->name or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.bill-paid-at')</th>
                            <td field-key='bill_paid_at'>{{ $user->bill_paid_at }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.email-verified-at')</th>
                            <td field-key='email_verified_at'>{{ $user->email_verified_at }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.address-one')</th>
                            <td field-key='address_one'>{{ $user->address_one }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.address-two')</th>
                            <td field-key='address_two'>{{ $user->address_two }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.postal-code')</th>
                            <td field-key='postal_code'>{{ $user->postal_code }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.city')</th>
                            <td field-key='city'>{{ $user->city }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.status')</th>
                            <td field-key='status'>{{ $user->status }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.country')</th>
                            <td field-key='country'>{{ $user->country->title or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.language')</th>
                            <td field-key='language'>{{ $user->language->title or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.currency')</th>
                            <td field-key='currency'>{{ $user->currency->title or '' }}</td>
                        </tr>
                    </table>
                </div>
            </div><!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    
<li role="presentation" class="active"><a href="#paypal" aria-controls="paypal" role="tab" data-toggle="tab">Paypal</a></li>
<li role="presentation" class=""><a href="#transactions" aria-controls="transactions" role="tab" data-toggle="tab">Transactions</a></li>
<li role="presentation" class=""><a href="#mpesa" aria-controls="mpesa" role="tab" data-toggle="tab">Mpesa</a></li>
<li role="presentation" class=""><a href="#fragment_comments" aria-controls="fragment_comments" role="tab" data-toggle="tab">Fragment Comments</a></li>
<li role="presentation" class=""><a href="#product_comments" aria-controls="product_comments" role="tab" data-toggle="tab">Product comments</a></li>
<li role="presentation" class=""><a href="#user_actions" aria-controls="user_actions" role="tab" data-toggle="tab">User actions</a></li>
<li role="presentation" class=""><a href="#post_comments" aria-controls="post_comments" role="tab" data-toggle="tab">Post Comments</a></li>
<li role="presentation" class=""><a href="#countries" aria-controls="countries" role="tab" data-toggle="tab">Countries</a></li>
<li role="presentation" class=""><a href="#accounts" aria-controls="accounts" role="tab" data-toggle="tab">Accounts</a></li>
<li role="presentation" class=""><a href="#accounts" aria-controls="accounts" role="tab" data-toggle="tab">Accounts</a></li>
<li role="presentation" class=""><a href="#raivill_accounts" aria-controls="raivill_accounts" role="tab" data-toggle="tab">Raivill accounts</a></li>
<li role="presentation" class=""><a href="#post_categories" aria-controls="post_categories" role="tab" data-toggle="tab">Post Categories</a></li>
<li role="presentation" class=""><a href="#product_categories" aria-controls="product_categories" role="tab" data-toggle="tab">Product Categories</a></li>
<li role="presentation" class=""><a href="#post_tags" aria-controls="post_tags" role="tab" data-toggle="tab">Post Tags</a></li>
<li role="presentation" class=""><a href="#shopping_cart" aria-controls="shopping_cart" role="tab" data-toggle="tab">Shopping Cart</a></li>
<li role="presentation" class=""><a href="#package" aria-controls="package" role="tab" data-toggle="tab">Package</a></li>
<li role="presentation" class=""><a href="#languages" aria-controls="languages" role="tab" data-toggle="tab">Languages</a></li>
<li role="presentation" class=""><a href="#product_comments" aria-controls="product_comments" role="tab" data-toggle="tab">Product comments</a></li>
<li role="presentation" class=""><a href="#post_fragments" aria-controls="post_fragments" role="tab" data-toggle="tab">Post fragments</a></li>
<li role="presentation" class=""><a href="#post_comments" aria-controls="post_comments" role="tab" data-toggle="tab">Post Comments</a></li>
<li role="presentation" class=""><a href="#fragment_comments" aria-controls="fragment_comments" role="tab" data-toggle="tab">Fragment Comments</a></li>
<li role="presentation" class=""><a href="#posts" aria-controls="posts" role="tab" data-toggle="tab">Posts</a></li>
<li role="presentation" class=""><a href="#package_item" aria-controls="package_item" role="tab" data-toggle="tab">Package Item</a></li>
<li role="presentation" class=""><a href="#orders" aria-controls="orders" role="tab" data-toggle="tab">Orders</a></li>
<li role="presentation" class=""><a href="#quote_requests" aria-controls="quote_requests" role="tab" data-toggle="tab">Quote Requests</a></li>
<li role="presentation" class=""><a href="#currencies" aria-controls="currencies" role="tab" data-toggle="tab">Currencies</a></li>
<li role="presentation" class=""><a href="#product_suppliers" aria-controls="product_suppliers" role="tab" data-toggle="tab">Product Suppliers</a></li>
<li role="presentation" class=""><a href="#products" aria-controls="products" role="tab" data-toggle="tab">Products</a></li>
<li role="presentation" class=""><a href="#users" aria-controls="users" role="tab" data-toggle="tab">Users</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    
<div role="tabpanel" class="tab-pane active" id="paypal">
<table class="table table-bordered table-striped {{ count($paypals) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.paypal.fields.user')</th>
                        <th>@lang('quickadmin.paypal.fields.transaction')</th>
                        <th>@lang('quickadmin.paypal.fields.create-time')</th>
                        <th>@lang('quickadmin.paypal.fields.update-time')</th>
                        <th>@lang('quickadmin.paypal.fields.paypal-id')</th>
                        <th>@lang('quickadmin.paypal.fields.email')</th>
                        <th>@lang('quickadmin.paypal.fields.country-code')</th>
                        <th>@lang('quickadmin.paypal.fields.firstname')</th>
                        <th>@lang('quickadmin.paypal.fields.lastname')</th>
                        <th>@lang('quickadmin.paypal.fields.amount')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($paypals) > 0)
            @foreach ($paypals as $paypal)
                <tr data-entry-id="{{ $paypal->id }}">
                    <td field-key='user'>{{ $paypal->user->name or '' }}</td>
                                <td field-key='transaction'>{{ $paypal->transaction->reference or '' }}</td>
                                <td field-key='create_time'>{{ $paypal->create_time }}</td>
                                <td field-key='update_time'>{{ $paypal->update_time }}</td>
                                <td field-key='paypal_id'>{{ $paypal->paypal_id }}</td>
                                <td field-key='email'>{{ $paypal->email }}</td>
                                <td field-key='country_code'>{{ $paypal->country_code }}</td>
                                <td field-key='firstname'>{{ $paypal->firstname }}</td>
                                <td field-key='lastname'>{{ $paypal->lastname }}</td>
                                <td field-key='amount'>{{ $paypal->amount }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['paypals.restore', $paypal->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['paypals.perma_del', $paypal->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('paypals.show',[$paypal->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['paypals.destroy', $paypal->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="27">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="transactions">
<table class="table table-bordered table-striped {{ count($transactions) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.transactions.fields.user')</th>
                        <th>@lang('quickadmin.transactions.fields.reference')</th>
                        <th>@lang('quickadmin.transactions.fields.type')</th>
                        <th>@lang('quickadmin.transactions.fields.amount')</th>
                        <th>@lang('quickadmin.transactions.fields.cost')</th>
                        <th>@lang('quickadmin.transactions.fields.status')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($transactions) > 0)
            @foreach ($transactions as $transaction)
                <tr data-entry-id="{{ $transaction->id }}">
                    <td field-key='user'>{{ $transaction->user->name or '' }}</td>
                                <td field-key='reference'>{{ $transaction->reference }}</td>
                                <td field-key='type'>{{ $transaction->type }}</td>
                                <td field-key='amount'>{{ $transaction->amount }}</td>
                                <td field-key='cost'>{{ $transaction->cost }}</td>
                                <td field-key='status'>{{ $transaction->status }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['transactions.restore', $transaction->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['transactions.perma_del', $transaction->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('transactions.show',[$transaction->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['transactions.destroy', $transaction->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="11">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="mpesa">
<table class="table table-bordered table-striped {{ count($mpesas) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.mpesa.fields.user')</th>
                        <th>@lang('quickadmin.mpesa.fields.transaction')</th>
                        <th>@lang('quickadmin.mpesa.fields.type')</th>
                        <th>@lang('quickadmin.mpesa.fields.time')</th>
                        <th>@lang('quickadmin.mpesa.fields.mpesa-transaction-id')</th>
                        <th>@lang('quickadmin.mpesa.fields.amount')</th>
                        <th>@lang('quickadmin.mpesa.fields.short-code')</th>
                        <th>@lang('quickadmin.mpesa.fields.firstname')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($mpesas) > 0)
            @foreach ($mpesas as $mpesa)
                <tr data-entry-id="{{ $mpesa->id }}">
                    <td field-key='user'>{{ $mpesa->user->name or '' }}</td>
                                <td field-key='transaction'>{{ $mpesa->transaction->reference or '' }}</td>
                                <td field-key='type'>{{ $mpesa->type }}</td>
                                <td field-key='time'>{{ $mpesa->time }}</td>
                                <td field-key='mpesa_transaction_id'>{{ $mpesa->mpesa_transaction_id }}</td>
                                <td field-key='amount'>{{ $mpesa->amount }}</td>
                                <td field-key='short_code'>{{ $mpesa->short_code }}</td>
                                <td field-key='firstname'>{{ $mpesa->firstname }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['mpesas.restore', $mpesa->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['mpesas.perma_del', $mpesa->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('mpesas.show',[$mpesa->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['mpesas.destroy', $mpesa->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="19">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="fragment_comments">
<table class="table table-bordered table-striped {{ count($fragment_comments) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.fragment-comments.fields.user')</th>
                        <th>@lang('quickadmin.fragment-comments.fields.active')</th>
                        <th>@lang('quickadmin.fragment-comments.fields.fragment')</th>
                        <th>@lang('quickadmin.fragment-comments.fields.created-by')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($fragment_comments) > 0)
            @foreach ($fragment_comments as $fragment_comment)
                <tr data-entry-id="{{ $fragment_comment->id }}">
                    <td field-key='user'>{{ $fragment_comment->user->firstname or '' }}</td>
                                <td field-key='active'>{{ $fragment_comment->active }}</td>
                                <td field-key='fragment'>{{ $fragment_comment->fragment->title or '' }}</td>
                                <td field-key='created_by'>{{ $fragment_comment->created_by->name or '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['fragment_comments.restore', $fragment_comment->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['fragment_comments.perma_del', $fragment_comment->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('fragment_comments.show',[$fragment_comment->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('edit')
                                    <a href="{{ route('fragment_comments.edit',[$fragment_comment->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['fragment_comments.destroy', $fragment_comment->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="product_comments">
<table class="table table-bordered table-striped {{ count($product_comments) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.product-comments.fields.user')</th>
                        <th>@lang('quickadmin.product-comments.fields.product')</th>
                        <th>@lang('quickadmin.product-comments.fields.active')</th>
                        <th>@lang('quickadmin.product-comments.fields.created-by')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($product_comments) > 0)
            @foreach ($product_comments as $product_comment)
                <tr data-entry-id="{{ $product_comment->id }}">
                    <td field-key='user'>{{ $product_comment->user->firstname or '' }}</td>
                                <td field-key='product'>{{ $product_comment->product->name or '' }}</td>
                                <td field-key='active'>{{ $product_comment->active }}</td>
                                <td field-key='created_by'>{{ $product_comment->created_by->name or '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['product_comments.restore', $product_comment->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['product_comments.perma_del', $product_comment->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('product_comments.show',[$product_comment->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('edit')
                                    <a href="{{ route('product_comments.edit',[$product_comment->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['product_comments.destroy', $product_comment->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="user_actions">
<table class="table table-bordered table-striped {{ count($user_actions) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.user-actions.created_at')</th>
                        <th>@lang('quickadmin.user-actions.fields.user')</th>
                        <th>@lang('quickadmin.user-actions.fields.action')</th>
                        <th>@lang('quickadmin.user-actions.fields.action-model')</th>
                        <th>@lang('quickadmin.user-actions.fields.action-id')</th>
                        
        </tr>
    </thead>

    <tbody>
        @if (count($user_actions) > 0)
            @foreach ($user_actions as $user_action)
                <tr data-entry-id="{{ $user_action->id }}">
                    <td>{{ $user_action->created_at or '' }}</td>
                                <td field-key='user'>{{ $user_action->user->name or '' }}</td>
                                <td field-key='action'>{{ $user_action->action }}</td>
                                <td field-key='action_model'>{{ $user_action->action_model }}</td>
                                <td field-key='action_id'>{{ $user_action->action_id }}</td>
                                
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="7">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="post_comments">
<table class="table table-bordered table-striped {{ count($post_comments) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.post-comments.fields.user')</th>
                        <th>@lang('quickadmin.post-comments.fields.post')</th>
                        <th>@lang('quickadmin.post-comments.fields.active')</th>
                        <th>@lang('quickadmin.post-comments.fields.created-by')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($post_comments) > 0)
            @foreach ($post_comments as $post_comment)
                <tr data-entry-id="{{ $post_comment->id }}">
                    <td field-key='user'>{{ $post_comment->user->firstname or '' }}</td>
                                <td field-key='post'>{{ $post_comment->post->title or '' }}</td>
                                <td field-key='active'>{{ $post_comment->active }}</td>
                                <td field-key='created_by'>{{ $post_comment->created_by->name or '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['post_comments.restore', $post_comment->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['post_comments.perma_del', $post_comment->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('post_comments.show',[$post_comment->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['post_comments.destroy', $post_comment->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="countries">
<table class="table table-bordered table-striped {{ count($countries) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.countries.fields.shortcode')</th>
                        <th>@lang('quickadmin.countries.fields.title')</th>
                        <th>@lang('quickadmin.countries.fields.created-by')</th>
                        <th>@lang('quickadmin.countries.fields.flag')</th>
                        <th>@lang('quickadmin.countries.fields.code')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($countries) > 0)
            @foreach ($countries as $country)
                <tr data-entry-id="{{ $country->id }}">
                    <td field-key='shortcode'>{{ $country->shortcode }}</td>
                                <td field-key='title'>{{ $country->title }}</td>
                                <td field-key='created_by'>{{ $country->created_by->name or '' }}</td>
                                <td field-key='flag'>@if($country->flag)<a href="{{ asset(env('UPLOAD_PATH').'/' . $country->flag) }}" target="_blank"><img src="{{ asset(env('UPLOAD_PATH').'/thumb/' . $country->flag) }}"/></a>@endif</td>
                                <td field-key='code'>{{ $country->code }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['countries.restore', $country->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['countries.perma_del', $country->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('countries.show',[$country->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('edit')
                                    <a href="{{ route('countries.edit',[$country->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['countries.destroy', $country->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="accounts">
<table class="table table-bordered table-striped {{ count($accounts) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.accounts.fields.number')</th>
                        <th>@lang('quickadmin.accounts.fields.balance')</th>
                        <th>@lang('quickadmin.accounts.fields.user')</th>
                        <th>@lang('quickadmin.accounts.fields.created-by')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($accounts) > 0)
            @foreach ($accounts as $account)
                <tr data-entry-id="{{ $account->id }}">
                    <td field-key='number'>{{ $account->number }}</td>
                                <td field-key='balance'>{{ $account->balance }}</td>
                                <td field-key='user'>{{ $account->user->name or '' }}</td>
                                <td field-key='created_by'>{{ $account->created_by->name or '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['accounts.restore', $account->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['accounts.perma_del', $account->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('accounts.show',[$account->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['accounts.destroy', $account->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="9">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="accounts">
<table class="table table-bordered table-striped {{ count($accounts) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.accounts.fields.number')</th>
                        <th>@lang('quickadmin.accounts.fields.balance')</th>
                        <th>@lang('quickadmin.accounts.fields.user')</th>
                        <th>@lang('quickadmin.accounts.fields.created-by')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($accounts) > 0)
            @foreach ($accounts as $account)
                <tr data-entry-id="{{ $account->id }}">
                    <td field-key='number'>{{ $account->number }}</td>
                                <td field-key='balance'>{{ $account->balance }}</td>
                                <td field-key='user'>{{ $account->user->name or '' }}</td>
                                <td field-key='created_by'>{{ $account->created_by->name or '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['accounts.restore', $account->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['accounts.perma_del', $account->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('accounts.show',[$account->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['accounts.destroy', $account->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="9">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="raivill_accounts">
<table class="table table-bordered table-striped {{ count($raivill_accounts) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.raivill-accounts.fields.number')</th>
                        <th>@lang('quickadmin.raivill-accounts.fields.balance')</th>
                        <th>@lang('quickadmin.raivill-accounts.fields.name')</th>
                        <th>@lang('quickadmin.raivill-accounts.fields.created-by')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($raivill_accounts) > 0)
            @foreach ($raivill_accounts as $raivill_account)
                <tr data-entry-id="{{ $raivill_account->id }}">
                    <td field-key='number'>{{ $raivill_account->number }}</td>
                                <td field-key='balance'>{{ $raivill_account->balance }}</td>
                                <td field-key='name'>{{ $raivill_account->name }}</td>
                                <td field-key='created_by'>{{ $raivill_account->created_by->name or '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['raivill_accounts.restore', $raivill_account->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['raivill_accounts.perma_del', $raivill_account->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('raivill_accounts.show',[$raivill_account->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['raivill_accounts.destroy', $raivill_account->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="9">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="post_categories">
<table class="table table-bordered table-striped {{ count($post_categories) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.post-categories.fields.name')</th>
                        <th>@lang('quickadmin.post-categories.fields.slug')</th>
                        <th>@lang('quickadmin.post-categories.fields.active')</th>
                        <th>@lang('quickadmin.post-categories.fields.created-by')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($post_categories) > 0)
            @foreach ($post_categories as $post_category)
                <tr data-entry-id="{{ $post_category->id }}">
                    <td field-key='name'>{{ $post_category->name }}</td>
                                <td field-key='slug'>{{ $post_category->slug }}</td>
                                <td field-key='active'>{{ $post_category->active }}</td>
                                <td field-key='created_by'>{{ $post_category->created_by->name or '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['post_categories.restore', $post_category->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['post_categories.perma_del', $post_category->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('post_categories.show',[$post_category->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('edit')
                                    <a href="{{ route('post_categories.edit',[$post_category->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['post_categories.destroy', $post_category->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="9">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="product_categories">
<table class="table table-bordered table-striped {{ count($product_categories) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.product-categories.fields.name')</th>
                        <th>@lang('quickadmin.product-categories.fields.slug')</th>
                        <th>@lang('quickadmin.product-categories.fields.active')</th>
                        <th>@lang('quickadmin.product-categories.fields.created-by')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($product_categories) > 0)
            @foreach ($product_categories as $product_category)
                <tr data-entry-id="{{ $product_category->id }}">
                    <td field-key='name'>{{ $product_category->name }}</td>
                                <td field-key='slug'>{{ $product_category->slug }}</td>
                                <td field-key='active'>{{ $product_category->active }}</td>
                                <td field-key='created_by'>{{ $product_category->created_by->name or '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['product_categories.restore', $product_category->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['product_categories.perma_del', $product_category->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('product_categories.show',[$product_category->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('edit')
                                    <a href="{{ route('product_categories.edit',[$product_category->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['product_categories.destroy', $product_category->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="9">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="post_tags">
<table class="table table-bordered table-striped {{ count($post_tags) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.post-tags.fields.name')</th>
                        <th>@lang('quickadmin.post-tags.fields.slug')</th>
                        <th>@lang('quickadmin.post-tags.fields.active')</th>
                        <th>@lang('quickadmin.post-tags.fields.created-by')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($post_tags) > 0)
            @foreach ($post_tags as $post_tag)
                <tr data-entry-id="{{ $post_tag->id }}">
                    <td field-key='name'>{{ $post_tag->name }}</td>
                                <td field-key='slug'>{{ $post_tag->slug }}</td>
                                <td field-key='active'>{{ $post_tag->active }}</td>
                                <td field-key='created_by'>{{ $post_tag->created_by->name or '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['post_tags.restore', $post_tag->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['post_tags.perma_del', $post_tag->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('post_tags.show',[$post_tag->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('edit')
                                    <a href="{{ route('post_tags.edit',[$post_tag->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['post_tags.destroy', $post_tag->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="9">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="shopping_cart">
<table class="table table-bordered table-striped {{ count($shopping_carts) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.shopping-cart.fields.identifier')</th>
                        <th>@lang('quickadmin.shopping-cart.fields.instance')</th>
                        <th>@lang('quickadmin.shopping-cart.fields.created-by')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($shopping_carts) > 0)
            @foreach ($shopping_carts as $shopping_cart)
                <tr data-entry-id="{{ $shopping_cart->id }}">
                    <td field-key='identifier'>{{ $shopping_cart->identifier }}</td>
                                <td field-key='instance'>{{ $shopping_cart->instance }}</td>
                                <td field-key='created_by'>{{ $shopping_cart->created_by->name or '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['shopping_carts.restore', $shopping_cart->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['shopping_carts.perma_del', $shopping_cart->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('shopping_carts.show',[$shopping_cart->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['shopping_carts.destroy', $shopping_cart->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="9">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="package">
<table class="table table-bordered table-striped {{ count($packages) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.package.fields.name')</th>
                        <th>@lang('quickadmin.package.fields.slug')</th>
                        <th>@lang('quickadmin.package.fields.active')</th>
                        <th>@lang('quickadmin.package.fields.created-by')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($packages) > 0)
            @foreach ($packages as $package)
                <tr data-entry-id="{{ $package->id }}">
                    <td field-key='name'>{{ $package->name }}</td>
                                <td field-key='slug'>{{ $package->slug }}</td>
                                <td field-key='active'>{{ $package->active }}</td>
                                <td field-key='created_by'>{{ $package->created_by->name or '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['packages.restore', $package->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['packages.perma_del', $package->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('packages.show',[$package->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('edit')
                                    <a href="{{ route('packages.edit',[$package->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['packages.destroy', $package->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="9">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="languages">
<table class="table table-bordered table-striped {{ count($languages) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.languages.fields.title')</th>
                        <th>@lang('quickadmin.languages.fields.code')</th>
                        <th>@lang('quickadmin.languages.fields.locale')</th>
                        <th>@lang('quickadmin.languages.fields.image')</th>
                        <th>@lang('quickadmin.languages.fields.created-by')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($languages) > 0)
            @foreach ($languages as $language)
                <tr data-entry-id="{{ $language->id }}">
                    <td field-key='title'>{{ $language->title }}</td>
                                <td field-key='code'>{{ $language->code }}</td>
                                <td field-key='locale'>{{ $language->locale }}</td>
                                <td field-key='image'>@if($language->image)<a href="{{ asset(env('UPLOAD_PATH').'/' . $language->image) }}" target="_blank"><img src="{{ asset(env('UPLOAD_PATH').'/thumb/' . $language->image) }}"/></a>@endif</td>
                                <td field-key='created_by'>{{ $language->created_by->name or '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['languages.restore', $language->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['languages.perma_del', $language->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('languages.show',[$language->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('edit')
                                    <a href="{{ route('languages.edit',[$language->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['languages.destroy', $language->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="product_comments">
<table class="table table-bordered table-striped {{ count($product_comments) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.product-comments.fields.user')</th>
                        <th>@lang('quickadmin.product-comments.fields.product')</th>
                        <th>@lang('quickadmin.product-comments.fields.active')</th>
                        <th>@lang('quickadmin.product-comments.fields.created-by')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($product_comments) > 0)
            @foreach ($product_comments as $product_comment)
                <tr data-entry-id="{{ $product_comment->id }}">
                    <td field-key='user'>{{ $product_comment->user->firstname or '' }}</td>
                                <td field-key='product'>{{ $product_comment->product->name or '' }}</td>
                                <td field-key='active'>{{ $product_comment->active }}</td>
                                <td field-key='created_by'>{{ $product_comment->created_by->name or '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['product_comments.restore', $product_comment->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['product_comments.perma_del', $product_comment->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('product_comments.show',[$product_comment->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('edit')
                                    <a href="{{ route('product_comments.edit',[$product_comment->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['product_comments.destroy', $product_comment->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="post_fragments">
<table class="table table-bordered table-striped {{ count($post_fragments) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.post-fragments.fields.title')</th>
                        <th>@lang('quickadmin.post-fragments.fields.post')</th>
                        <th>@lang('quickadmin.post-fragments.fields.active')</th>
                        <th>@lang('quickadmin.post-fragments.fields.created-by')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($post_fragments) > 0)
            @foreach ($post_fragments as $post_fragment)
                <tr data-entry-id="{{ $post_fragment->id }}">
                    <td field-key='title'>{{ $post_fragment->title }}</td>
                                <td field-key='post'>{{ $post_fragment->post->title or '' }}</td>
                                <td field-key='active'>{{ $post_fragment->active }}</td>
                                <td field-key='created_by'>{{ $post_fragment->created_by->name or '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['post_fragments.restore', $post_fragment->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['post_fragments.perma_del', $post_fragment->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('post_fragments.show',[$post_fragment->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('edit')
                                    <a href="{{ route('post_fragments.edit',[$post_fragment->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['post_fragments.destroy', $post_fragment->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="post_comments">
<table class="table table-bordered table-striped {{ count($post_comments) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.post-comments.fields.user')</th>
                        <th>@lang('quickadmin.post-comments.fields.post')</th>
                        <th>@lang('quickadmin.post-comments.fields.active')</th>
                        <th>@lang('quickadmin.post-comments.fields.created-by')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($post_comments) > 0)
            @foreach ($post_comments as $post_comment)
                <tr data-entry-id="{{ $post_comment->id }}">
                    <td field-key='user'>{{ $post_comment->user->firstname or '' }}</td>
                                <td field-key='post'>{{ $post_comment->post->title or '' }}</td>
                                <td field-key='active'>{{ $post_comment->active }}</td>
                                <td field-key='created_by'>{{ $post_comment->created_by->name or '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['post_comments.restore', $post_comment->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['post_comments.perma_del', $post_comment->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('post_comments.show',[$post_comment->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['post_comments.destroy', $post_comment->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="fragment_comments">
<table class="table table-bordered table-striped {{ count($fragment_comments) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.fragment-comments.fields.user')</th>
                        <th>@lang('quickadmin.fragment-comments.fields.active')</th>
                        <th>@lang('quickadmin.fragment-comments.fields.fragment')</th>
                        <th>@lang('quickadmin.fragment-comments.fields.created-by')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($fragment_comments) > 0)
            @foreach ($fragment_comments as $fragment_comment)
                <tr data-entry-id="{{ $fragment_comment->id }}">
                    <td field-key='user'>{{ $fragment_comment->user->firstname or '' }}</td>
                                <td field-key='active'>{{ $fragment_comment->active }}</td>
                                <td field-key='fragment'>{{ $fragment_comment->fragment->title or '' }}</td>
                                <td field-key='created_by'>{{ $fragment_comment->created_by->name or '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['fragment_comments.restore', $fragment_comment->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['fragment_comments.perma_del', $fragment_comment->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('fragment_comments.show',[$fragment_comment->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('edit')
                                    <a href="{{ route('fragment_comments.edit',[$fragment_comment->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['fragment_comments.destroy', $fragment_comment->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="posts">
<table class="table table-bordered table-striped {{ count($posts) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.posts.fields.title')</th>
                        <th>@lang('quickadmin.posts.fields.slug')</th>
                        <th>@lang('quickadmin.posts.fields.product')</th>
                        <th>@lang('quickadmin.posts.fields.active')</th>
                        <th>@lang('quickadmin.posts.fields.created-by')</th>
                        <th>@lang('quickadmin.posts.fields.tags')</th>
                        <th>@lang('quickadmin.posts.fields.categories')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($posts) > 0)
            @foreach ($posts as $post)
                <tr data-entry-id="{{ $post->id }}">
                    <td field-key='title'>{{ $post->title }}</td>
                                <td field-key='slug'>{{ $post->slug }}</td>
                                <td field-key='product'>{{ $post->product->name or '' }}</td>
                                <td field-key='active'>{{ $post->active }}</td>
                                <td field-key='created_by'>{{ $post->created_by->name or '' }}</td>
                                <td field-key='tags'>
                                    @foreach ($post->tags as $singleTags)
                                        <span class="label label-info label-many">{{ $singleTags->name }}</span>
                                    @endforeach
                                </td>
                                <td field-key='categories'>
                                    @foreach ($post->categories as $singleCategories)
                                        <span class="label label-info label-many">{{ $singleCategories->name }}</span>
                                    @endforeach
                                </td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['posts.restore', $post->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['posts.perma_del', $post->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('posts.show',[$post->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('edit')
                                    <a href="{{ route('posts.edit',[$post->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['posts.destroy', $post->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="14">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="package_item">
<table class="table table-bordered table-striped {{ count($package_items) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.package-item.fields.package')</th>
                        <th>@lang('quickadmin.package-item.fields.name')</th>
                        <th>@lang('quickadmin.package-item.fields.price')</th>
                        <th>@lang('quickadmin.package-item.fields.active')</th>
                        <th>@lang('quickadmin.package-item.fields.created-by')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($package_items) > 0)
            @foreach ($package_items as $package_item)
                <tr data-entry-id="{{ $package_item->id }}">
                    <td field-key='package'>{{ $package_item->package->name or '' }}</td>
                                <td field-key='name'>{{ $package_item->name }}</td>
                                <td field-key='price'>{{ $package_item->price }}</td>
                                <td field-key='active'>{{ $package_item->active }}</td>
                                <td field-key='created_by'>{{ $package_item->created_by->name or '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['package_items.restore', $package_item->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['package_items.perma_del', $package_item->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('package_items.show',[$package_item->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('edit')
                                    <a href="{{ route('package_items.edit',[$package_item->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['package_items.destroy', $package_item->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="11">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="orders">
<table class="table table-bordered table-striped {{ count($orders) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.orders.fields.product')</th>
                        <th>@lang('quickadmin.orders.fields.reference')</th>
                        <th>@lang('quickadmin.orders.fields.status')</th>
                        <th>@lang('quickadmin.orders.fields.transactions')</th>
                        <th>@lang('quickadmin.orders.fields.active')</th>
                        <th>@lang('quickadmin.orders.fields.created-by')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($orders) > 0)
            @foreach ($orders as $order)
                <tr data-entry-id="{{ $order->id }}">
                    <td field-key='product'>{{ $order->product->name or '' }}</td>
                                <td field-key='reference'>{{ $order->reference }}</td>
                                <td field-key='status'>{{ $order->status }}</td>
                                <td field-key='transactions'>
                                    @foreach ($order->transactions as $singleTransactions)
                                        <span class="label label-info label-many">{{ $singleTransactions->reference }}</span>
                                    @endforeach
                                </td>
                                <td field-key='active'>{{ $order->active }}</td>
                                <td field-key='created_by'>{{ $order->created_by->name or '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['orders.restore', $order->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['orders.perma_del', $order->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('orders.show',[$order->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('edit')
                                    <a href="{{ route('orders.edit',[$order->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['orders.destroy', $order->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="12">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="quote_requests">
<table class="table table-bordered table-striped {{ count($quote_requests) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.quote-requests.fields.firstname')</th>
                        <th>@lang('quickadmin.quote-requests.fields.lastname')</th>
                        <th>@lang('quickadmin.quote-requests.fields.email')</th>
                        <th>@lang('quickadmin.quote-requests.fields.phone')</th>
                        <th>@lang('quickadmin.quote-requests.fields.website')</th>
                        <th>@lang('quickadmin.quote-requests.fields.created-by')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($quote_requests) > 0)
            @foreach ($quote_requests as $quote_request)
                <tr data-entry-id="{{ $quote_request->id }}">
                    <td field-key='firstname'>{{ $quote_request->firstname }}</td>
                                <td field-key='lastname'>{{ $quote_request->lastname }}</td>
                                <td field-key='email'>{{ $quote_request->email }}</td>
                                <td field-key='phone'>{{ $quote_request->phone }}</td>
                                <td field-key='website'>{{ $quote_request->website }}</td>
                                <td field-key='created_by'>{{ $quote_request->created_by->name or '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['quote_requests.restore', $quote_request->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['quote_requests.perma_del', $quote_request->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('quote_requests.show',[$quote_request->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['quote_requests.destroy', $quote_request->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="12">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="currencies">
<table class="table table-bordered table-striped {{ count($currencies) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.currencies.fields.title')</th>
                        <th>@lang('quickadmin.currencies.fields.code')</th>
                        <th>@lang('quickadmin.currencies.fields.symbol-left')</th>
                        <th>@lang('quickadmin.currencies.fields.symbol-right')</th>
                        <th>@lang('quickadmin.currencies.fields.decimal-place')</th>
                        <th>@lang('quickadmin.currencies.fields.value')</th>
                        <th>@lang('quickadmin.currencies.fields.active')</th>
                        <th>@lang('quickadmin.currencies.fields.created-by')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($currencies) > 0)
            @foreach ($currencies as $currency)
                <tr data-entry-id="{{ $currency->id }}">
                    <td field-key='title'>{{ $currency->title }}</td>
                                <td field-key='code'>{{ $currency->code }}</td>
                                <td field-key='symbol_left'>{{ $currency->symbol_left }}</td>
                                <td field-key='symbol_right'>{{ $currency->symbol_right }}</td>
                                <td field-key='decimal_place'>{{ $currency->decimal_place }}</td>
                                <td field-key='value'>{{ $currency->value }}</td>
                                <td field-key='active'>{{ $currency->active }}</td>
                                <td field-key='created_by'>{{ $currency->created_by->name or '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['currencies.restore', $currency->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['currencies.perma_del', $currency->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('currencies.show',[$currency->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('edit')
                                    <a href="{{ route('currencies.edit',[$currency->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['currencies.destroy', $currency->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="13">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="product_suppliers">
<table class="table table-bordered table-striped {{ count($product_suppliers) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.product-suppliers.fields.name')</th>
                        <th>@lang('quickadmin.product-suppliers.fields.slug')</th>
                        <th>@lang('quickadmin.product-suppliers.fields.website')</th>
                        <th>@lang('quickadmin.product-suppliers.fields.image')</th>
                        <th>@lang('quickadmin.product-suppliers.fields.email')</th>
                        <th>@lang('quickadmin.product-suppliers.fields.phone')</th>
                        <th>@lang('quickadmin.product-suppliers.fields.country')</th>
                        <th>@lang('quickadmin.product-suppliers.fields.active')</th>
                        <th>@lang('quickadmin.product-suppliers.fields.created-by')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($product_suppliers) > 0)
            @foreach ($product_suppliers as $product_supplier)
                <tr data-entry-id="{{ $product_supplier->id }}">
                    <td field-key='name'>{{ $product_supplier->name }}</td>
                                <td field-key='slug'>{{ $product_supplier->slug }}</td>
                                <td field-key='website'>{{ $product_supplier->website }}</td>
                                <td field-key='image'>@if($product_supplier->image)<a href="{{ asset(env('UPLOAD_PATH').'/' . $product_supplier->image) }}" target="_blank"><img src="{{ asset(env('UPLOAD_PATH').'/thumb/' . $product_supplier->image) }}"/></a>@endif</td>
                                <td field-key='email'>{{ $product_supplier->email }}</td>
                                <td field-key='phone'>{{ $product_supplier->phone }}</td>
                                <td field-key='country'>{{ $product_supplier->country->shortcode or '' }}</td>
                                <td field-key='active'>{{ $product_supplier->active }}</td>
                                <td field-key='created_by'>{{ $product_supplier->created_by->name or '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['product_suppliers.restore', $product_supplier->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['product_suppliers.perma_del', $product_supplier->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('product_suppliers.show',[$product_supplier->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('edit')
                                    <a href="{{ route('product_suppliers.edit',[$product_supplier->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['product_suppliers.destroy', $product_supplier->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="15">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="products">
<table class="table table-bordered table-striped {{ count($products) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.products.fields.name')</th>
                        <th>@lang('quickadmin.products.fields.price')</th>
                        <th>@lang('quickadmin.products.fields.views')</th>
                        <th>@lang('quickadmin.products.fields.likes-count')</th>
                        <th>@lang('quickadmin.products.fields.comments-count')</th>
                        <th>@lang('quickadmin.products.fields.category')</th>
                        <th>@lang('quickadmin.products.fields.created-by')</th>
                        <th>@lang('quickadmin.products.fields.supplier')</th>
                        <th>@lang('quickadmin.products.fields.reference')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($products) > 0)
            @foreach ($products as $product)
                <tr data-entry-id="{{ $product->id }}">
                    <td field-key='name'>{{ $product->name }}</td>
                                <td field-key='price'>{{ $product->price }}</td>
                                <td field-key='views'>{{ $product->views }}</td>
                                <td field-key='likes_count'>{{ $product->likes_count }}</td>
                                <td field-key='comments_count'>{{ $product->comments_count }}</td>
                                <td field-key='category'>{{ $product->category->name or '' }}</td>
                                <td field-key='created_by'>{{ $product->created_by->name or '' }}</td>
                                <td field-key='supplier'>{{ $product->supplier->name or '' }}</td>
                                <td field-key='reference'>{{ $product->reference }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['products.restore', $product->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['products.perma_del', $product->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                                                </td>
                                @else
                                <td>
                                    @can('view')
                                    <a href="{{ route('products.show',[$product->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('edit')
                                    <a href="{{ route('products.edit',[$product->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['products.destroy', $product->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="20">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="users">
<table class="table table-bordered table-striped {{ count($users) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.users.fields.name')</th>
                        <th>@lang('quickadmin.users.fields.email')</th>
                        <th>@lang('quickadmin.users.fields.role')</th>
                        <th>@lang('quickadmin.users.fields.firstname')</th>
                        <th>@lang('quickadmin.users.fields.lastname')</th>
                        <th>@lang('quickadmin.users.fields.avatar')</th>
                        <th>@lang('quickadmin.users.fields.created-by')</th>
                        <th>@lang('quickadmin.users.fields.status')</th>
                                                <th>&nbsp;</th>

        </tr>
    </thead>

    <tbody>
        @if (count($users) > 0)
            @foreach ($users as $user)
                <tr data-entry-id="{{ $user->id }}">
                    <td field-key='name'>{{ $user->name }}</td>
                                <td field-key='email'>{{ $user->email }}</td>
                                <td field-key='role'>
                                    @foreach ($user->role as $singleRole)
                                        <span class="label label-info label-many">{{ $singleRole->title }}</span>
                                    @endforeach
                                </td>
                                <td field-key='firstname'>{{ $user->firstname }}</td>
                                <td field-key='lastname'>{{ $user->lastname }}</td>
                                <td field-key='avatar'>@if($user->avatar)<a href="{{ asset(env('UPLOAD_PATH').'/' . $user->avatar) }}" target="_blank"><img src="{{ asset(env('UPLOAD_PATH').'/thumb/' . $user->avatar) }}"/></a>@endif</td>
                                <td field-key='created_by'>{{ $user->created_by->name or '' }}</td>
                                <td field-key='status'>{{ $user->status }}</td>
                                                                <td>
                                    @can('view')
                                    <a href="{{ route('users.show',[$user->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('edit')
                                    <a href="{{ route('users.edit',[$user->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['users.destroy', $user->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>

                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="26">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
</div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.users.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
