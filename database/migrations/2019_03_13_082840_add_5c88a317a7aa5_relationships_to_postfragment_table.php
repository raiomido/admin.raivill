<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5c88a317a7aa5RelationshipsToPostFragmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('post_fragments', function(Blueprint $table) {
            if (!Schema::hasColumn('post_fragments', 'post_id')) {
                $table->integer('post_id')->unsigned()->nullable();
                $table->foreign('post_id', '31621_5c88a3170b759')->references('id')->on('posts')->onDelete('cascade');
                }
                if (!Schema::hasColumn('post_fragments', 'created_by_id')) {
                $table->integer('created_by_id')->unsigned()->nullable();
                $table->foreign('created_by_id', '31621_5c88a31718397')->references('id')->on('users')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('post_fragments', function(Blueprint $table) {
            
        });
    }
}
