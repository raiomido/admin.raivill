<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1552369941MpesasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('mpesas')) {
            Schema::create('mpesas', function (Blueprint $table) {
                $table->increments('id');
                $table->string('type')->nullable();
                $table->string('time')->nullable();
                $table->string('mpesa_transaction_id')->nullable();
                $table->string('amount')->nullable();
                $table->string('short_code')->nullable();
                $table->string('bill_ref_number')->nullable();
                $table->string('invoice_number')->nullable();
                $table->string('msisdn')->nullable();
                $table->string('firstname')->nullable();
                $table->string('middlename')->nullable();
                $table->string('lastname')->nullable();
                $table->string('org_account_balance')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mpesas');
    }
}
