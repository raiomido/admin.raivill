<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1552367940PackageItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('package_items')) {
            Schema::create('package_items', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name')->nullable();
                $table->text('description');
                $table->decimal('price', 15, 2)->nullable();
                $table->enum('active', array('1', '0'))->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_items');
    }
}
