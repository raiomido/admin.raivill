<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5c8741455d625RelationshipsToPackageItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('package_items', function(Blueprint $table) {
            if (!Schema::hasColumn('package_items', 'package_id')) {
                $table->integer('package_id')->unsigned()->nullable();
                $table->foreign('package_id', '31478_5c874144c4f45')->references('id')->on('packages')->onDelete('cascade');
                }
                if (!Schema::hasColumn('package_items', 'created_by_id')) {
                $table->integer('created_by_id')->unsigned()->nullable();
                $table->foreign('created_by_id', '31478_5c874144cb012')->references('id')->on('users')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('package_items', function(Blueprint $table) {
            
        });
    }
}
