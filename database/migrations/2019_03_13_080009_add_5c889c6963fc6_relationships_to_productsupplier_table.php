<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5c889c6963fc6RelationshipsToProductSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_suppliers', function(Blueprint $table) {
            if (!Schema::hasColumn('product_suppliers', 'country_id')) {
                $table->integer('country_id')->unsigned()->nullable();
                $table->foreign('country_id', '31607_5c889c68878f8')->references('id')->on('countries')->onDelete('cascade');
                }
                if (!Schema::hasColumn('product_suppliers', 'created_by_id')) {
                $table->integer('created_by_id')->unsigned()->nullable();
                $table->foreign('created_by_id', '31607_5c889c6894c84')->references('id')->on('users')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_suppliers', function(Blueprint $table) {
            
        });
    }
}
