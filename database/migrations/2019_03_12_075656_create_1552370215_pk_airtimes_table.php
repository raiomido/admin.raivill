<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1552370215PkAirtimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('pk_airtimes')) {
            Schema::create('pk_airtimes', function (Blueprint $table) {
                $table->increments('id');
                $table->string('pay_konnect_transaction_id')->nullable();
                $table->string('pds_transaction_id')->nullable();
                $table->string('time')->nullable();
                $table->string('response_code')->nullable();
                $table->string('response_message')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pk_airtimes');
    }
}
