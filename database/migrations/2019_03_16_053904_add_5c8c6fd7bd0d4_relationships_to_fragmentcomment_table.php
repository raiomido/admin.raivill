<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5c8c6fd7bd0d4RelationshipsToFragmentCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fragment_comments', function(Blueprint $table) {
            if (!Schema::hasColumn('fragment_comments', 'user_id')) {
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign('user_id', '31978_5c8c6fd715b3a')->references('id')->on('users')->onDelete('cascade');
                }
                if (!Schema::hasColumn('fragment_comments', 'fragment_id')) {
                $table->integer('fragment_id')->unsigned()->nullable();
                $table->foreign('fragment_id', '31978_5c8c6fd7234b8')->references('id')->on('post_fragments')->onDelete('cascade');
                }
                if (!Schema::hasColumn('fragment_comments', 'created_by_id')) {
                $table->integer('created_by_id')->unsigned()->nullable();
                $table->foreign('created_by_id', '31978_5c8c6fd730ba6')->references('id')->on('users')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fragment_comments', function(Blueprint $table) {
            
        });
    }
}
