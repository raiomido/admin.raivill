<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5c88a01684864PostPostCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('post_post_category')) {
            Schema::create('post_post_category', function (Blueprint $table) {
                $table->integer('post_id')->unsigned()->nullable();
                $table->foreign('post_id', 'fk_p_31609_31604_postcate_5c88a0168491b')->references('id')->on('posts')->onDelete('cascade');
                $table->integer('post_category_id')->unsigned()->nullable();
                $table->foreign('post_category_id', 'fk_p_31604_31609_post_pos_5c88a0168496a')->references('id')->on('post_categories')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_post_category');
    }
}
