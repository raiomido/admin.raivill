<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Drop5c88a015937725c88a0159174cPostPostCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('post_post_category');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(! Schema::hasTable('post_post_category')) {
            Schema::create('post_post_category', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('post_id')->unsigned()->nullable();
            $table->foreign('post_id', 'fk_p_31609_31604_postcate_5c889f7a938a5')->references('id')->on('posts');
                $table->integer('post_category_id')->unsigned()->nullable();
            $table->foreign('post_category_id', 'fk_p_31604_31609_post_pos_5c889f7a9314c')->references('id')->on('post_categories');
                
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }
}
