<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1552371023AccountEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('account_entries', function (Blueprint $table) {
            if(Schema::hasColumn('account_entries', 'created_by_id')) {
                $table->dropForeign('31494_5c874cc44b4f0');
                $table->dropIndex('31494_5c874cc44b4f0');
                $table->dropColumn('created_by_id');
            }
            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('account_entries', function (Blueprint $table) {
                        
        });

    }
}
