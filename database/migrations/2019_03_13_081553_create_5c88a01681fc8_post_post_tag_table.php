<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5c88a01681fc8PostPostTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('post_post_tag')) {
            Schema::create('post_post_tag', function (Blueprint $table) {
                $table->integer('post_id')->unsigned()->nullable();
                $table->foreign('post_id', 'fk_p_31609_31610_posttag__5c88a0168207c')->references('id')->on('posts')->onDelete('cascade');
                $table->integer('post_tag_id')->unsigned()->nullable();
                $table->foreign('post_tag_id', 'fk_p_31610_31609_post_pos_5c88a016820c6')->references('id')->on('post_tags')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_post_tag');
    }
}
