<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5c889e22dc2acRelationshipsToProductCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_comments', function(Blueprint $table) {
            if (!Schema::hasColumn('product_comments', 'user_id')) {
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign('user_id', '31608_5c889e223607f')->references('id')->on('users')->onDelete('cascade');
                }
                if (!Schema::hasColumn('product_comments', 'product_id')) {
                $table->integer('product_id')->unsigned()->nullable();
                $table->foreign('product_id', '31608_5c889e22424eb')->references('id')->on('products')->onDelete('cascade');
                }
                if (!Schema::hasColumn('product_comments', 'created_by_id')) {
                $table->integer('created_by_id')->unsigned()->nullable();
                $table->foreign('created_by_id', '31608_5c889e224e729')->references('id')->on('users')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_comments', function(Blueprint $table) {
            
        });
    }
}
