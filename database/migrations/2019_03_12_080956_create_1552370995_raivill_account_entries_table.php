<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1552370995RaivillAccountEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('raivill_account_entries')) {
            Schema::create('raivill_account_entries', function (Blueprint $table) {
                $table->increments('id');
                $table->string('reference');
                $table->enum('type', array('DEBIT', 'CREDIT'));
                $table->string('balance_before')->nullable();
                $table->decimal('balance_after', 15, 2)->nullable();
                $table->decimal('amount', 15, 2)->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('raivill_account_entries');
    }
}
