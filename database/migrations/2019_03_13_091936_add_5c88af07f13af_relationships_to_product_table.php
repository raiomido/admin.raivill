<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5c88af07f13afRelationshipsToProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function(Blueprint $table) {
            if (!Schema::hasColumn('products', 'category_id')) {
                $table->integer('category_id')->unsigned()->nullable();
                $table->foreign('category_id', '31606_5c889ae58aa46')->references('id')->on('product_categories')->onDelete('cascade');
                }
                if (!Schema::hasColumn('products', 'created_by_id')) {
                $table->integer('created_by_id')->unsigned()->nullable();
                $table->foreign('created_by_id', '31606_5c889ae59777e')->references('id')->on('users')->onDelete('cascade');
                }
                if (!Schema::hasColumn('products', 'supplier_id')) {
                $table->integer('supplier_id')->unsigned()->nullable();
                $table->foreign('supplier_id', '31606_5c889cd161a43')->references('id')->on('product_suppliers')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function(Blueprint $table) {
            
        });
    }
}
