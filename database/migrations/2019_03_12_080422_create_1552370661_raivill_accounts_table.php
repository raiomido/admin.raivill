<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1552370661RaivillAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('raivill_accounts')) {
            Schema::create('raivill_accounts', function (Blueprint $table) {
                $table->increments('id');
                $table->string('number');
                $table->decimal('balance', 15, 2)->nullable();
                $table->string('name');
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('raivill_accounts');
    }
}
