<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1552370321PkTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('pk_tokens')) {
            Schema::create('pk_tokens', function (Blueprint $table) {
                $table->increments('id');
                $table->string('pay_konnect_transaction_id')->nullable();
                $table->string('pds_transaction_id')->nullable();
                $table->string('time')->nullable();
                $table->string('response_code')->nullable();
                $table->string('response_message')->nullable();
                $table->string('token')->nullable();
                $table->string('units')->nullable();
                $table->string('bssttoken')->nullable();
                $table->string('bsst_token_units')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pk_tokens');
    }
}
