<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5c874a288573bRelationshipsToPkAirtimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pk_airtimes', function(Blueprint $table) {
            if (!Schema::hasColumn('pk_airtimes', 'transaction_id')) {
                $table->integer('transaction_id')->unsigned()->nullable();
                $table->foreign('transaction_id', '31489_5c874a27ef562')->references('id')->on('transactions')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pk_airtimes', function(Blueprint $table) {
            
        });
    }
}
