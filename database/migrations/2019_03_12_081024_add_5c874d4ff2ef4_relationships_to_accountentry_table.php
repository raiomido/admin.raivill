<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5c874d4ff2ef4RelationshipsToAccountEntryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('account_entries', function(Blueprint $table) {
            if (!Schema::hasColumn('account_entries', 'account_id')) {
                $table->integer('account_id')->unsigned()->nullable();
                $table->foreign('account_id', '31494_5c874cc442e7d')->references('id')->on('accounts')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('account_entries', function(Blueprint $table) {
            
        });
    }
}
