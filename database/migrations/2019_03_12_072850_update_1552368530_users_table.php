<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1552368530UsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            if (!Schema::hasColumn('users', 'bill_paid_at')) {
                $table->date('bill_paid_at')->nullable();
            }
            if (!Schema::hasColumn('users', 'email_verified_at')) {
                $table->date('email_verified_at')->nullable();
            }
            if (!Schema::hasColumn('users', 'address_one')) {
                $table->string('address_one')->nullable();
            }
            if (!Schema::hasColumn('users', 'address_two')) {
                $table->string('address_two')->nullable();
            }
            if (!Schema::hasColumn('users', 'postal_code')) {
                $table->string('postal_code')->nullable();
            }
            if (!Schema::hasColumn('users', 'city')) {
                $table->string('city')->nullable();
            }
            if (!Schema::hasColumn('users', 'status')) {
                $table->enum('status', ['1', '0'])->nullable();
            }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('bill_paid_at');
            $table->dropColumn('email_verified_at');
            $table->dropColumn('address_one');
            $table->dropColumn('address_two');
            $table->dropColumn('postal_code');
            $table->dropColumn('city');
            $table->dropColumn('status');

        });

    }
}
