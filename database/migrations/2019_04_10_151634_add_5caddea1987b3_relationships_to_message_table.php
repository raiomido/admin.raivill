<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5caddea1987b3RelationshipsToMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messages', function(Blueprint $table) {
            if (!Schema::hasColumn('messages', 'chat_id')) {
                $table->integer('chat_id')->unsigned()->nullable();
                $table->foreign('chat_id', '33780_5caddea0d2fc3')->references('id')->on('chats')->onDelete('cascade');
                }
                if (!Schema::hasColumn('messages', 'user_id')) {
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign('user_id', '33780_5caddea0e066a')->references('id')->on('users')->onDelete('cascade');
                }
                if (!Schema::hasColumn('messages', 'created_by_id')) {
                $table->integer('created_by_id')->unsigned()->nullable();
                $table->foreign('created_by_id', '33780_5caddea0edfd6')->references('id')->on('users')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages', function(Blueprint $table) {
            
        });
    }
}
