<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaypalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('paypals')) {
            Schema::create('paypals', function (Blueprint $table) {
                $table->increments('id');
                $table->string('paypal_transaction_id')->nullable();
                $table->string('amount')->nullable();
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                $table->integer('transaction_id')->unsigned()->nullable();
                $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('cascade');
                $table->string('create_time')->nullable();
                $table->string('update_time')->nullable();
                $table->string('paypal_id')->nullable();
                $table->string('intent')->nullable();
                $table->string('transaction_status')->nullable();
                $table->string('email')->nullable();
                $table->string('payer_id')->nullable();
                $table->string('country_code')->nullable();
                $table->string('firstname')->nullable();
                $table->string('lastname')->nullable();
                $table->string('phone')->nullable();
                $table->string('currency_code')->nullable();
                $table->string('payee_email_address')->nullable();
                $table->string('payee_merchant_id')->nullable();
                $table->text('shipping')->nullable();
                $table->string('payment_status')->nullable();
                $table->string('payment_id')->nullable();
                $table->text('seller_protection')->nullable();
                $table->text('links')->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paypals');
    }
}
