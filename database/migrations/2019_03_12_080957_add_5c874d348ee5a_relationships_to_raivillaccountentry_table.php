<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5c874d348ee5aRelationshipsToRaivillAccountEntryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('raivill_account_entries', function(Blueprint $table) {
            if (!Schema::hasColumn('raivill_account_entries', 'raivill_account_id')) {
                $table->integer('raivill_account_id')->unsigned()->nullable();
                $table->foreign('raivill_account_id', '31495_5c874d33f2bdb')->references('id')->on('raivill_accounts')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('raivill_account_entries', function(Blueprint $table) {
            
        });
    }
}
