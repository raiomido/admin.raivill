<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5c88bc55b7494OrderTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('order_transaction')) {
            Schema::create('order_transaction', function (Blueprint $table) {
                $table->integer('order_id')->unsigned()->nullable();
                $table->foreign('order_id', 'fk_p_31633_31485_transact_5c88bc55b7546')->references('id')->on('orders')->onDelete('cascade');
                $table->integer('transaction_id')->unsigned()->nullable();
                $table->foreign('transaction_id', 'fk_p_31485_31633_order_tr_5c88bc55b758e')->references('id')->on('transactions')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_transaction');
    }
}
