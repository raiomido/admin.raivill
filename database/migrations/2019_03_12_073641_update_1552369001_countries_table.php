<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1552369001CountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('countries', function (Blueprint $table) {

            if (!Schema::hasColumn('countries', 'flag')) {
                $table->string('flag')->nullable();
            }
            if (!Schema::hasColumn('countries', 'code')) {
                $table->string('code')->nullable();
            }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->dropColumn('flag');
            $table->dropColumn('code');

        });

    }
}
