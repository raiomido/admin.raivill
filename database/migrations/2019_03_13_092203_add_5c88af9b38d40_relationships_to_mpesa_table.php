<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5c88af9b38d40RelationshipsToMpesaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mpesas', function(Blueprint $table) {
            if (!Schema::hasColumn('mpesas', 'user_id')) {
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign('user_id', '31488_5c8749167993a')->references('id')->on('users')->onDelete('cascade');
                }
                if (!Schema::hasColumn('mpesas', 'transaction_id')) {
                $table->integer('transaction_id')->unsigned()->nullable();
                $table->foreign('transaction_id', '31488_5c87491682303')->references('id')->on('transactions')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mpesas', function(Blueprint $table) {
            
        });
    }
}
