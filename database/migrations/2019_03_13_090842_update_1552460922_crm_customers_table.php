<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1552460922CrmCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_customers', function (Blueprint $table) {
            if(Schema::hasColumn('crm_customers', 'testimonial')) {
                $table->dropColumn('testimonial');
            }
            
        });
Schema::table('crm_customers', function (Blueprint $table) {
            
if (!Schema::hasColumn('crm_customers', 'testimonial')) {
                $table->text('testimonial')->nullable();
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_customers', function (Blueprint $table) {
            $table->dropColumn('testimonial');
            
        });
Schema::table('crm_customers', function (Blueprint $table) {
                        $table->string('testimonial')->nullable();
                
        });

    }
}
