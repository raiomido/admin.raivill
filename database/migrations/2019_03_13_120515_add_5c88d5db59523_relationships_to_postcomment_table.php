<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5c88d5db59523RelationshipsToPostCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('post_comments', function(Blueprint $table) {
            if (!Schema::hasColumn('post_comments', 'user_id')) {
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign('user_id', '31640_5c88d5da99880')->references('id')->on('users')->onDelete('cascade');
                }
                if (!Schema::hasColumn('post_comments', 'post_id')) {
                $table->integer('post_id')->unsigned()->nullable();
                $table->foreign('post_id', '31640_5c88d5daa74d4')->references('id')->on('posts')->onDelete('cascade');
                }
                if (!Schema::hasColumn('post_comments', 'created_by_id')) {
                $table->integer('created_by_id')->unsigned()->nullable();
                $table->foreign('created_by_id', '31640_5c88d5dab4ad5')->references('id')->on('users')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('post_comments', function(Blueprint $table) {
            
        });
    }
}
