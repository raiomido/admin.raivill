<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5c874a931b292RelationshipsToPkTokenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pk_tokens', function(Blueprint $table) {
            if (!Schema::hasColumn('pk_tokens', 'transaction_id')) {
                $table->integer('transaction_id')->unsigned()->nullable();
                $table->foreign('transaction_id', '31490_5c874a9287b9d')->references('id')->on('transactions')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pk_tokens', function(Blueprint $table) {
            
        });
    }
}
