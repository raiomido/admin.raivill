<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $this->call(ContentPageSeed::class);
        $this->call(CountrySeed::class);
        $this->call(RoleSeed::class);
        $this->call(UserSeed::class);
        $this->call(PermissionSeed::class);
        $this->call(CrmStatusSeed::class);
        $this->call(FaqCategorySeed::class);
        $this->call(FaqQuestionSeed::class);
        $this->call(RoleSeedPivot::class);
        $this->call(UserSeedPivot::class);
        $this->call(PackagesSeed::class);
        $this->call(PackageItemsSeed::class);
        $this->call(AccountSeed::class);
        $this->call(RaivillAccountSeed::class);
        $this->call(ProductSupplierSeed::class);
        $this->call(ProductCategorySeed::class);
        $this->call(PostCategorySeed::class);

    }
}
