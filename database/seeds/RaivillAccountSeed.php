<?php

use Illuminate\Database\Seeder;

class RaivillAccountSeed extends Seeder
{
    use \App\Http\Traits\Util;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['id' => 1, 'name' => 'Raivill Invent', 'number' => $this->generateAccountNumber(true)]
        ];
        foreach ($items as $item) {
            \App\RaivillAccount::create($item);
        }
    }
}
