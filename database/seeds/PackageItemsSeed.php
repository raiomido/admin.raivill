<?php

use Illuminate\Database\Seeder;

class PackageItemsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $packageItems = [
            [
                'name'        => 'Domain name and Hosting',
                'package_id'  => 1,
                'description' => 'A domain name of your preference is purchased, and hosted. This is done prior to starting the development work',
                'price'       => 5000,
                'active'      => 1,
            ],
            [
                'name'        => 'Purchase and installation of themes',
                'package_id'  => 1,
                'description' => 'This involves the purchase, installation and configuration of a ready made theme of your preference from theme vendors. This also involves configuration of emails',
                'price'       => 20000,
                'active'      => 1,
            ],
            [
                'name'        => 'Search Engine Optimization',
                'package_id'  => 1,
                'description' => 'I not only build search engine-friendly websites, I also configure your site for search bots to crawl and index it easily',
                'price'       => 10000,
                'active'      => 1,
            ],
            [
                'name'        => 'Domain name and Hosting',
                'package_id'  => 2,
                'description' => 'A domain name of your preference is purchased, and hosted. This is done prior to starting the development work',
                'price'       => 5000,
                'active'      => 1,
            ],
            [
                'name'        => 'Development of front end and back end',
                'package_id'  => 2,
                'description' => 'This involves custom development of your website\'s files from scratch using PHP and Vue.js. This also involves configuration of emails',
                'price'       => 35000,
                'active'      => 1,
            ],
            [
                'name'        => 'Search Engine Optimization',
                'package_id'  => 2,
                'description' => 'I not only build search engine-friendly websites, I also configure your site for search bots to crawl and index it easily',
                'price'       => 10000,
                'active'      => 1,
            ],
            [
                'name'        => 'Domain name and Hosting (Dedicated)',
                'package_id'  => 3,
                'description' => 'A domain name of your preference is purchased, and hosted. This is done prior to starting the development work',
                'price'       => 15000,
                'active'      => 1,
            ],
            [
                'name'        => 'Development of front end and back end',
                'package_id'  => 3,
                'description' => 'This involves custom development of your website\'s files from scratch using PHP and Vue.js. This development will include development of user authentication and access control (administration) modules. There is also configuration of emails',
                'price'       => 55000,
                'active'      => 1,
            ],
            [
                'name'        => 'Search Engine Optimization',
                'package_id'  => 3,
                'description' => 'I not only build search engine-friendly websites, I also configure your site for search bots to crawl and index it easily',
                'price'       => 10000,
                'active'      => 1,
            ],
            [
                'name'        => 'Domain name and Hosting (Dedicated)',
                'package_id'  => 4,
                'description' => 'A domain name of your preference is purchased, and hosted. This is done prior to starting the development work',
                'price'       => 15000,
                'active'      => 1,
            ],
            [
                'name'        => 'Development of front end and back end',
                'package_id'  => 4,
                'description' => 'This involves custom development of your custom application\'s files from scratch using PHP and Vue.js. This development will include but not limited to development of user authentication, access control (administration), push notifications and job queueing modules and an API. There is also configuration of emails. This package is ideal for startups who are looking to launch/ revamp a mobile application or web solution. This package does not include the development of the mobile application.',
                'price'       => 225000,
                'active'      => 1,
            ],
            [
                'name'        => 'Search Engine Optimization',
                'package_id'  => 4,
                'description' => 'I will not only build search engine-friendly app front / landing page, I will also configure your front office for search bots to crawl and index it easily',
                'price'       => 10000,
                'active'      => 1,
            ],
        ];

        foreach ($packageItems as $packageItem) {
            \App\PackageItem::create($packageItem);
        }
    }
}
