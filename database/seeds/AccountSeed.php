<?php

use Illuminate\Database\Seeder;

class AccountSeed extends Seeder
{
    use \App\Http\Traits\Util;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [  'number' => $this->generateAccountNumber(),
               'balance' => 0,
               'created_by_id' => 1,
               'user_id' => 1,
            ]
        ];

        foreach ($items as $item) {
            \App\Account::create($item);
        }
    }
}
