<?php

use Illuminate\Database\Seeder;

class ProductCategorySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['name' => 'Post Product', 'slug' => 'post-product', 'active' => 1, 'created_by_id' => 1],

        ];

        foreach ($items as $item) {
            \App\ProductCategory::create($item);
        }
    }
}
