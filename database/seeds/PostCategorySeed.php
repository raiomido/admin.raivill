<?php

use Illuminate\Database\Seeder;

class PostCategorySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['name' => 'Computer Programming', 'slug' => 'computer-programming', 'active' => 1, 'created_by_id' => 1],

        ];

        foreach ($items as $item) {
            \App\PostCategory::create($item);
        }
    }
}
