<?php

use Illuminate\Database\Seeder;

class PackagesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $packages = array(
            array('id' => '1','name' => 'Basic','slug' => 'basic','active' => '1','created_at' => '2019-03-18 16:39:00','updated_at' => '2019-03-18 16:39:00','deleted_at' => NULL,'created_by_id' => '1'),
            array('id' => '2','name' => 'Small Business','slug' => 'small-business','active' => '1','created_at' => '2019-03-18 16:39:54','updated_at' => '2019-03-18 16:39:54','deleted_at' => NULL,'created_by_id' => '1'),
            array('id' => '3','name' => 'Corporate Business','slug' => 'corporate-business','active' => '1','created_at' => '2019-03-18 16:40:32','updated_at' => '2019-03-18 16:40:32','deleted_at' => NULL,'created_by_id' => '1'),
            array('id' => '4','name' => 'Pro','slug' => 'pro','active' => '1','created_at' => '2019-03-18 16:41:01','updated_at' => '2019-03-18 16:41:01','deleted_at' => NULL,'created_by_id' => '1')
        );
        foreach ($packages as $package) {
            \App\Package::create($package);
        }
    }
}
