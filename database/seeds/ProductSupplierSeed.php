<?php

use Illuminate\Database\Seeder;

class ProductSupplierSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['name' => 'Raicast', 'slug' => 'raicast', 'website' => 'https://raicast.co.ke', 'email' => 'info@raicast.co.ke', 'phone' => '+254741266296', 'description' => 'Raicast Blog', 'active' => 1, 'country_id' => 1, 'created_by_id' => 1],

        ];

        foreach ($items as $item) {
            \App\ProductSupplier::create($item);
        }
    }
}
