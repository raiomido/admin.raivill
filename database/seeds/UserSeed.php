<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    use \App\Http\Traits\Util;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            
            ['id' => 1, 'name' => 'raiomido', 'email' => 'raiomido@gmail.com', 'password' => '$2y$10$ec2Xa6AMgtAVCRdxMbI6cOWPZUHFnqFNMTJEvCq8chf4VcHM9EJ6S', 'remember_token' => '', 'firstname' => 'Rai', 'lastname' => 'Omido', 'avatar' => null, 'newsletter' => null, 'about' => null, 'created_by_id' => null, 'bill_paid_at' => null, 'email_verified_at' => null, 'address_one' => null, 'address_two' => null, 'postal_code' => null, 'city' => null, 'status' => null, 'country_id' => null, 'language_id' => null, 'currency_id' => null,],

        ];

        foreach ($items as $item) {
            $user = \App\User::create($item);
            $user->account()->create(
                [
                    'number' => $this->generateAccountNumber(),
                    'balance' => 0,
                    'created_by_id' => 1,
                ]
            );
        }
    }
}
