<?php

$factory->define(App\RaivillAccount::class, function (Faker\Generator $faker) {
    return [
        "number" => $faker->name,
        "balance" => $faker->randomNumber(2),
        "name" => $faker->name,
        "created_by_id" => factory('App\User')->create(),
    ];
});
