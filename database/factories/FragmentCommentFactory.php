<?php

$factory->define(App\FragmentComment::class, function (Faker\Generator $faker) {
    return [
        "user_id" => factory('App\User')->create(),
        "comment" => $faker->name,
        "active" => $faker->name,
        "fragment_id" => factory('App\PostFragment')->create(),
        "created_by_id" => factory('App\User')->create(),
    ];
});
