<?php

$factory->define(App\ProductComment::class, function (Faker\Generator $faker) {
    return [
        "user_id" => factory('App\User')->create(),
        "product_id" => factory('App\Product')->create(),
        "comment" => $faker->name,
        "active" => $faker->name,
        "created_by_id" => factory('App\User')->create(),
    ];
});
