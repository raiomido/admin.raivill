<?php

$factory->define(App\Language::class, function (Faker\Generator $faker) {
    return [
        "title" => $faker->name,
        "code" => $faker->name,
        "locale" => $faker->name,
        "created_by_id" => factory('App\User')->create(),
    ];
});
