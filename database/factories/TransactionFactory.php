<?php

$factory->define(App\Transaction::class, function (Faker\Generator $faker) {
    return [
        "user_id" => factory('App\User')->create(),
        "reference" => $faker->name,
        "type" => $faker->name,
        "amount" => $faker->randomNumber(2),
        "cost" => $faker->randomNumber(2),
        "status" => $faker->name,
    ];
});
