<?php

$factory->define(App\Message::class, function (Faker\Generator $faker) {
    return [
        "chat_id" => factory('App\Chat')->create(),
        "user_id" => factory('App\User')->create(),
        "message" => $faker->name,
        "active" => collect(["1","0",])->random(),
        "created_by_id" => factory('App\User')->create(),
    ];
});
