<?php

$factory->define(App\Product::class, function (Faker\Generator $faker) {
    return [
        "name" => $faker->name,
        "slug" => $faker->name,
        "price" => $faker->randomNumber(2),
        "link" => $faker->name,
        "summary" => $faker->name,
        "description" => $faker->name,
        "views" => $faker->randomNumber(2),
        "likes_count" => $faker->randomNumber(2),
        "comments_count" => $faker->randomNumber(2),
        "category_id" => factory('App\ProductCategory')->create(),
        "created_by_id" => factory('App\User')->create(),
        "supplier_id" => factory('App\ProductSupplier')->create(),
        "reference" => $faker->name,
    ];
});
