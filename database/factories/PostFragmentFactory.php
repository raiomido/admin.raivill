<?php

$factory->define(App\PostFragment::class, function (Faker\Generator $faker) {
    return [
        "title" => $faker->name,
        "body" => $faker->name,
        "post_id" => factory('App\Post')->create(),
        "active" => collect(["1","0",])->random(),
        "created_by_id" => factory('App\User')->create(),
    ];
});
