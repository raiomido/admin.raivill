<?php

$factory->define(App\Currency::class, function (Faker\Generator $faker) {
    return [
        "title" => $faker->name,
        "code" => $faker->name,
        "symbol_left" => $faker->name,
        "symbol_right" => $faker->name,
        "decimal_place" => $faker->randomNumber(2),
        "value" => $faker->randomNumber(2),
        "active" => collect(["1","0",])->random(),
        "created_by_id" => factory('App\User')->create(),
    ];
});
