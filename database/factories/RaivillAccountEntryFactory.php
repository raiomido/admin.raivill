<?php

$factory->define(App\RaivillAccountEntry::class, function (Faker\Generator $faker) {
    return [
        "reference" => $faker->name,
        "type" => collect(["DEBIT","CREDIT",])->random(),
        "balance_before" => $faker->name,
        "balance_after" => $faker->randomNumber(2),
        "amount" => $faker->randomNumber(2),
        "raivill_account_id" => factory('App\RaivillAccount')->create(),
    ];
});
