<?php

$factory->define(App\Paypal::class, function (Faker\Generator $faker) {
    return [
        "user_id" => factory('App\User')->create(),
        "transaction_id" => factory('App\Transaction')->create(),
        "type" => $faker->name,
        "time" => $faker->name,
        "paypal_transaction_id" => $faker->name,
        "amount" => $faker->name,
    ];
});
