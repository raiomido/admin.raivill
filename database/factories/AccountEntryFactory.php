<?php

$factory->define(App\AccountEntry::class, function (Faker\Generator $faker) {
    return [
        "reference" => $faker->name,
        "type" => collect(["DEBIT","CREDIT",])->random(),
        "balance_before" => $faker->name,
        "balance_after" => $faker->randomNumber(2),
        "amount" => $faker->randomNumber(2),
        "account_id" => factory('App\Account')->create(),
    ];
});
