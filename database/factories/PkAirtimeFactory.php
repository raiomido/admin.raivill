<?php

$factory->define(App\PkAirtime::class, function (Faker\Generator $faker) {
    return [
        "pay_konnect_transaction_id" => $faker->name,
        "pds_transaction_id" => $faker->name,
        "time" => $faker->name,
        "response_code" => $faker->name,
        "response_message" => $faker->name,
        "transaction_id" => factory('App\Transaction')->create(),
    ];
});
