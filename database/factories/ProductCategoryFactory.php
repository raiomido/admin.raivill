<?php

$factory->define(App\ProductCategory::class, function (Faker\Generator $faker) {
    return [
        "name" => $faker->name,
        "slug" => $faker->name,
        "active" => collect(["1","0",])->random(),
        "created_by_id" => factory('App\User')->create(),
    ];
});
