<?php

$factory->define(App\Order::class, function (Faker\Generator $faker) {
    return [
        "product_id" => factory('App\Product')->create(),
        "reference" => $faker->name,
        "status" => collect(["PENDING_PAYMENT","COMPLETED","PROCESSING","ON_HOLD","CANCELLED","REFUNDED",])->random(),
        "payment_completed_at" => $faker->date("Y-m-d", $max = 'now'),
        "active" => collect(["1","0",])->random(),
        "created_by_id" => factory('App\User')->create(),
    ];
});
