<?php

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        "name" => $faker->name,
        "email" => $faker->safeEmail,
        "password" => str_random(10),
        "remember_token" => $faker->name,
        "firstname" => $faker->name,
        "lastname" => $faker->name,
        "newsletter" => collect(["1","0",])->random(),
        "about" => $faker->name,
        "created_by_id" => factory('App\User')->create(),
        "bill_paid_at" => $faker->date("Y-m-d", $max = 'now'),
        "email_verified_at" => $faker->date("Y-m-d", $max = 'now'),
        "address_one" => $faker->name,
        "address_two" => $faker->name,
        "postal_code" => $faker->name,
        "city" => $faker->name,
        "status" => collect(["1","0",])->random(),
        "country_id" => factory('App\Country')->create(),
        "language_id" => factory('App\Language')->create(),
        "currency_id" => factory('App\Currency')->create(),
    ];
});
