<?php

$factory->define(App\Mpesa::class, function (Faker\Generator $faker) {
    return [
        "user_id" => factory('App\User')->create(),
        "transaction_id" => factory('App\Transaction')->create(),
        "type" => $faker->name,
        "time" => $faker->name,
        "mpesa_transaction_id" => $faker->name,
        "amount" => $faker->name,
        "short_code" => $faker->name,
        "bill_ref_number" => $faker->name,
        "invoice_number" => $faker->name,
        "msisdn" => $faker->name,
        "firstname" => $faker->name,
        "middlename" => $faker->name,
        "lastname" => $faker->name,
        "org_account_balance" => $faker->name,
    ];
});
