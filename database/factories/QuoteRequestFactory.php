<?php

$factory->define(App\QuoteRequest::class, function (Faker\Generator $faker) {
    return [
        "firstname" => $faker->name,
        "lastname" => $faker->name,
        "email" => $faker->safeEmail,
        "phone" => $faker->name,
        "website" => $faker->name,
        "message" => $faker->name,
        "created_by_id" => factory('App\User')->create(),
    ];
});
