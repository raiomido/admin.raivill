<?php

$factory->define(App\ShoppingCart::class, function (Faker\Generator $faker) {
    return [
        "identifier" => $faker->name,
        "instance" => $faker->name,
        "content" => $faker->name,
        "created_by_id" => factory('App\User')->create(),
    ];
});
