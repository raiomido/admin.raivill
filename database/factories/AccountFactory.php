<?php

$factory->define(App\Account::class, function (Faker\Generator $faker) {
    return [
        "number" => $faker->name,
        "balance" => $faker->randomNumber(2),
        "user_id" => factory('App\User')->create(),
        "created_by_id" => factory('App\User')->create(),
    ];
});
