<?php

$factory->define(App\PackageItem::class, function (Faker\Generator $faker) {
    return [
        "package_id" => factory('App\Package')->create(),
        "name" => $faker->name,
        "description" => $faker->name,
        "price" => $faker->randomNumber(2),
        "active" => collect(["1","0",])->random(),
        "created_by_id" => factory('App\User')->create(),
    ];
});
