<?php

$factory->define(App\ProductSupplier::class, function (Faker\Generator $faker) {
    return [
        "name" => $faker->name,
        "slug" => $faker->name,
        "website" => $faker->name,
        "email" => $faker->safeEmail,
        "phone" => $faker->name,
        "description" => $faker->name,
        "country_id" => factory('App\Country')->create(),
        "active" => collect(["1","0",])->random(),
        "created_by_id" => factory('App\User')->create(),
    ];
});
